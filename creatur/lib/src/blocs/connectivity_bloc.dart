import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:rxdart/rxdart.dart';

/// The class that constantly send connectivity states to its subscribers
class ConnectivityBloc {
  /// Singleton instance of the connectivityBloc
  static final ConnectivityBloc _instance = ConnectivityBloc._createInstance();

  /// The broadcast stream for the connectivity
  final PublishSubject<bool> _connectivityController = PublishSubject<bool>();

  /// The corresponding stream for the bloc
  Stream<bool> get connectivityStream => _connectivityController.stream;

  /// The sink add function for updating the events on the Stream
  Function get _changeConnectivity => _connectivityController.sink.add;

  /// The connectivity instance
  final Connectivity _connectivity = Connectivity();

  /// Indicates whether it has connectivity or not
  bool _hasInternet;

  /// The connectivity last state to be accessed externally
  Future<bool> get lastState async => _hasInternet;

  /// the connectivity listener
  StreamSubscription<ConnectivityResult> _listener;

  /// the default constructor to create the bloc
  factory ConnectivityBloc() => _instance;

  /// The default singleton constructor for the class
  ConnectivityBloc._createInstance() {
    if (_listener == null) {
      _listener =
          _connectivity.onConnectivityChanged.listen(_connectivityListener);
    }
  }

  /// Receives the connectivity change and notifies the stream of that change
  void _connectivityListener(ConnectivityResult state) {
    bool pre = _hasInternet;
    if (pre == null || pre != (state != ConnectivityResult.none)) {
      _hasInternet = state != ConnectivityResult.none;
      _changeConnectivity(_hasInternet);
      print('CAMBIO LA CONECTIVIDAD A $state Y BOOL ES $_hasInternet');
    }
  }

  /// disposes the streams of this bloc
  void dispose() {
    _connectivityController.close();
    _listener.cancel();
  }
}
