import 'package:creatur/src/blocs/register/register_validators.dart';
import 'package:creatur/src/models/login_model.dart';
import 'package:creatur/src/repositories/user_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

/// Class that defines the Bloc logic
class RegisterBloc with RegisterValidator {
  /// The emailController behaviorSubject
  final BehaviorSubject _emailController = BehaviorSubject<String>();

  /// The passwordController behaviorSubject
  final BehaviorSubject _passwordController = BehaviorSubject<String>();

  /// The password2Controller behaviorSubject (password confirmation)
  final BehaviorSubject _password2Controller = BehaviorSubject<String>();

  /// The repository reference for the logIn
  final UserRepository _userRepository = UserRepository();

  /// changes and retrieves the value in the bloc for the email
  Function(String) get changeEmail => _emailController.sink.add;

  /// changes and retrieves the value in the bloc for the password
  Function(String) get changePassword => _passwordController.sink.add;

  /// changes and retrieves the value in the bloc for the password validation
  Function(String) get changePassword2 => _password2Controller.sink.add;

  /// Retrieves the stream value in the streamController of the email
  Stream<String> get email => _emailController.stream.value;

  /// Retrieves the stream value in the streamController of the password
  Stream<String> get password => _passwordController.stream.value;

  /// Retrieves the stream value in the streamController of the second password
  Stream<String> get password2 => _passwordController.stream.value;

  // Get streams
  get emailStream => _emailController.stream.transform(validateEmail);
  get passwordStream => _passwordController.stream.transform(validatePassword);
  get password2Stream => _password2Controller.stream
          .transform(validatePassword)
          .doOnData((String passwordConfirm) {
        if (0 != _passwordController.value.compareTo(passwordConfirm)) {
          _password2Controller.addError("Las contraseñas no coinciden");
        }
      });

  /// Validate if format is valid
  Stream<bool> get loginValidStream => Rx.combineLatest3(
      emailStream,
      passwordStream,
      password2Stream,
      (emailStream, passwordStream, password2Stream) => true);

  /// Submits the values from the email and passwords fields
  Future<bool> submit() async {
    final validEmail = _emailController.value;
    final validPassword = _passwordController.value;

    try {
      return await _userRepository
          .register(LoginModel(validEmail, validPassword).encodeToJson());
    } catch (e) {
      var error = e;
      if (e?.message == null || e?.message == '')
        error = new CreaturError(
            type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
      throw error;
    }
  }

  /// Disposes the stream controllers closing them
  void dispose() {
    _emailController.close();
    _passwordController.close();
    _password2Controller.close();
  }
}
