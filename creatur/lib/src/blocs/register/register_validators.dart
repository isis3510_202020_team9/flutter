import 'dart:async';

class RegisterValidator {
  /// Validator for password
  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
    bool hasUppercase = password.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(new RegExp(r'[0-9]'));
    bool hasLowercase = password.contains(new RegExp(r'[a-z]'));
    if (password.length < 6)
      sink.addError('La contraseña debe tener mínimo 6 caracteres.');
    else if (!hasUppercase)
      sink.addError('La contraseña debe tener mínimo 1 mayúscula.');
    else if (!hasDigits)
      sink.addError('La contraseña debe tener mínimo 1 número.');
    else if (!hasLowercase)
      sink.addError('La contraseña debe tener mínimo 1 minúscula.');
    else
      sink.add(password);
  });

  final validateEmail = StreamTransformer<String, String>.fromHandlers(
      handleData: (changeEmail, sink) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (regExp.hasMatch(changeEmail))
      sink.add(changeEmail);
    else
      sink.addError('Email incorrecto.');
  });
}
