import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/repositories/product_repository.dart';
import 'package:creatur/src/repositories/venture_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

/// The class that contains the category bloc of the explore page
class CategoryBloc {
  /// Checks if the request for products is loading
  bool _isLoadingProducts;

  /// Checks if the request for ventures is loading
  bool _isLoadingVentures;

  /// The list of category products
  List<ProductModel> _categoryProducts;

  /// The list of the category ventures
  List<VentureModel> _categoryVentures;

  /// The product repository to access all the funcionalities
  ProductRepository _productRepository;

  /// The venture repository to access all the funcionalities
  VentureRepository _ventureRepository;

  /// The category products stream
  final PublishSubject<List<ProductModel>> _productsController =
      PublishSubject<List<ProductModel>>();

  /// The category ventures stream
  final PublishSubject<List<VentureModel>> _venturesController =
      PublishSubject<List<VentureModel>>();

  /// The sink to add the list of product in the stream
  Function(List<ProductModel>) get changeProducts =>
      _productsController.sink.add;

  /// The sink to add the list of ventures in the stream
  Function(List<VentureModel>) get changeVentures =>
      _venturesController.sink.add;

  /// The stream of the suggested products
  Stream<List<ProductModel>> get categoryProductsStream =>
      _productsController.stream;

  /// The stream of the suggested ventures
  Stream<List<VentureModel>> get categoryVenturesStream =>
      _venturesController.stream;

  /// Creates the recommended bloc
  CategoryBloc() {
    _isLoadingProducts = false;
    _isLoadingVentures = false;
    _categoryProducts = List();
    _categoryVentures = List();
    _productRepository = ProductRepository();
    _ventureRepository = VentureRepository();
  }

  /// Loads the category products from the DB and sends them to the stream
  Future<List<ProductModel>> getCategoryProducts(String category) async {
    if (_isLoadingProducts) return [];
    _isLoadingProducts = true;
    try {
      final List<ProductModel> response =
          await _productRepository.getCategoryProducts(category);
      _categoryProducts = response;
      changeProducts(_categoryProducts);
      _isLoadingProducts = false;
      return response;
    } catch (e) {
      _isLoadingProducts = false;
      _productsController.sink.addError(AppErrors.CATEGORY_PRODUCTS_ERROR);
      return null;
    }
  }

  /// Loads the category ventures from the DB and sends them to the stream
  Future<List<VentureModel>> getCategoryVentures(String category) async {
    if (_isLoadingVentures) return [];
    _isLoadingVentures = true;
    try {
      final List<VentureModel> response =
          await _ventureRepository.getCategoryVentures(category);
      _categoryVentures = response;
      changeVentures(_categoryVentures);
      _isLoadingVentures = false;
      return response;
    } catch (e) {
      _isLoadingVentures = false;
      _venturesController.sink.addError(AppErrors.CATEGORY_VENTURES_ERROR);
      return null;
    }
  }

  /// Dispose the streams to close them
  void dispose() {
    _productsController.close();
    _venturesController.close();
  }
}
