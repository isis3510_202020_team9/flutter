import 'package:creatur/src/blocs/venture/venture-home_bloc.dart';

/// Class which provides the venture admin related blocs
class BusinessBloc {
  /// Singleton instance for the businessBloc
  static final BusinessBloc _instance = BusinessBloc._createInstance();

  /// The venture home bloc
  VentureHomeBloc _ventureHomeBloc;

  /// Default constructor of the class
  BusinessBloc._createInstance() {
    _ventureHomeBloc = VentureHomeBloc();
  }

  /// Singleton retrieval for the class
  factory BusinessBloc() => _instance;

  /// loginBloc getter
  VentureHomeBloc get ventureHomeBloc => _ventureHomeBloc;
}
