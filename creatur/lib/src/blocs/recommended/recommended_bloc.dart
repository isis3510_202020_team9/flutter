import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/repositories/product_repository.dart';
import 'package:creatur/src/repositories/repository.dart';
import 'package:creatur/src/repositories/venture_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

/// The class that contains the recommendations of the home page
class RecommendedBloc {
  /// The suggested products pagination
  int _productsPagination;

  /// Checks if the request for products is loading
  bool _isLoadingProducts;

  /// Checks if the request for ventures is loading
  bool _isLoadingVentures;

  /// Checks if the request for near ventures & products is loading
  bool _isLoadingNearOnes;

  /// Checks if the request for trendy ventures is loading
  bool _isLoadingTrendyVentures;

  /// Checks if the request for trendy products is loading
  bool _isLoadingTrendyProducts;

  /// The list of suggested products
  List<ProductModel> _suggestedProducts;

  /// The list of the suggested ventures
  List<VentureModel> _suggestedVentures;

  /// The list of the near ventures
  List<VentureModel> _nearVentures;

  /// The list of the near products
  List<ProductModel> _nearProducts;

  /// The list of the trendy ventures
  List<VentureModel> _trendyVentures;

  /// The list of the trendy products
  List<ProductModel> _trendyProducts;

  /// The product repository to access all the funcionalities
  ProductRepository _productRepository;

  /// The venture repository to access all the funcionalities
  VentureRepository _ventureRepository;

  /// The Suggested products stream
  final PublishSubject<List<ProductModel>> _productsController =
      PublishSubject<List<ProductModel>>();

  /// The Suggested ventures stream
  final PublishSubject<List<VentureModel>> _venturesController =
      PublishSubject<List<VentureModel>>();

  /// The Near ventures stream
  final PublishSubject<List<VentureModel>> _nearVenturesController =
      PublishSubject<List<VentureModel>>();

  /// The Near products stream
  final PublishSubject<List<ProductModel>> _nearProductsController =
      PublishSubject<List<ProductModel>>();

  /// The trendy ventures stream
  final PublishSubject<List<VentureModel>> _trendyVenturesController =
      PublishSubject<List<VentureModel>>();

  /// The trendy products stream
  final PublishSubject<List<ProductModel>> _trendyProductsController =
      PublishSubject<List<ProductModel>>();

  /// The sink to add the list of product in the stream
  Function(List<ProductModel>) get changeProducts =>
      _productsController.sink.add;

  /// The sink to add the list of ventures in the stream
  Function(List<VentureModel>) get changeVentures =>
      _venturesController.sink.add;

  /// The sink to add the list of near ventures in the stream
  Function(List<VentureModel>) get changeNearVentures =>
      _nearVenturesController.sink.add;

  /// The sink to add the list of near products in the stream
  Function(List<ProductModel>) get changeNearProducts =>
      _nearProductsController.sink.add;

  /// The sink to add the list of trendy ventures in the stream
  Function(List<VentureModel>) get changeTrendyVentures =>
      _trendyVenturesController.sink.add;

  /// The sink to add the list of trendy products in the stream
  Function(List<ProductModel>) get changeTrendyProducts =>
      _trendyProductsController.sink.add;

  /// The stream of the suggested products
  Stream<List<ProductModel>> get suggestedProductsStream =>
      _productsController.stream;

  /// The stream of the suggested ventures
  Stream<List<VentureModel>> get suggestedVenturesStream =>
      _venturesController.stream;

  /// The stream of the near ventures
  Stream<List<VentureModel>> get nearVenturesStream =>
      _nearVenturesController.stream;

  /// The stream of the near products
  Stream<List<ProductModel>> get nearProductsStream =>
      _nearProductsController.stream;

  /// The stream of the trendy ventures
  Stream<List<VentureModel>> get trendyVenturesStream =>
      _trendyVenturesController.stream;

  /// The stream of the trendy products
  Stream<List<ProductModel>> get trendyProductsStream =>
      _trendyProductsController.stream;

  /// Creates the recommended bloc
  RecommendedBloc() {
    _productsPagination = 0;
    _isLoadingProducts = false;
    _isLoadingVentures = false;
    _isLoadingNearOnes = false;
    _isLoadingTrendyVentures = false;
    _isLoadingTrendyProducts = false;
    _suggestedProducts = List();
    _suggestedVentures = List();
    _nearVentures = List();
    _nearProducts = List();
    _trendyVentures = List();
    _trendyProducts = List();
    _productRepository = ProductRepository();
    _ventureRepository = VentureRepository();
  }

  /// Loads the suggested products from the DB and sends them to the stream
  Future<List<ProductModel>> getSuggestedProducts() async {
    if (_isLoadingProducts) return [];
    _productsPagination++;
    _isLoadingProducts = true;
    try {
      final List<ProductModel> response =
          await _productRepository.getSuggestedProducts(_productsPagination);
      _suggestedProducts.addAll(response);
      changeProducts(_suggestedProducts);
      _isLoadingProducts = false;
      return response;
    } catch (e) {
      _productsPagination--;
      _isLoadingProducts = false;
      _productsController.sink.addError(AppErrors.RECOMMENDED_PRODUCTS_ERROR);
      return null;
    }
  }

  /// Loads the suggested ventures from the DB and sends them to the stream
  Future<List<VentureModel>> getSuggestedVentures() async {
    if (_isLoadingVentures) return [];
    _isLoadingVentures = true;
    try {
      final List<VentureModel> response =
          await _ventureRepository.getSuggestedVentures();
      //_suggestedVentures.addAll(response); /// Not paginated so replace them
      _suggestedVentures = response;
      changeVentures(_suggestedVentures);
      _isLoadingVentures = false;
      return response;
    } catch (e) {
      _isLoadingVentures = false;
      _venturesController.sink.addError(AppErrors.RECOMMENDED_VENTURES_ERROR);
      return null;
    }
  }

  /// Loads the near ventures and products from the DB and sends them to the stream
  Future<Map<String, List>> getNear() async {
    if (_isLoadingNearOnes) return {};
    _isLoadingNearOnes = true;
    try {
      final Map<String, List> response = await _ventureRepository.getNear();
      _nearVentures.addAll(response['ventures'] as List<VentureModel>);
      _nearProducts.addAll(response['products'] as List<ProductModel>);
      changeNearVentures(_nearVentures);
      changeNearProducts(_nearProducts);
      _isLoadingNearOnes = false;
      return response;
    } catch (e) {
      _isLoadingNearOnes = false;
      _nearProductsController.sink.addError(AppErrors.NEAR_PRODUCTS_ERROR);
      _nearVenturesController.sink.addError(AppErrors.NEAR_VENTURES_ERROR);
      return null;
    }
  }

  /// Loads the trendy ventures from the DB and sends them to the stream
  Future<List<VentureModel>> getTrendyVentures() async {
    if (_isLoadingTrendyVentures) return [];
    _isLoadingTrendyVentures = true;
    try {
      final List<VentureModel> response = await _ventureRepository.getTrendy();
      _trendyVentures = response;
      changeTrendyVentures(_trendyVentures);
      _isLoadingTrendyVentures = false;
      return response;
    } catch (e) {
      _isLoadingTrendyVentures = false;
      _trendyVenturesController.sink.addError(AppErrors.TRENDY_VENTURES_ERROR);
      return null;
    }
  }

  /// Loads the trendy products from the DB and sends them to the stream
  Future<List<ProductModel>> getTrendyProducts() async {
    if (_isLoadingTrendyProducts) return [];
    _isLoadingTrendyProducts = true;
    try {
      final List<ProductModel> response = await _productRepository.getTrendy();
      _trendyProducts = response;
      changeTrendyProducts(_trendyProducts);
      _isLoadingTrendyProducts = false;
      return response;
    } catch (e) {
      _isLoadingTrendyProducts = false;
      _trendyProductsController.sink.addError(AppErrors.TRENDY_PRODUCTS_ERROR);
      return null;
    }
  }

  /// Logs the user out clearing all data
  Future<void> logout() async {
    await Repository.logout();
  }

  /// Dispose the streams to close them
  void dispose() {
    _productsController.close();
    _venturesController.close();
    _nearVenturesController.close();
    _nearProductsController.close();
    _trendyVenturesController.close();
    _trendyProductsController.close();
  }
}
