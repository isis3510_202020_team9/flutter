import 'dart:async';

import 'package:creatur/src/models/profiling_model.dart';
import 'package:creatur/src/repositories/user_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

/// Profiling bloc class
class ProfilingBloc {
  /// The question 1 controller
  final BehaviorSubject _firstQuestionController = BehaviorSubject<int>();

  /// The question 2 controller
  final BehaviorSubject _secondQuestionController = BehaviorSubject<int>();

  /// The question 3 controller
  final BehaviorSubject _thirdQuestionController = BehaviorSubject<int>();

  // Change questions options

  Function(int) get changeFirstQuestion => _firstQuestionController.sink.add;

  Function(int) get changeSecondQuestion => _secondQuestionController.sink.add;

  Function(int) get changeThirdQuestion => _thirdQuestionController.sink.add;

  /// The repository reference for the logIn
  final UserRepository _userRepository = UserRepository();

  /// Retrieves the stream value in the streamController of the first question
  get firstQuestion => _firstQuestionController.stream;

  /// Retrieves the stream value in the streamController of the  second question
  get secondQuestion => _secondQuestionController.stream;

  /// Retrieves the stream value in the streamController of the  third question
  get thirdQuestion => _thirdQuestionController.stream;

  Future submit() async {
    final response1 = await _firstQuestionController.stream.value;
    final response2 = await _secondQuestionController.stream.value;
    final response3 = await _thirdQuestionController.stream.value;
    try {
      // Send request
      await _userRepository.profiling(ProfilingModel([
        Response(question: 1, answer: response1 != null ? response1 : 1),
        Response(question: 2, answer: response2 != null ? response2 : 1),
        Response(question: 3, answer: response3 != null ? response3 : 1)
      ]).encodeToJson());
      return true;
    } catch (e) {
      var error = e;
      if (e?.message == null || e?.message == '')
        error = new CreaturError(
            type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
      throw error;
    }
  }

  /// Disposes the stream controllers closing them
  void dispose() {
    _firstQuestionController.close();
    _secondQuestionController.close();
    _thirdQuestionController.close();
  }
}
