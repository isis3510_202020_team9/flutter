import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/repositories/product_repository.dart';
import 'package:rxdart/rxdart.dart';

import 'create-venture_validators.dart';

class CreateProductBloc with VentureValidator {
  /// The name controller behaviorSubject
  final BehaviorSubject _productNameController = BehaviorSubject<String>();

  /// The phone controller behaviorSubject
  final BehaviorSubject _productPrizeController = BehaviorSubject<String>();

  /// The address controller behaviorSubject
  final BehaviorSubject _productDescriptionController =
      BehaviorSubject<String>();

  /// The instagram user controller behaviorSubject
  final BehaviorSubject _productCategoryController = BehaviorSubject<String>();

  final ProductRepository _productRepository = ProductRepository();

  /// changes and retrieves the value in the bloc for the name
  Function(String) get changeProductName => _productNameController.sink.add;

  Stream<String> get productName => _productNameController.stream.value;

  Function(String) get changeProductPrize => _productPrizeController.sink.add;

  Stream<String> get productPrize => _productPrizeController.stream.value;

  Function(String) get changeProductCategory =>
      _productCategoryController.sink.add;

  Stream<String> get productCategory => _productCategoryController.stream.value;

  Function(String) get changeProductDescription =>
      _productDescriptionController.sink.add;

  Stream<String> get productDescription =>
      _productDescriptionController.stream.value;

  get productNameStream =>
      _productNameController.stream.transform(validateName);

  get productPrizeStream =>
      _productPrizeController.stream.transform(validatePrize);

  get productDescriptionStream =>
      _productDescriptionController.stream.transform(validateDescription);

  get productCategorytream =>
      _productCategoryController.stream.transform(validateVentureCategory);

  Stream<bool> get formValidStream => Rx.combineLatest4(
      productNameStream,
      productPrizeStream,
      productDescriptionStream,
      productCategorytream,
      (
        productNameStream,
        productPrizeStream,
        productDescriptionStream,
        productCategorytream,
      ) =>
          true);

  // Create product bloc
  Future<ProductModel> createProduct() async {
    final name = _productNameController.value;
    final prize = _productPrizeController.value;
    final description = _productDescriptionController.value;
    final category = _productCategoryController.value;

    return await _productRepository.createProduct({
      "name": name,
      "prize": int.parse(prize),
      "description": description,
      "productCategory": category,
    });
  }

  // Save answers in shared preferences
  Future<void> saveAnswers() async {
    final name = _productNameController.value;
    final prize = _productPrizeController.value;
    final description = _productDescriptionController.value;
    final category = _productCategoryController.value;

    return await _productRepository.saveAnswers({
      "name": name,
      "prize": prize,
      "description": description,
      "category": [category],
    });
  }

  // Refresh answers
  Future refreshAnswers() async {
    final answers = await _productRepository.refreshAnswers();
    if (answers != null && answers.isNotEmpty) {
      if (answers['name'] != null && answers['name'] != '')
        changeProductName(answers['name']);
      if (answers['description'] != null && answers['description'] != '')
        changeProductDescription(answers['description']);
      if (answers['prize'] != null && answers['prize'] != '')
        changeProductPrize(answers['prize']);
      if (answers['category'][0] != null && answers['category'][0] != '')
        changeProductCategory(answers['category'][0]);
    }
    return answers ?? '';
  }

  Future<void> dispose() async {
    await _productNameController.drain();
    _productNameController.close();
    _productPrizeController.close();
    _productDescriptionController.close();
    _productCategoryController.close();
  }
}
