import 'package:creatur/src/blocs/venture/create-venture_validators.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/repositories/venture_repository.dart';
import 'package:rxdart/rxdart.dart';

class CreateVentureBloc with VentureValidator {
  /// The name controller behaviorSubject
  final BehaviorSubject _ventureNameController = BehaviorSubject<String>();

  /// The phone controller behaviorSubject
  final BehaviorSubject _venturePhoneController = BehaviorSubject<String>();

  /// The address controller behaviorSubject
  final BehaviorSubject _ventureAddressController = BehaviorSubject<String>();

  /// The facebook user controller behaviorSubject
  final BehaviorSubject _ventureFacebookController = BehaviorSubject<String>();

  /// The instagram user controller behaviorSubject
  final BehaviorSubject _ventureInstagramController = BehaviorSubject<String>();

  /// The instagram user controller behaviorSubject
  final BehaviorSubject _ventureCategoryController = BehaviorSubject<String>();

  final VentureRepository _ventureRepository = VentureRepository();

  /// changes and retrieves the value in the bloc for the name
  Function(String) get changeVentureName => _ventureNameController.sink.add;

  Stream<String> get ventureName => _ventureNameController.stream.value;

  Function(String) get changeVenturePhone => _venturePhoneController.sink.add;

  Stream<String> get venturePhone => _venturePhoneController.stream.value;

  Function(String) get changeVentureAddress =>
      _ventureAddressController.sink.add;

  Stream<String> get ventureAddress => _ventureAddressController.stream.value;

  Function(String) get changeVentureFacebook =>
      _ventureFacebookController.sink.add;

  Stream<String> get ventureFacebook => _ventureFacebookController.stream.value;

  Function(String) get changeVentureInstagram =>
      _ventureInstagramController.sink.add;

  Stream<String> get ventureInstagram =>
      _ventureInstagramController.stream.value;

  Function(String) get changeVentureCategory =>
      _ventureCategoryController.sink.add;

  Stream<String> get ventureCategory => _ventureCategoryController.stream.value;

  get ventureNameStream =>
      _ventureNameController.stream.transform(validateName);

  get venturePhoneStream =>
      _venturePhoneController.stream.transform(validateVenturePhone);

  get ventureAddressStream =>
      _ventureAddressController.stream.transform(validateVentureAddress);

  get ventureFacebookStream =>
      _ventureFacebookController.stream.transform(validateFacebookUser);

  get ventureInstagramStream =>
      _ventureInstagramController.stream.transform(validateInstagramUser);

  get ventureCategorytream =>
      _ventureCategoryController.stream.transform(validateVentureCategory);

  Stream<bool> get formValidStream => Rx.combineLatest6(
      ventureNameStream,
      venturePhoneStream,
      ventureAddressStream,
      ventureFacebookStream,
      ventureInstagramStream,
      ventureCategorytream,
      (
        ventureNameStream,
        venturePhoneStream,
        ventureAddressStream,
        ventureFacebookStream,
        ventureInstagramStream,
        ventureCategorytream,
      ) =>
          true);

  // Create venture
  Future<VentureModel> createVenture() async {
    final name = _ventureNameController.value;
    final phone = _venturePhoneController.value;
    final address = _ventureAddressController.value;
    final facebook = _ventureFacebookController.value;
    final instagram = _ventureInstagramController.value;
    final category = _ventureCategoryController.value;

    return await _ventureRepository.createVenture({
      "name": name,
      "phone": phone,
      "address": address,
      "facebookUrl": "https://www.facebook.com/$facebook/",
      "instagramUrl": "https://www.instagram.com/$instagram/",
      "categories": [category],
    });
  }

  /// Save venture data
  Future<void> saveVentureData() async {
    final name = _ventureNameController.value;
    final phone = _venturePhoneController.value;
    final address = _ventureAddressController.value;
    final facebook = _ventureFacebookController.value;
    final instagram = _ventureInstagramController.value;
    final category = _ventureCategoryController.value;

    await _ventureRepository.saveVentureAnswers({
      "name": name,
      "phone": phone,
      "address": address,
      "facebookUrl": "https://www.facebook.com/$facebook/",
      "instagramUrl": "https://www.instagram.com/$instagram/",
      "categories": [category],
    });
  }

  // Refresh answers
  Future refreshAnswers() async {
    final answers = await _ventureRepository.refreshAnswers();
    if (answers != null && answers.isNotEmpty) {
      changeVentureName(answers['name']);
      changeVenturePhone(answers['phone']);
      changeVentureAddress(answers['address']);
      changeVentureFacebook(answers['facebookUrl']);
      changeVentureInstagram(answers['instagramUrl']);
      changeVentureFacebook(answers['categories'][0]);
    }
    return answers ?? '';
  }

  void dispose() {
    _ventureNameController.close();
    _venturePhoneController.close();
    _ventureAddressController.close();
    _ventureFacebookController.close();
    _ventureInstagramController.close();
    _ventureCategoryController.close();
  }
}
