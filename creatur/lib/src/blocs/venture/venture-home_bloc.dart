import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/repositories/order_repository.dart';
import 'package:creatur/src/repositories/user_repository.dart';
import 'package:creatur/src/repositories/venture_repository.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

/// The bloc of the venture admin stuff
class VentureHomeBloc {
  /// The venture of the user
  VentureModel _userVenture;

  /// Indicates if the products are being loaded
  bool _isLoadingProducts;

  /// Indicates if the orders are being loaded
  bool _isLoadingOrders;

  /// The list of products of the venture
  List<ProductModel> _ventureProductsList;

  /// The list of products of the venture
  List<OrderModel> _ventureOrdersList;

  /// The repository of the user to access the requests
  UserRepository _userRepository;

  /// The repository of the orders to access the requests
  OrderRepository _orderRepository;

  /// The repository of the venture to access the requests
  VentureRepository _ventureRepository;

  /// The venture's products stream
  final PublishSubject<List<ProductModel>> _productsController =
      PublishSubject<List<ProductModel>>();

  /// The venture's orders stream
  final PublishSubject<List<OrderModel>> _ordersController =
      PublishSubject<List<OrderModel>>();

  /// The sink to add the list of product in the stream
  Function(List<ProductModel>) get changeProducts =>
      _productsController.sink.add;

  /// The sink to add the list of orders in the stream
  Function(List<OrderModel>) get changeOrders => _ordersController.sink.add;

  /// The stream of the venture's products
  Stream<List<ProductModel>> get ventureProductsStream =>
      _productsController.stream;

  /// The stream of the venture's orders
  Stream<List<OrderModel>> get ventureOrdersStream => _ordersController.stream;

  /// The default named constructor
  VentureHomeBloc() {
    _isLoadingProducts = false;
    _isLoadingOrders = false;
    _ventureProductsList = List();
    _ventureOrdersList = List();
    _userRepository = UserRepository();
    _ventureRepository = VentureRepository();
    _orderRepository = OrderRepository();
  }

  /// The future with the venture of the user
  Future<VentureModel> get venture => _retrieveMyVenture();

  /// Tells if he user has a venture or not
  bool get ownsVenture => _userRepository.role == AppConstants.VENTURE_ROLE;

  /// Retrieves the venture of the user
  Future<VentureModel> _retrieveMyVenture() async {
    try {
      final VentureModel venture = await _userRepository.getMyVenture();
      _userVenture = venture;
      return _userVenture;
    } catch (e) {
      print("ERROR MY_VENTURE -> $e");
      throw CreaturError(
          type: CreaturErrorType.CONNECTION,
          message: e?.message ?? AppErrors.VENTURE_HOME_ERROR);
    }
  }

  /// Loads the venture products from the DB and sends them to the stream
  Future<List<ProductModel>> getVentureProducts() async {
    if (_isLoadingProducts) return [];
    _isLoadingProducts = true;
    try {
      final List<ProductModel> response =
          await _ventureRepository.getVentureProducts(_userVenture?.id, true);
      _ventureProductsList = response;
      changeProducts(_ventureProductsList);
      _isLoadingProducts = false;
      return response;
    } catch (e) {
      _isLoadingProducts = false;
      _productsController.sink.addError(AppErrors.VENTURE_PRODUCTS_ERROR);
      return null;
    }
  }

  /// Loads the venture orders from the DB and sends them to the stream
  Future<List<OrderModel>> getVentureOrders() async {
    if (_isLoadingOrders) return [];
    _isLoadingOrders = true;
    try {
      final List<OrderModel> response =
          await _orderRepository.getVentureOrders();
      _ventureOrdersList = response;
      changeOrders(_ventureOrdersList);
      _isLoadingOrders = false;
      return response;
    } catch (e) {
      _isLoadingOrders = false;
      _ordersController.sink.addError(AppErrors.VENTURE_ORDERS_ERROR);
      return null;
    }
  }

  /// Deletes a product from the venture that owns it
  Future<bool> deleteProduct(String productId) async {
    try {
      final bool deleted =
          await _ventureRepository.deleteVentureProduct(productId);
      if (deleted) {
        _ventureProductsList.removeWhere((element) => element.id == productId);
        changeProducts(_ventureProductsList);
        return deleted;
      } else {
        throw Exception();
      }
    } catch (e) {
      print("ERROR DELETING -> $e");
      throw CreaturError(
          type: CreaturErrorType.CONNECTION,
          message: e?.message ?? AppErrors.CONNECTION);
    }
  }

  /// Dispose the streams to close them
  void dispose() {
    _productsController.close();
    _ordersController.close();
  }
}
