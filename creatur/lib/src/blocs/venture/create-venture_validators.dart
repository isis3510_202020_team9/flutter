import 'dart:async';

import 'package:creatur/src/utils/validation/validation.dart';

//
class VentureValidator {
  final validateName =
      StreamTransformer<String, String>.fromHandlers(handleData: (name, sink) {
    if (name.length > 20)
      sink.addError('El nombre no puede superar 20 caracteres.');
    else
      sink.add(name);
  });

  final validateVenturePhone =
      StreamTransformer<String, String>.fromHandlers(handleData: (phone, sink) {
    if (double.tryParse(phone) == null) {
      sink.addError(
          'No se aceptan caracteres diferentes a numeros para el teléfono');
    } else if (phone.length > 11)
      sink.addError('El teléfono no puede superar 10 dígitos.');
    else
      sink.add(phone);
  });

  final validateVentureAddress = StreamTransformer<String, String>.fromHandlers(
      handleData: (address, sink) {
    isFilled(address)
        ? sink.add(address)
        : sink.addError('No puedes dejar este campo vacío');
  });

  final validateFacebookUser = StreamTransformer<String, String>.fromHandlers(
      handleData: (facebook, sink) {
    isFilled(facebook)
        ? sink.add(facebook)
        : sink.addError('No puedes dejar este campo vacío');
  });

  final validateInstagramUser =
      StreamTransformer<String, String>.fromHandlers(handleData: (ig, sink) {
    isFilled(ig)
        ? sink.add(ig)
        : sink.addError('No puedes dejar este campo vacío');
  });

  final validateVentureCategory =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (category, sink) {
    isFilled(category)
        ? sink.add(category)
        : sink.addError('No puedes dejar este campo vacío');
  });

  final validatePrize =
      StreamTransformer<String, String>.fromHandlers(handleData: (prize, sink) {
    if (double.tryParse(prize) == null) {
      sink.addError(
          'No se aceptan caracteres diferentes a numeros para el precio');
    } else
      sink.add(prize);
  });

  final validateDescription = StreamTransformer<String, String>.fromHandlers(
      handleData: (category, sink) {
    isFilled(category)
        ? sink.add(category)
        : sink.addError('No puedes dejar este campo vacío');
  });
}
