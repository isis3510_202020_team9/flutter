import 'package:creatur/src/blocs/start_bloc.dart';
import 'package:flutter/widgets.dart';

/// Provider class for having the login Bloc accessible on the views
class BlocProvider extends InheritedWidget {
  /// The StartBloc reference
  final StartBloc _startBloc = StartBloc();

  /// The default named Provider constructor
  BlocProvider({Key key, Widget child}) : super(key: key, child: child);

  /// Indicates wether and update on the widget should notify the provider
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  /// StartBloc getter
  StartBloc get startBloc => _startBloc;

  /// Provides the Start Bloc in the context of the child widgets
  static StartBloc of(BuildContext context) =>
      (context.dependOnInheritedWidgetOfExactType<BlocProvider>()).startBloc;
}
