import 'package:creatur/src/blocs/activity/activity_bloc.dart';
import 'package:creatur/src/blocs/activity/order-review_bloc.dart';
import 'package:creatur/src/blocs/connectivity_bloc.dart';
import 'package:creatur/src/blocs/detail/venture-detail_bloc.dart';
import 'package:creatur/src/blocs/login/login_bloc.dart';
import 'package:creatur/src/blocs/order/complete-order_bloc.dart';
import 'package:creatur/src/blocs/order/order-status_bloc.dart';
import 'package:creatur/src/blocs/order/order_bloc.dart';
import 'package:creatur/src/blocs/profiling/profiling_bloc.dart';
import 'package:creatur/src/blocs/register/register_bloc.dart';
import 'package:creatur/src/blocs/recommended/recommended_bloc.dart';
import 'package:creatur/src/blocs/venture/create-product_bloc.dart';
import 'package:creatur/src/blocs/venture/create-venture_bloc.dart';
import 'package:creatur/src/blocs/categories/category_bloc.dart';

/// Class which provides the login and registration blocs
class StartBloc {
  /// Singleton instance for the startBloc
  static final StartBloc _instance = StartBloc._createInstance();

  /// The login bloc
  LoginBloc _loginBloc;

  ///The registration bloc
  RegisterBloc _registerBloc;

  ///The recommended Bloc
  RecommendedBloc _recommendedBloc;

  /// The profiling bloc
  ProfilingBloc _profilingBloc;

  /// The eventual Connectivity Bloc
  ConnectivityBloc _connectivityBloc;

  /// The create venture bloc
  CreateVentureBloc _createVentureBloc;

  /// The create product bloc
  CreateProductBloc _createProductBloc;

  /// The venture detail bloc
  VentureDetailBloc _ventureDetailBloc;

  /// The complete order bloc
  CompleteOrderBloc _completeOrderBloc;

  /// Order bloc
  OrderBloc _orderBloc;

  /// Change status
  OrderStatusBloc _orderStatusBloc;

  /// User orders activity
  ActivityBloc _activityBloc;

  /// Category prods & ventures
  CategoryBloc _categoryBloc;

  /// Order review bloc
  OrderReviewBloc _orderReviewBloc;

  /// Default constructor of the class
  StartBloc._createInstance() {
    _loginBloc = LoginBloc();
    _registerBloc = RegisterBloc();
    _recommendedBloc = RecommendedBloc();
    _profilingBloc = ProfilingBloc();
    _connectivityBloc = ConnectivityBloc();
    _createVentureBloc = CreateVentureBloc();
    _createProductBloc = CreateProductBloc();
    _ventureDetailBloc = VentureDetailBloc();
    _completeOrderBloc = CompleteOrderBloc();
    _orderBloc = OrderBloc();
    _orderStatusBloc = OrderStatusBloc();
    _activityBloc = ActivityBloc();
    _categoryBloc = CategoryBloc();
    _orderReviewBloc = OrderReviewBloc();
  }

  /// Singleton retrieval for the class
  factory StartBloc() => _instance;

  /// loginBloc getter
  LoginBloc get loginBloc => _loginBloc;

  /// RegisterBloc getter
  RegisterBloc get registerBloc => _registerBloc;

  /// RecommendedBloc getter
  RecommendedBloc get recommendedBloc => _recommendedBloc;

  /// ProfilingBloc getter
  ProfilingBloc get profilingBloc => _profilingBloc;

  /// The connectivity bloc getter
  ConnectivityBloc get connectivityBloc => _connectivityBloc;

  /// The create venture bloc getter
  CreateVentureBloc get createVentureBloc => _createVentureBloc;

  /// The create venture bloc getter
  CreateProductBloc get createProductBloc => _createProductBloc;

  /// The venture detail bloc getter
  VentureDetailBloc get ventureDetailBloc => _ventureDetailBloc;

  /// The complete order bloc getter
  CompleteOrderBloc get orderBloc => _completeOrderBloc;

  /// The order bloc getter
  OrderBloc get orderDetailBloc => _orderBloc;

  /// The order status bloc getter
  OrderStatusBloc get orderStatusBloc => _orderStatusBloc;

  /// the activity bloc getter
  ActivityBloc get activityBloc => _activityBloc;

  /// The category bloc getter
  CategoryBloc get categoryBloc => _categoryBloc;

  /// the review bloc getter
  OrderReviewBloc get reviewBloc => _orderReviewBloc;
}
