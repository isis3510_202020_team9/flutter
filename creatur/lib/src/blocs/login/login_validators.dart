import 'dart:async';

/// Login validator class for managing the login form restrictions
class LoginValidator {
  /// Validator for password
  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
    if (password.length < 6)
      sink.addError('La contraseña debe tener mínimo 6 caracteres.');
    else
      sink.add(password);
  });

  // Validator for email
  final validateEmail = StreamTransformer<String, String>.fromHandlers(
      handleData: (changeEmail, sink) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (regExp.hasMatch(changeEmail))
      sink.add(changeEmail);
    else
      sink.addError('Email incorrecto.');
  });
}
