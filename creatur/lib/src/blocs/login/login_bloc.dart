import 'package:creatur/src/blocs/login/login_validators.dart';
import 'package:creatur/src/models/login_model.dart';
import 'package:creatur/src/repositories/user_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

/// Class that defines the Bloc logic
class LoginBloc with LoginValidator {
  /// The emailController behaviorSubject
  final BehaviorSubject _emailController = BehaviorSubject<String>();

  /// The passwordController behaviorSubject
  final BehaviorSubject _passwordController = BehaviorSubject<String>();

  /// The repository reference for the logIn
  final UserRepository _userRepository = UserRepository();

  /// changes and retrieves the value in the bloc for the email
  Function(String) get changeEmail => _emailController.sink.add;

  /// changes and retrieves the value in the bloc for the password
  Function(String) get changePassword => _passwordController.sink.add;

  /// Retrieves the stream value in the streamController of the email
  Stream<String> get email => _emailController.stream.value;

  /// Retrieves the stream value in the streamController of the password
  Stream<String> get password => _passwordController.stream.value;

  // Get stream
  get emailStream => _emailController.stream.transform(validateEmail);
  get passwordStream => _passwordController.stream.transform(validatePassword);

  /// Validate if format is valid
  Stream<bool> get loginValidStream => Rx.combineLatest2(
      emailStream, passwordStream, (emailStream, passwordStream) => true);

  /// Submits the values from the email and passwords fields
  Future<bool> submit() async {
    final validEmail = _emailController.value;
    final validPassword = _passwordController.value;
    try {
      return await _userRepository
          .logIn(LoginModel(validEmail, validPassword).encodeToJson());
    } catch (e) {
      var error = e;
      if (e?.message == null || e?.message == '')
        error = new CreaturError(
            type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
      throw error;
    }
  }

  /// Disposes the stream controllers closing them
  void dispose() {
    _emailController.close();
    _passwordController.close();
  }
}
