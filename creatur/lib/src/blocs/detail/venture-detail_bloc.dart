import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/repositories/venture_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

/// The class that contains the logic of the venture detail
class VentureDetailBloc {
  /// Checks if the request for products is loading
  bool _isLoadingProducts;

  /// The list of suggested products
  List<ProductModel> _ventureProducts;

  /// The venture repository to access all the funcionalities
  VentureRepository _ventureRepository;

  /// The venture's products stream
  final PublishSubject<List<ProductModel>> _productsController =
      PublishSubject<List<ProductModel>>();

  /// The sink to add the list of product in the stream
  Function(List<ProductModel>) get changeProducts =>
      _productsController.sink.add;

  /// The stream of the venture's products
  Stream<List<ProductModel>> get ventureProductsStream =>
      _productsController.stream;

  /// Creates the venture detail bloc
  VentureDetailBloc() {
    _isLoadingProducts = false;
    _ventureProducts = List();
    _ventureRepository = VentureRepository();
  }

  /// Loads the venture's products from the DB and sends them to the stream
  Future<List<ProductModel>> getVentureProducts(String ventureId) async {
    if (_isLoadingProducts) return [];
    _isLoadingProducts = true;
    try {
      final List<ProductModel> response =
          await _ventureRepository.getVentureProducts(ventureId, false);
      _ventureProducts = response;
      changeProducts(_ventureProducts);
      _isLoadingProducts = false;
      return response;
    } catch (e) {
      _isLoadingProducts = false;
      _productsController.sink.addError(AppErrors.VENTURE_PRODUCTS_ERROR);
      return null;
    }
  }

  /// Dispose the streams to close them
  void dispose() {
    _productsController.close();
  }
}
