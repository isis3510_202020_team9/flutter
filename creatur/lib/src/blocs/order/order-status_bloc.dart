import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/repositories/order_repository.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

class OrderStatusBloc {
  String _statusField = AppConstants.STATUS_NOT_STARTED;

  OrderRepository _orderRepository;

  OrderStatusBloc() {
    _orderRepository = OrderRepository();
  }

  /// The order status controller behaviorSubject
  final BehaviorSubject _statusController = BehaviorSubject<String>();

  /// Change order status
  changeStatus(String sta) {
    if (sta != null) {
      _statusField = sta;
      _statusController.sink.add(_statusField);
      return;
    }
    var index = AppConstants.statusList.indexOf(_statusField);
    _statusField = AppConstants.statusList[++index];
    _statusController.sink.add(_statusField);
  }

  rejectStatus() => _statusController.sink.add(AppConstants.STATUS_DECLINED);

  Stream<String> get status => _statusController.stream.value;

  get statusStream => _statusController.stream;

  /// Change order status
  Future<OrderModel> changeOrderStatus(String orderId) async {
    final status = _statusController.value;

    try {
      return await _orderRepository.changeOrderStatus(orderId, status);
    } catch (e) {
      var error = e;
      if (e?.message == null || e?.message == '')
        error = new CreaturError(
            type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
      _revertStatus();
      throw error;
    }
  }

  /// Reverts the status of an order
  _revertStatus() {
    var index = AppConstants.statusList.indexOf(_statusField);
    _statusField = AppConstants.statusList[--index];
    _statusController.sink.add(_statusField);
  }

  void dispose() {
    _statusController.close();
  }
}
