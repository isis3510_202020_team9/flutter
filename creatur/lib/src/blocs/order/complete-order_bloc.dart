import 'package:creatur/src/blocs/order/order_validator.dart';
import 'package:creatur/src/models/create_order_model.dart';
import 'package:creatur/src/repositories/order_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

class CompleteOrderBloc with OrderValidator {
  int _counter = 0;

  /// The address controller behaviorSubject
  final BehaviorSubject _addressController = BehaviorSubject<String>();

  /// The date controller behaviorSubject
  final BehaviorSubject _dateController = BehaviorSubject<DateTime>();

  // The product units controller behaviorSubjec
  final BehaviorSubject _unitsController = BehaviorSubject<int>();

  final OrderRepository _orderRepository = OrderRepository();

  Function(String) get changeAddress => _addressController.sink.add;

  Stream<String> get address => _addressController.stream.value;

  Function(DateTime) get changeDate => _dateController.sink.add;

  Stream<DateTime> get date => _dateController.stream.value;

  incrementUnits() => _unitsController.sink.add(++_counter);

  decrementUnits() => _unitsController.sink.add(--_counter);

  Stream<int> get units => _unitsController.stream.value;

  get addressStream => _addressController.stream.transform(validateAddress);

  get dateStream => _dateController.stream.transform(validateDate);

  get unitsStream => _unitsController.stream.transform(validateUnits);

  get unitsCurrentValue => _unitsController.value;

  Stream<bool> get formValidStream => Rx.combineLatest3(
      addressStream,
      dateStream,
      unitsStream,
      (
        addressStream,
        dateStream,
        unitsStream,
      ) =>
          true);

  // Creates an order to buy
  Future<Map<String, dynamic>> createOrder(
      String productId, String ventureId) async {
    final address = _addressController.value;
    final date = _dateController.value;
    final units = _unitsController.value;
    try {
      return await _orderRepository.createOrder(CreateOrderModel(
          address,
          date.toIso8601String(),
          ventureId,
          [ProductUnits(id: productId, units: units)]).encodeToJson());
    } catch (e) {
      var error = e;
      if (e?.message == null || e?.message == '')
        error = new CreaturError(
            type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
      throw error;
    }
  }

  void dispose() {
    _addressController.close();
    _dateController.close();
    _unitsController.close();
  }
}
