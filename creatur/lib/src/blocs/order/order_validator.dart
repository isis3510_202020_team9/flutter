import 'dart:async';

import 'package:creatur/src/utils/validation/validation.dart';

class OrderValidator {
  final validateAddress = StreamTransformer<String, String>.fromHandlers(
      handleData: (address, sink) {
    isFilled(address)
        ? sink.add(address)
        : sink.addError('No puedes dejar este campo vacío');
  });

  final validateDate = StreamTransformer<DateTime, DateTime>.fromHandlers(
      handleData: (date, sink) {
    date != null
        ? sink.add(date)
        : sink.addError('No puedes dejar este campo vacío');
  });

  final validateUnits =
      StreamTransformer<int, int>.fromHandlers(handleData: (units, sink) {
    if (units > 0)
      sink.add(units);
    else
      sink.addError('Tienes que comprar mas de una unidad');
  });
}
