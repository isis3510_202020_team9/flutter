import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/repositories/order_repository.dart';
import 'package:rxdart/rxdart.dart';

class OrderBloc {
  OrderRepository _orderRepository;

  OrderBloc() {
    _orderRepository = OrderRepository();
  }

  final PublishSubject<OrderModel> _orderController =
      PublishSubject<OrderModel>();

  Function(OrderModel) get changeOrder => _orderController.sink.add;

  Stream<OrderModel> get orderStream => _orderController.stream;

  Future<OrderModel> getOrder(String orderId) async {
    try {
      final OrderModel order = await _orderRepository.getOrder(orderId);
      changeOrder(order);
      return order;
    } catch (error) {
      return null;
    }
  }

  void dispose() {
    _orderController.close();
  }
}
