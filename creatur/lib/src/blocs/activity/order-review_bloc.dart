import 'package:creatur/src/repositories/order_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

class OrderReviewBloc {
  /// The review answer controller
  final BehaviorSubject _reviewController = BehaviorSubject<int>();

  Function(int) get changeReview => _reviewController.sink.add;

  OrderRepository _orderRepository = OrderRepository();

  /// Retrieves the stream value in the streamController of the review question
  get gradeStream => _reviewController.stream;

  Future createReview(String orderId) async {
    final grade = await _reviewController.stream.value;

    try {
      // Send request
      return await _orderRepository.createReview(orderId, grade);
    } catch (e) {
      var error = e;
      if (e?.message == null || e?.message == '')
        error = new CreaturError(
            type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
      throw error;
    }
  }

  /// Disposes the stream controllers closing them
  void dispose() {
    _reviewController.close();
  }
}
