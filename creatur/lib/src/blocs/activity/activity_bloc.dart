import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/repositories/order_repository.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:rxdart/rxdart.dart';

/// The bloc of the activity of the user
class ActivityBloc {
  /// Indicates if the orders are being loaded
  bool _isLoadingOrders;

  /// The list of orders of the user
  List<OrderModel> _userOrdersList;

  /// The repository of the orders to access the requests
  OrderRepository _orderRepository;

  /// The user's orders stream
  final PublishSubject<List<OrderModel>> _ordersController =
      PublishSubject<List<OrderModel>>();

  /// The sink to add the list of orders in the stream
  Function(List<OrderModel>) get changeOrders => _ordersController.sink.add;

  /// The stream of the user's orders
  Stream<List<OrderModel>> get userOrdersStream => _ordersController.stream;

  /// The default named constructor
  ActivityBloc() {
    _isLoadingOrders = false;
    _userOrdersList = List();
    _orderRepository = OrderRepository();
  }

  /// Loads the user orders from the DB and sends them to the stream
  Future<List<OrderModel>> getUserOrders() async {
    if (_isLoadingOrders) return [];
    _isLoadingOrders = true;
    try {
      final List<OrderModel> response = await _orderRepository.getUserOrders();
      _userOrdersList = response;
      changeOrders(_userOrdersList);
      _isLoadingOrders = false;
      return response;
    } catch (e) {
      _isLoadingOrders = false;
      _ordersController.sink.addError(AppErrors.USER_ORDERS_ERROR);
      return null;
    }
  }

  /// Dispose the streams to close them
  void dispose() {
    _ordersController.close();
  }
}
