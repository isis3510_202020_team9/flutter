import 'package:creatur/src/blocs/business_bloc.dart';
import 'package:flutter/widgets.dart';

/// Provider class for having the login Bloc accessible on the views
class VentureProvider extends InheritedWidget {
  /// The StartBloc reference
  final BusinessBloc _businessBloc = BusinessBloc();

  /// The default named Provider constructor
  VentureProvider({Key key, Widget child}) : super(key: key, child: child);

  /// Indicates wether and update on the widget should notify the provider
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  /// StartBloc getter
  BusinessBloc get businessBloc => _businessBloc;

  /// Provides the Start Bloc in the context of the child widgets
  static BusinessBloc of(BuildContext context) =>
      (context.dependOnInheritedWidgetOfExactType<VentureProvider>())
          .businessBloc;
}
