import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/routes.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

/// The main widget for the application
class App extends StatelessWidget {
  /// The overriden build method
  @override
  Widget build(BuildContext context) => BlocProvider(
        child: MaterialApp(
          theme: ThemeData.light(),
          color: AppColors.PRIMARY_COLOR,
          title: 'Creatur',
          initialRoute: '/',
          routes: creaturRoutes,
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('es', 'CO'),
          ],
        ),
      );
}
