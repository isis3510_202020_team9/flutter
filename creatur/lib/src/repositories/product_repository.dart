import 'dart:convert';

import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/providers/database/product_db.dart';
import 'package:creatur/src/providers/product_api_provider.dart';
import 'package:creatur/src/repositories/repository.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';

/// The repository class that consumes the provider api of the products
class ProductRepository extends Repository {
  /// The instance of the product Api provider
  final _productApiProvider = ProductApiProvider();

  /// The local sqlite database for products
  final _productLocalDb = ProductDB();

  /// Retrieves and transforms the products suggested using the provider
  Future<List<ProductModel>> getSuggestedProducts(int page) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      return _productLocalDb.getAllProducts(AppConstants.RECOMMENDED_SECTION,
          offset: (page - 1) * 20);
    } else {
      List<ProductModel> suggested =
          await _productApiProvider.getSuggestedProducts(page, token);

      /// Insert the retrieved ones into sqlite
      _productLocalDb.insertAllProducts(
          suggested, AppConstants.RECOMMENDED_SECTION);

      return suggested;
    }
  }

  /// Retrieves and transforms the trendy products using the provider
  Future<List<ProductModel>> getTrendy() async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      return _productLocalDb.getAllProducts(AppConstants.TENDENCIES_SECTION);
    } else {
      List<ProductModel> trendy =
          await _productApiProvider.getTrendyProducts(token);

      /// Insert the retrieved ones into sqlite
      _productLocalDb.insertAllProducts(
          trendy, AppConstants.TENDENCIES_SECTION);

      return trendy;
    }
  }

  /// Retrieves and transforms the category products using the provider
  Future<List<ProductModel>> getCategoryProducts(String category) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      return _productLocalDb.getAllProducts(AppConstants.CATEGORIES_SECTION);
    } else {
      List<ProductModel> categoryP =
          await _productApiProvider.getCategoryProducts(token, category);

      /// Insert the retrieved ones into sqlite
      _productLocalDb.insertAllProducts(
          categoryP, AppConstants.CATEGORIES_SECTION);

      return categoryP;
    }
  }

  /// Create a product
  Future<ProductModel> createProduct(Map<String, dynamic> payload) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final response = await _productApiProvider.createProduct(payload, token);
      await prefs.clearKey(AppConstants.CREATE_PRODUCT_FORM);
      return response;
    }
  }

  /// Save answers
  Future<void> saveAnswers(Map<String, dynamic> payload) async {
    await prefs.setKey(AppConstants.CREATE_PRODUCT_FORM, jsonEncode(payload));
  }

  /// Refresh answers
  Future<Map<String, dynamic>> refreshAnswers() async {
    final answers = prefs.getKey(AppConstants.CREATE_PRODUCT_FORM);
    if (answers != null && answers.isNotEmpty) return jsonDecode(answers);
    return null;
  }
}
