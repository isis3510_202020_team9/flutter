import 'dart:async';

import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/providers/database/venture_db.dart';
import 'package:creatur/src/providers/user_api_provider.dart';
import 'package:creatur/src/repositories/repository.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';

/// The repository class to consume the provider api access
class UserRepository extends Repository {
  /// The instance of the user api provider
  final UserApiProvider _userApiProvider;

  /// The local Dbs to acces information w/out connectivity
  final VentureDB _ventureLocalDb;

  UserRepository()
      : _userApiProvider = UserApiProvider(),
        _ventureLocalDb = VentureDB();

  /// Uses the provider to log in into the application
  Future<bool> logIn(Map<String, dynamic> credentials) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final response = await _userApiProvider.logIn(credentials);
      token = response['token'];
      role = response['role'];
      hasProfiling = response['hasProfiling'] as bool;
      return response['hasProfiling'];
    }
  }

  /// Uses the provider to register into the application
  Future<bool> register(Map<String, dynamic> credentials) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final tokenId = await _userApiProvider.register(credentials);
      role = AppConstants.USER_ROLE;
      token = tokenId;
      return true;
    }
  }

  /// Uses the provider to profile the user
  Future profiling(Map<String, dynamic> answers) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final response = await _userApiProvider.profiling(answers, token);
      hasProfiling = true;
      return response;
    }
  }

  /// Retrieves the venture of the user given by the provider
  Future<VentureModel> getMyVenture() async {
    final hasInternet = await checkConnectivity();
    final VentureModel userVenture = await _ventureLocalDb.getUserVenture();
    if (userVenture != null) {
      return userVenture;
    } else if (!hasInternet) {
      throw CreaturError(
          type: CreaturErrorType.CONNECTION,
          message: AppErrors.VENTURE_HOME_ERROR);
    } else {
      final VentureModel response = await _userApiProvider.getMyVenture(token);

      /// Save into the localDB
      _ventureLocalDb.insertVenture(
          response, AppConstants.USER_VENTURE_SECTION);

      return response;
    }
  }
}
