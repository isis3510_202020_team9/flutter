import 'dart:convert';

import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/providers/database/product_db.dart';
import 'package:creatur/src/providers/database/venture_db.dart';
import 'package:creatur/src/providers/venture_api_provider.dart';
import 'package:creatur/src/repositories/repository.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/utils/geolocation/geolocator_bloc.dart';

/// Represent a venture repository
class VentureRepository extends Repository {
  /// The instance of the product Api provider
  final VentureApiProvider _ventureApiProvider;

  /// The local sqlite database for ventures and products
  final VentureDB _ventureLocalDb;
  final ProductDB _productLocalDb;

  /// The default constructor for the class
  VentureRepository()
      : this._ventureApiProvider = VentureApiProvider(),
        this._ventureLocalDb = VentureDB(),
        this._productLocalDb = ProductDB();

  /// Retrieves and transforms the ventures suggested using the provider
  Future<List<VentureModel>> getSuggestedVentures() async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      return _ventureLocalDb.getAllVentures(AppConstants.RECOMMENDED_SECTION);
    } else {
      List<VentureModel> suggested =
          await _ventureApiProvider.getSuggestedVentures(token);

      /// Insert retrieved data into sqlite
      _ventureLocalDb.insertAllVentures(
          suggested, AppConstants.RECOMMENDED_SECTION);

      return suggested;
    }
  }

  /// Retrieves and transforms the near ventures & products using the provider
  Future<Map<String, List>> getNear() async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      List<ProductModel> nearProducts =
          await _productLocalDb.getAllProducts(AppConstants.NEAR_SECTION);
      List<VentureModel> nearVentures =
          await _ventureLocalDb.getAllVentures(AppConstants.NEAR_SECTION);

      return {'products': nearProducts, 'ventures': nearVentures};
    } else {
      final geo = Geolocation();
      final position = await geo.requestGeolocation();

      Map<String, List> near = await _ventureApiProvider.getNear(
          position.latitude, position.longitude, token);

      /// Insert to sqlite cache
      _ventureLocalDb.insertAllVentures(
          near['ventures'], AppConstants.NEAR_SECTION);
      _productLocalDb.insertAllProducts(
          near['products'], AppConstants.NEAR_SECTION);

      return near;
    }
  }

  /// Retrieves and transforms the trendy ventures using the provider
  Future<List<VentureModel>> getTrendy() async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      return _ventureLocalDb.getAllVentures(AppConstants.TENDENCIES_SECTION);
    } else {
      List<VentureModel> trendy =
          await _ventureApiProvider.getSuggestedVentures(token);

      /// Insert retrieved data into sqlite
      _ventureLocalDb.insertAllVentures(
          trendy, AppConstants.TENDENCIES_SECTION);

      return trendy;
    }
  }

  /// Retrieves and transforms the ventures of the category using the provider
  Future<List<VentureModel>> getCategoryVentures(String category) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      return _ventureLocalDb.getAllVentures(AppConstants.CATEGORIES_SECTION);
    } else {
      List<VentureModel> categoryV =
          await _ventureApiProvider.getCategoryVentures(token, category);

      /// Insert retrieved data into sqlite
      _ventureLocalDb.insertAllVentures(
          categoryV, AppConstants.CATEGORIES_SECTION);

      return categoryV;
    }
  }

  /// Retrieves the products of a venture
  Future<List<ProductModel>> getVentureProducts(
      String ventureId, bool fromVenture) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      return _productLocalDb.getProductsOfVenture(ventureId);
    } else {
      List<ProductModel> products =
          await _ventureApiProvider.getProducts(ventureId, token);

      /// Insert retrieved data into sqlite
      _productLocalDb.insertAllProducts(
          products,
          (fromVenture)
              ? AppConstants.USER_VENTURE_SECTION
              : AppConstants.VENTURE_DETAIL_SECTION,
          ventureId);

      return products;
    }
  }

  /// Deletes the product of the venture from the remote and local dbs
  Future<bool> deleteVentureProduct(String productId) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final bool response =
          await _ventureApiProvider.deleteVentureProduct(productId, token);

      // Deletes the product from cache
      _productLocalDb.deleteProduct(
          AppConstants.USER_VENTURE_SECTION, productId);

      return response;
    }
  }

  /// Creates a venture for the user
  Future<VentureModel> createVenture(Map<String, dynamic> payload) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final response = await _ventureApiProvider.createVenture(payload, token);
      role = AppConstants.VENTURE_ROLE;
      await prefs.clearKey(AppConstants.CREATE_vENTURE_FORM);
      return response;
    }
  }

  /// Save answers
  Future<void> saveVentureAnswers(Map<String, dynamic> payload) async {
    await prefs.setKey(AppConstants.CREATE_vENTURE_FORM, jsonEncode(payload));
  }

  /// Refresh answers
  Future<Map<String, dynamic>> refreshAnswers() async {
    await prefs.clearKey(AppConstants.CREATE_vENTURE_FORM);
    final answers = prefs.getKey(AppConstants.CREATE_vENTURE_FORM);
    if (answers.length > 0) return jsonDecode(answers);
    return null;
  }
}
