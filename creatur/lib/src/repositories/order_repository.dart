import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/providers/order_api_provider.dart';
import 'package:creatur/src/repositories/repository.dart';
import 'package:creatur/src/utils/errors.dart';

/// The repository of everything related to the orders
class OrderRepository extends Repository {
  /// The provider of the orders
  final OrderApiProvider _orderApiProvider;

  /// The default constructor
  OrderRepository() : _orderApiProvider = OrderApiProvider();

  /// Create an order
  Future createOrder(Map<String, dynamic> payload) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final response = await _orderApiProvider.createOrder(payload, token);
      return response;
    }
  }

  /// Retrieves the orders of a venture
  Future<List<OrderModel>> getVentureOrders() async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION,
          message: AppErrors.VENTURE_ORDERS_ERROR);
    } else {
      return await _orderApiProvider.getOrders(token);
    }
  }

  /// Get an order by its id
  Future<OrderModel> getOrder(String orderId) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final response = await _orderApiProvider.getOrder(orderId, token);
      return response;
    }
  }

  /// Change an order
  Future<OrderModel> changeOrderStatus(String orderId, String status) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION, message: AppErrors.CONNECTION);
    } else {
      final response =
          await _orderApiProvider.changeStatus(orderId, status, token);
      return response;
    }
  }

  /// Retrieves the orders of a user
  Future<List<OrderModel>> getUserOrders() async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION,
          message: AppErrors.USER_ORDERS_ERROR);
    } else {
      return await _orderApiProvider.getUserOrders(token);
    }
  }

  /// Creates a review for an order
  Future createReview(String orderId, int grade) async {
    final hasInternet = await checkConnectivity();
    if (!hasInternet) {
      throw new CreaturError(
          type: CreaturErrorType.CONNECTION,
          message: AppErrors.USER_ORDERS_ERROR);
    } else {
      return await _orderApiProvider.createReview(orderId, token, grade);
    }
  }
}
