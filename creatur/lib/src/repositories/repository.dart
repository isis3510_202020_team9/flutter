import 'package:creatur/src/providers/database/product_db.dart';
import 'package:creatur/src/providers/database/venture_db.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/storage.dart';
import 'package:connectivity/connectivity.dart';

/// Class for represent a generic repository
class Repository {
  UserPreferences prefs;

  String get token => prefs.getKey(AppConstants.TOKEN);

  set token(String token) => prefs.setKey(AppConstants.TOKEN, token);

  /// Has profiling vars
  bool get hasProfiling => prefs.getBool(AppConstants.HAS_PROFILING);

  set hasProfiling(bool hasProf) =>
      prefs.setBoolean(AppConstants.HAS_PROFILING, hasProf);

  /// Role
  String get role => prefs.getKey(AppConstants.ROLE);

  set role(String role) => prefs.setKey(AppConstants.ROLE, role);

  Repository() {
    prefs = UserPreferences();
  }

  /// Check connectivity at the phone
  checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    return (connectivityResult != ConnectivityResult.none);
  }

  /// Logouts the user deleting all the information stored because of him
  static Future<void> logout() async {
    await UserPreferences().clearPrefs();
    await ProductDB().deleteAllProducts();
    await VentureDB().deleteAllVentures();
  }
}
