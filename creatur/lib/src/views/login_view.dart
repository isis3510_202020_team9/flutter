import 'package:creatur/src/blocs/login/login_bloc.dart';
import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/components/button.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/components/logo.dart';
import 'package:creatur/src/components/text_field.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';

/// Class that corresponds to the user Login View
class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

/// The login main state for the view
class _LoginViewState extends State<LoginView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  ///Login function to log in the user into the application
  void _login(BuildContext context, Function submit) async {
    DialogBuilder(context).showLoadingIndicator();
    try {
      final response = await submit();
      DialogBuilder(context).hideOpenDialog();
      if (response != null && response)
        Navigator.pushReplacementNamed(context, '/home');
      else if (response != null && response == false) {
        Navigator.pushReplacementNamed(context, '/profiling');
      }
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message);
    }
  }

  void _showSnackBar(String message) {
    SnackBar snackBar = SnackBar(content: Text('$message'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    /// the login BLoC to use for the states
    final loginBloc = BlocProvider.of(context).loginBloc;

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColors.BACKGROUND_COLOR,
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                logoType(),
                emailField(loginBloc),
                passwordField(loginBloc),
                SizedBox(
                  height: 20.0,
                ),
                submitButton(loginBloc),
                SizedBox(
                  height: 10.0,
                ),
                redirectToSignUp(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Defines the email field for the view
  Widget emailField(LoginBloc bloc) => StreamBuilder<String>(
      stream: bloc.emailStream,
      builder: (context, snapshot) {
        return CreaturTextField(
          textInputType: TextInputType.emailAddress,
          hint: 'Ingresa tu correo electrónico',
          label: 'Email',
          error: snapshot.error,
          onChanged: bloc.changeEmail,
          maxLength: 80,
        );
      });

  /// Defines the password field for the view
  Widget passwordField(LoginBloc bloc) => StreamBuilder<String>(
      stream: bloc.passwordStream,
      builder: (context, snapshot) {
        return CreaturTextField(
          hint: 'Ingresa la contraseña',
          label: 'Contraseña',
          error: snapshot.error,
          obscureText: true,
          onChanged: bloc.changePassword,
          maxLength: 20,
        );
      });

  /// Defines the submit button for the view
  Widget submitButton(LoginBloc bloc) {
    return StreamBuilder(
        stream: bloc.loginValidStream,
        builder: (context, snapshot) {
          return CreaturMainButton(
            onPressed:
                snapshot.hasData ? () => _login(context, bloc.submit) : null,
            text: 'Ingresar',
          );
        });
  }

  /// Redirect to sign up if the user doesnt have an account
  Widget redirectToSignUp() {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'No tengo cuenta aún. ',
              style: TextStyle(fontFamily: 'Rubik'),
            ),
            GestureDetector(
              child: Text(
                'Registrarme',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: AppColors.PRIMARY_COLOR,
                  fontFamily: 'Rubik',
                ),
              ),
              onTap: () => Navigator.pushReplacementNamed(context, '/register'),
            )
          ]),
    );
  }
}
