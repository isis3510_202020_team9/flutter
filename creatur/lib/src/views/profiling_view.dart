import 'package:creatur/src/blocs/profiling/profiling_bloc.dart';
import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';

class ProfilingView extends StatefulWidget {
  ProfilingView({Key key}) : super(key: key);

  @override
  _ProfilingViewState createState() => _ProfilingViewState();
}

class _ProfilingViewState extends State<ProfilingView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // static questions
  List questions = [
    {
      "answers": [
        {
          "id": 1,
          "title": "Ir a comprar ropa nueva",
        },
        {
          "id": 2,
          "title": "Salir a comer con mis amigos.",
        },
        {
          "id": 3,
          "title": "Ir al zoológico",
        },
        {
          "id": 4,
          "title": "Ver netflix mientras como helado.",
        },
        {
          "id": 5,
          "title": "Tener una tarde llena de juegos de mesa.",
        },
        {
          "id": 6,
          "title": "Quedarme en casa viendo partidos.",
        }
      ]
    },
    {
      "answers": [
        {
          "id": 1,
          "title": "Twitch",
        },
        {
          "id": 2,
          "title": "Rappi",
        },
        {
          "id": 3,
          "title": "Nike training, Onefootball, Google Now",
        },
        {
          "id": 4,
          "title": "Laika",
        },
        {"id": 5, "title": "Trendier"}
      ]
    },
    {
      "answers": [
        {
          "id": 1,
          "title": "Viajar por el mundo probando diferentes gastronomías.",
        },
        {
          "id": 2,
          "title": "Gastarme todo el dinero en joyas y ropa.",
        },
        {
          "id": 3,
          "title": "Regalar juguetes a fundaciones.",
        },
        {
          "id": 4,
          "title": "Ir a los próximos Juegos Olímpicos.",
        },
        {
          "id": 5,
          "title":
              "Comprarme una casa enorme donde pueda tener las mascotas posibles.",
        }
      ]
    }
  ];

  /// Profiling method with loading
  void _profiling(BuildContext context, Function submit) async {
    try {
      DialogBuilder(context).showLoadingIndicator();
      final response = await submit();
      DialogBuilder(context).hideOpenDialog();
      if (response != null) Navigator.pushReplacementNamed(context, '/home');
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message);
    }
  }

  void _showSnackBar(String message) {
    SnackBar snackBar = SnackBar(content: Text('$message'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final profilingBloc = BlocProvider.of(context).profilingBloc;
    return Scaffold(
      key: _scaffoldKey,
      body: WillPopScope(
        onWillPop: () async => false,
        child: SafeArea(
          child: Container(
            margin: EdgeInsets.all(20.0),
            padding: EdgeInsets.only(top: 25.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Nos gustaría',
                    style: TextStyle(
                      color: AppColors.PRIMARY_COLOR,
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0,
                      fontFamily: 'Poppins',
                    ),
                  ),
                  Text(
                    'conocer más de ti',
                    style: TextStyle(
                      color: AppColors.PRIMARY_COLOR,
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0,
                      fontFamily: 'Poppins',
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  StreamBuilder(
                    stream: profilingBloc.firstQuestion,
                    initialData: 1,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      return dropdownList(
                          '¿Qué prefieres hacer los fines de semana?',
                          questions[0]['answers'],
                          profilingBloc.changeFirstQuestion,
                          snapshot.data);
                    },
                  ),
                  StreamBuilder(
                    stream: profilingBloc.secondQuestion,
                    initialData: 1,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      return dropdownList(
                          '¿Qué aplicación pensarías en descargar primero?',
                          questions[1]['answers'],
                          profilingBloc.changeSecondQuestion,
                          snapshot.data);
                    },
                  ),
                  StreamBuilder(
                    stream: profilingBloc.thirdQuestion,
                    initialData: 1,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      return dropdownList(
                          '¿Qué harías primero si te ganaras la lotería?',
                          questions[2]['answers'],
                          profilingBloc.changeThirdQuestion,
                          snapshot.data);
                    },
                  ),
                  SizedBox(
                    height: 22.0,
                  ),
                  Center(child: button(context, profilingBloc))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// Represent a dropdown list of the questionaire
  Widget dropdownList(
      String question, List items, Function onChanged, int currentValue) {
    return Column(
      children: [
        Text(
          '$question',
          style: TextStyle(
            fontSize: 20.0,
            fontFamily: 'Rubik',
          ),
        ),
        SizedBox(height: 18.0),
        Center(
          child: Container(
            padding: const EdgeInsets.only(left: 16.0, right: 10.0),
            width: 339,
            height: 53,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: AppColors.INPUT_BACKGROUND_COLOR,
                border: Border.all(
                    color: AppColors.ACCENT_COLOR_GREEN.withOpacity(0.59),
                    width: 1.4)),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                  isExpanded: true,
                  value: currentValue,
                  items: items
                      .map((item) => DropdownMenuItem(
                          child: Text(
                            '${item['title']}',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                            ),
                          ),
                          value: item['id']))
                      .toList(),
                  onChanged: onChanged),
            ),
          ),
        ),
        SizedBox(height: 22.0),
      ],
    );
  }

  /// Profiling button
  Widget button(BuildContext context, ProfilingBloc bloc) {
    return RaisedButton(
      onPressed: () => _profiling(context, bloc.submit),
      child: Container(
        width: 305,
        height: 50,
        child: Center(
          child: Text(
            'Enviar',
            style: TextStyle(
              fontSize: 19,
              fontFamily: 'Rubik',
            ),
          ),
        ),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      color: AppColors.ACCENT_COLOR_GREEN,
    );
  }
}
