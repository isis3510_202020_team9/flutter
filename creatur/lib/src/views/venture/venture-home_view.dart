import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:creatur/src/blocs/venture_provider.dart';
import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/views/detail_views/order-detail_view.dart';
import 'package:creatur/src/views/detail_views/product_detail_view.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// The class that corresponds to the home of the venture if the user has one
class VentureHomeView extends StatefulWidget {
  /// The venture to be loaded
  final VentureModel venture;

  /// The default constructor
  VentureHomeView({Key key, this.venture}) : super(key: key);

  @override
  _VentureHomeState createState() => _VentureHomeState();
}

/// The main state of the VentureHomeView
class _VentureHomeState extends State<VentureHomeView> {
  /// The global key for the scaffold
  GlobalKey<ScaffoldState> _scaffoldKey;

  /// The venture to render
  VentureModel venture;

  // /// Saves if there's a snackbar already being shown
  // bool _hasSnackBar;

  // /// Stream subscription to listen to connectivity changes
  // StreamSubscription<bool> _noConnListener;

  @override
  void initState() {
    super.initState();
    venture = widget.venture;
  }

  @override
  Widget build(BuildContext context) {
    // /// Initialize the reference of the bloc
    // final _connectivityBloc = BlocProvider.of(context).connectivityBloc;

    /// The venture bloc
    final _ventureBloc = VentureProvider.of(context).ventureHomeBloc;

    // /// Uses the stream to listen when internet's gone
    // _noConnListener =
    //     _connectivityBloc.connectivityStream.listen(_showSnackBar);

    return SafeArea(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: AppColors.BACKGROUND_COLOR,
          appBar: AppBar(
            backgroundColor: AppColors.BACKGROUND_COLOR,
            elevation: 0,
            bottom: TabBar(
              isScrollable: false,
              labelPadding: EdgeInsets.zero,
              indicatorWeight: 3.0,
              indicatorColor: AppColors.ACCENT_COLOR_BLUE,
              labelColor: Color(0xFF484848),
              labelStyle: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12,
                fontFamily: 'Poppins',
              ),
              tabs: <Widget>[
                Tab(
                  text: "PRODUCTOS",
                ),
                Tab(
                  text: "ORDENES",
                ),
              ],
            ),
            title: Text(
              venture?.name,
              style: const TextStyle(
                color: AppColors.PRIMARY_COLOR,
                fontSize: 24,
                fontFamily: 'Poppins',
              ),
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              Container(
                margin: const EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () => print('GO TO VENTURE'),
                  child: Hero(
                    tag: venture.id,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: CachedNetworkImage(
                        placeholder: (context, url) => Image.asset(
                          'assets/img/no-image.png',
                          fit: BoxFit.cover,
                          height: 55.0,
                          width: 55.0,
                        ),
                        errorWidget: (context, url, _) => Image.asset(
                          'assets/img/no-image.png',
                          fit: BoxFit.cover,
                          height: 55.0,
                          width: 55.0,
                        ),
                        imageUrl: venture?.photo,
                        fit: BoxFit.cover,
                        height: 55.0,
                        width: 55.0,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          body: TabBarView(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  _ProductsList(
                    venture: venture,
                    getProductsFun: _ventureBloc.getVentureProducts,
                    productsStream: _ventureBloc.ventureProductsStream,
                    deleteFunction: _ventureBloc.deleteProduct,
                    // scaffoldKey: _scaffoldKey,
                  ),
                  Positioned(
                    bottom: 15.0,
                    right: 15.0,
                    child: FloatingActionButton(
                      backgroundColor: AppColors.ACCENT_COLOR_BLUE,
                      foregroundColor: AppColors.BACKGROUND_COLOR,
                      onPressed: () async {
                        final result = await Navigator.of(context)
                            .pushNamed('/createProduct');
                        if (result ?? false) {
                          _ventureBloc.getVentureProducts();
                        }
                      },
                      tooltip: 'Crear producto',
                      child: Icon(
                        Icons.add,
                      ),
                    ),
                  ),
                ],
              ),
              _OrdersList(
                venture: venture,
                getOrdersFun: _ventureBloc.getVentureOrders,
                ordersStream: _ventureBloc.ventureOrdersStream,
              ),
            ],
          ),
        ),
      ),
    );
  }

  // /// Shows a snackbar if the internet connection is gone
  // void _showSnackBar(bool hasInternet) {
  //   if (!hasInternet && !_hasSnackBar) {
  //     SnackBar snackBar = SnackBar(content: Text(AppErrors.NO_UPDATE_ERROR));
  //     _scaffoldKey.currentState
  //         .showSnackBar(snackBar)
  //         .closed
  //         .whenComplete(() => _hasSnackBar = false);
  //   }
  // }

  @override
  void dispose() {
    venture = null;
    super.dispose();
  }
}

/// The product list of the venture
class _ProductsList extends StatelessWidget {
  /// The venture model to render
  final VentureModel venture;

  /// The function to update the list of products
  final Function() getProductsFun;

  /// The stream to hear from
  final Stream<List<ProductModel>> productsStream;

  /// Function to delete a product of the list
  final Function(String) deleteFunction;

  // /// The page controller for the pagination
  // final ScrollController _pageController;

  /// The default constructor
  _ProductsList(
      {Key key,
      this.venture,
      @required this.getProductsFun,
      @required this.productsStream,
      @required this.deleteFunction})
      : super(key: key);

  /// Shows a snack bar for the messages
  void _showSnackBarMessage(String message, BuildContext context) {
    SnackBar snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () => Scaffold.of(context)
            .removeCurrentSnackBar(reason: SnackBarClosedReason.action),
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    /// Calls the function to retrieve the products
    getProductsFun();

    return Container(
      child: RefreshIndicator(
        semanticsLabel: 'Cargando información',
        onRefresh: getProductsFun,
        child: _productsListStreamBuilder(),
      ),
    );
  }

  /// Loads the products of the venture
  Widget _productsListStreamBuilder() {
    return StreamBuilder<List<ProductModel>>(
      stream: productsStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return (snapshot.data.isNotEmpty)
              ? _productsListItems(snapshot.data)
              : ListView(
                  itemExtent: 300,
                  children: <Widget>[
                    Center(
                      child: Container(
                        width: 250,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppErrors.NO_PRODUCTS_MY_VENTURE,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'Rubik',
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            OutlineButton(
                              color: AppColors.PRIMARY_COLOR,
                              onPressed: getProductsFun,
                              textColor: AppColors.PRIMARY_COLOR,
                              child: Text(
                                'Reintentar',
                                style: TextStyle(
                                  fontFamily: 'Rubik',
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.PRIMARY_COLOR,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                );
        } else if (snapshot.hasError) {
          return ListView(
            itemExtent: 300,
            children: <Widget>[
              Center(
                child: Container(
                  width: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        snapshot.error.toString(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Rubik',
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      OutlineButton(
                        color: AppColors.PRIMARY_COLOR,
                        onPressed: getProductsFun,
                        textColor: AppColors.PRIMARY_COLOR,
                        child: Text(
                          'Reintentar',
                          style: TextStyle(
                            fontFamily: 'Rubik',
                            fontWeight: FontWeight.bold,
                            color: AppColors.PRIMARY_COLOR,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
        }

        return Center(
          child: Theme(
            data: Theme.of(context)
                .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  /// Renders the products of the list into it
  Widget _productsListItems(List<ProductModel> products) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: ListView.builder(
        // itemExtent: 90,
        scrollDirection: Axis.vertical,
        itemCount: products.length,
        itemBuilder: (context, index) => _productTile(context, products[index]),
      ),
    );
  }

  /// Creates a product tile
  Widget _productTile(BuildContext context, ProductModel product) {
    /// the currency formatter
    final formatCurrency =
        NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');

    return Card(
      margin: const EdgeInsets.only(top: 20.0),
      elevation: 3.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: ListTile(
        contentPadding: const EdgeInsets.all(10.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        onTap: () async {
          bool didIt = await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ProductDetailView(
                product: product,
                venture: venture,
                fromVenture: true,
                deleteFunction: deleteFunction,
              ),
            ),
          );

          if (didIt ?? false) {
            _showSnackBarMessage(AppErrors.SUCCESS_DELETE_PRODUCT, context);
          }
        },
        leading: Hero(
          tag: product.id,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: CachedNetworkImage(
              placeholder: (context, url) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              errorWidget: (context, url, _) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              imageUrl: product.photo,
              fit: BoxFit.cover,
              height: 70.0,
              width: 80.0,
            ),
          ),
        ),
        title: Text(
          product.name,
          style: TextStyle(
            fontFamily: 'Rubik',
            fontSize: 16,
          ),
        ),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              '${formatCurrency.format(product.price)}',
              style: TextStyle(
                fontFamily: 'Rubik',
                fontWeight: FontWeight.w500,
                fontSize: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// The orders list of the venture
class _OrdersList extends StatelessWidget {
  /// The venture model to render
  final VentureModel venture;

  /// The function to update the list of orders
  final Function() getOrdersFun;

  /// The stream to hear from
  final Stream<List<OrderModel>> ordersStream;

  /// The default constructor
  _OrdersList(
      {Key key,
      this.venture,
      @required this.getOrdersFun,
      @required this.ordersStream})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// Calls the function to retrieve the orders
    getOrdersFun();

    return Container(
      child: RefreshIndicator(
        semanticsLabel: 'Cargando información',
        onRefresh: getOrdersFun,
        child: _ordersListStreamBuilder(),
      ),
    );
  }

  /// Loads the orders of the venture
  Widget _ordersListStreamBuilder() {
    return StreamBuilder<List<OrderModel>>(
      stream: ordersStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return (snapshot.data.isNotEmpty)
              ? _ordersListItems(snapshot.data)
              : ListView(
                  itemExtent: 300,
                  children: <Widget>[
                    Center(
                      child: Container(
                        width: 250,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppErrors.NO_ORDERS_MY_VENTURE,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'Rubik',
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            OutlineButton(
                              color: AppColors.PRIMARY_COLOR,
                              onPressed: getOrdersFun,
                              textColor: AppColors.PRIMARY_COLOR,
                              child: Text(
                                'Reintentar',
                                style: TextStyle(
                                  fontFamily: 'Rubik',
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.PRIMARY_COLOR,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                );
        } else if (snapshot.hasError) {
          return ListView(
            itemExtent: 300,
            children: <Widget>[
              Center(
                child: Container(
                  width: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        snapshot.error.toString(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Rubik',
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      OutlineButton(
                        color: AppColors.PRIMARY_COLOR,
                        onPressed: getOrdersFun,
                        textColor: AppColors.PRIMARY_COLOR,
                        child: Text(
                          'Reintentar',
                          style: TextStyle(
                            fontFamily: 'Rubik',
                            fontWeight: FontWeight.bold,
                            color: AppColors.PRIMARY_COLOR,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
        }

        return Center(
          child: Theme(
            data: Theme.of(context)
                .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  /// Renders the orders of the list into it
  Widget _ordersListItems(List<OrderModel> orders) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: ListView.builder(
        // itemExtent: 100,
        scrollDirection: Axis.vertical,
        itemCount: orders.length,
        itemBuilder: (context, index) => _orderTile(context, orders[index]),
      ),
    );
  }

  /// Creates a order tile
  Widget _orderTile(BuildContext context, OrderModel order) {
    /// the currency and date formatters
    final formatCurrency =
        NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');
    final DateFormat formatDate = DateFormat.yMd("es_CO");

    return Card(
      margin: const EdgeInsets.only(top: 20.0),
      elevation: 3.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: ListTile(
        contentPadding: const EdgeInsets.all(8.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        onTap: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OrderDetailView(
                venture: venture,
                orderId: order.id,
                fromVenture: true,
              ),
            ),
          );
          //Refresh the orders when going back, so that they are always up to date
          getOrdersFun();
        },
        leading: Hero(
          tag: '${order.products[0].id}${order.id}',
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: CachedNetworkImage(
              placeholder: (context, url) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              errorWidget: (context, url, _) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              imageUrl: order.products[0].photo,
              fit: BoxFit.cover,
              height: 70.0,
              width: 80.0,
            ),
          ),
        ),
        title: Text(
          order.products[0].name,
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontFamily: 'Rubik',
            fontWeight: FontWeight.w500,
            color: AppColors.PRIMARY_COLOR,
            fontSize: 20,
          ),
        ),
        subtitle: _orderStatus(order.status),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(
              '${formatDate.format(order.desiredDate)}',
              style: TextStyle(
                fontFamily: 'Rubik',
                fontSize: 12,
              ),
            ),
            Text(
              '${formatCurrency.format(order.finalPrize)}',
              style: TextStyle(
                fontFamily: 'Rubik',
                fontWeight: FontWeight.w500,
                fontSize: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Retrieves the order status
  Widget _orderStatus(String status) {
    final Color color = AppColors.statusColors[status];
    final String statusStr = AppConstants.statusMap[status];

    return Container(
      // padding: const EdgeInsets.symmetric(horizontal: 10.0),
      width: 60,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Text(
        statusStr,
        textAlign: TextAlign.center,
        style:
            TextStyle(fontFamily: 'Poppins', fontSize: 14, color: Colors.black),
      ),
    );
  }
}
