import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/blocs/venture/create-venture_bloc.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/components/text_field.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:flutter/material.dart';

class CreateVentureView extends StatefulWidget {
  CreateVentureView({Key key}) : super(key: key);

  @override
  _CreateVentureViewState createState() => _CreateVentureViewState();
}

class _CreateVentureViewState extends State<CreateVentureView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Function saveAnswers;

  @override
  void deactivate() {
    saveAnswers();
    super.deactivate();
  }

  /// Submit
  _submit(BuildContext context, Function send) async {
    try {
      DialogBuilder(context).showLoadingIndicator();
      final response = await send();
      DialogBuilder(context).hideOpenDialog();
      if (response != null) Navigator.pop(context, true);
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message);
    }
  }

  void _showSnackBar(String message) {
    SnackBar snackBar = SnackBar(content: Text('$message'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final createVentureBloc = BlocProvider.of(context).createVentureBloc;

    saveAnswers = createVentureBloc.saveVentureData;

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: AppColors.BACKGROUND_COLOR,
          appBar: AppBar(
            leading: BackButton(
              color: Colors.black,
              onPressed: () => Navigator.pop(context),
            ),
            backgroundColor: AppColors.BACKGROUND_COLOR,
            elevation: 0,
          ),
          body: SafeArea(
            child: Container(
              margin: EdgeInsets.all(20.0),
              child: FutureBuilder(
                  future: createVentureBloc.refreshAnswers(),
                  builder: (contex, snapshot) {
                    if (snapshot.hasData) {
                      final initialData = snapshot.data;
                      final name = initialData == "" ? '' : initialData['name'];
                      final phone =
                          initialData == "" ? '' : initialData['phone'];
                      final address =
                          initialData == "" ? '' : initialData['address'];
                      final facebookUrl =
                          initialData == "" ? '' : initialData['facebookUrl'];
                      final instagramUrl =
                          initialData == "" ? '' : initialData['instagramUrl'];
                      return SingleChildScrollView(
                        child: Column(
                          children: [
                            Text(
                              'Tu emprendimiento',
                              style: TextStyle(
                                color: AppColors.PRIMARY_COLOR,
                                fontWeight: FontWeight.bold,
                                fontSize: 30.0,
                                fontFamily: 'Poppins',
                              ),
                            ),
                            _ventureNameInput(createVentureBloc, name),
                            _venturePhoneInput(createVentureBloc, phone),
                            _ventureAddressInput(createVentureBloc, address),
                            _ventureFacebookInput(
                                createVentureBloc, facebookUrl),
                            _ventureInstagramInput(
                                createVentureBloc, instagramUrl),
                            _ventureCategory(
                                createVentureBloc, AppConstants.categories),
                            _submitButton(context, createVentureBloc)
                          ],
                        ),
                      );
                    } else {
                      return Center(
                        child: Theme(
                          data: Theme.of(context).copyWith(
                              accentColor: AppColors.ACCENT_COLOR_BLUE),
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                  }),
            ),
          )),
    );
  }

// Venture name input
  Widget _ventureNameInput(CreateVentureBloc bloc, String initialName) =>
      StreamBuilder<String>(
          stream: bloc.ventureNameStream,
          builder: (context, snapshot) {
            return CreaturTextField(
              textInputType: TextInputType.name,
              hint: 'Ingresa el nombre de tu emprendimiento',
              label: 'Nombre',
              error: snapshot.error,
              controller: TextEditingController(text: initialName),
              onChanged: bloc.changeVentureName,
            );
          });

  // Venture name input
  Widget _venturePhoneInput(CreateVentureBloc bloc, String initialPhone) =>
      StreamBuilder<String>(
          stream: bloc.venturePhoneStream,
          builder: (context, snapshot) {
            return CreaturTextField(
              textInputType: TextInputType.number,
              hint: 'Ingresa el teléfono',
              label: 'Teléfono',
              error: snapshot.error,
              controller: TextEditingController(text: initialPhone),
              onChanged: bloc.changeVenturePhone,
            );
          });

  Widget _ventureAddressInput(CreateVentureBloc bloc, String initialAddress) =>
      StreamBuilder<String>(
          stream: bloc.ventureAddressStream,
          builder: (context, snapshot) {
            return CreaturTextField(
              textInputType: TextInputType.text,
              hint: 'Ingresa la dirección',
              label: 'Dirección',
              error: snapshot.error,
              controller: TextEditingController(text: initialAddress),
              onChanged: bloc.changeVentureAddress,
            );
          });

  Widget _ventureFacebookInput(CreateVentureBloc bloc, String url) =>
      StreamBuilder<String>(
          stream: bloc.ventureFacebookStream,
          builder: (context, snapshot) {
            return CreaturTextField(
              textInputType: TextInputType.text,
              hint: 'Ingresa tu usuario',
              label: 'Usuario de Facebook',
              error: snapshot.error,
              controller: TextEditingController(text: url),
              onChanged: bloc.changeVentureFacebook,
            );
          });

  Widget _ventureInstagramInput(CreateVentureBloc bloc, String url) =>
      StreamBuilder<String>(
          stream: bloc.ventureInstagramStream,
          builder: (context, snapshot) {
            return CreaturTextField(
              textInputType: TextInputType.text,
              hint: 'Ingresa tu usuario',
              label: 'Instagram',
              error: snapshot.error,
              controller: TextEditingController(text: url),
              onChanged: bloc.changeVentureInstagram,
            );
          });

  Widget _ventureCategory(CreateVentureBloc bloc, List items) =>
      StreamBuilder<String>(
          stream: bloc.ventureCategorytream,
          builder: (context, snapshot) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Categorias',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Rubik',
                    ),
                  ),
                  Center(
                    child: Container(
                      padding: const EdgeInsets.only(left: 16.0, right: 10.0),
                      width: 339,
                      height: 53,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: AppColors.INPUT_BACKGROUND_COLOR,
                          border: Border.all(
                              color: AppColors.ACCENT_COLOR_GREEN
                                  .withOpacity(0.59),
                              width: 1.4)),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                            isExpanded: true,
                            value: snapshot.data,
                            items: items
                                .map((item) => DropdownMenuItem<String>(
                                    child: Text(
                                      '${item.keys.first}',
                                      style: TextStyle(
                                        fontFamily: 'Rubik',
                                      ),
                                    ),
                                    value: item.values.first))
                                .toList(),
                            onChanged: bloc.changeVentureCategory),
                      ),
                    ),
                  ),
                  SizedBox(height: 22.0),
                ],
              ),
            );
          });

  Widget _submitButton(BuildContext context, CreateVentureBloc bloc) {
    return StreamBuilder(
        stream: bloc.formValidStream,
        builder: (context, snapshot) {
          return RaisedButton(
            onPressed: snapshot.hasData
                ? () => _submit(context, bloc.createVenture)
                : null,
            child: Container(
              width: 305,
              height: 30,
              child: Center(
                child: Text(
                  'Crear',
                  style: TextStyle(
                    fontSize: 19,
                    fontFamily: 'Rubik',
                  ),
                ),
              ),
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            color: AppColors.ACCENT_COLOR_GREEN,
          );
        });
  }
}
