import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/blocs/venture/create-product_bloc.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/components/text_field.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:flutter/material.dart';

class CreateProductView extends StatefulWidget {
  CreateProductView({Key key}) : super(key: key);

  @override
  _CreateProductViewState createState() => _CreateProductViewState();
}

class _CreateProductViewState extends State<CreateProductView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Function saveAnswers;

  CreateProductBloc createProductBloc;

  _submit(BuildContext context, Function send) async {
    try {
      DialogBuilder(context).showLoadingIndicator();
      final response = await send();
      DialogBuilder(context).hideOpenDialog();
      if (response != null) Navigator.pop(context, true);
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message);
    }
  }

  void _showSnackBar(String message) {
    SnackBar snackBar = SnackBar(content: Text('$message'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  void didChangeDependencies() {
    createProductBloc = BlocProvider.of(context).createProductBloc;
    super.didChangeDependencies();
  }

  @override
  void deactivate() {
    saveAnswers();
    super.deactivate();
  }

  // @override
  // void dispose() async {
  //   await createProductBloc.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    final createProductBloc = BlocProvider.of(context).createProductBloc;

    saveAnswers = createProductBloc.saveAnswers;

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: AppColors.BACKGROUND_COLOR,
        key: _scaffoldKey,
        appBar: AppBar(
          leading: BackButton(
            color: Colors.black,
            onPressed: () => Navigator.pop(context, false),
          ),
          backgroundColor: AppColors.BACKGROUND_COLOR,
          elevation: 0,
        ),
        body: SafeArea(
          child: Container(
            margin: EdgeInsets.all(20.0),
            child: SingleChildScrollView(
              child: FutureBuilder(
                  future: createProductBloc.refreshAnswers(),
                  builder: (contex, snapshot) {
                    final initialData = snapshot.data;
                    final validation =
                        (initialData == "" || initialData == null);
                    final name = (initialData == "" || initialData == null)
                        ? ""
                        : (initialData['name'] != null
                            ? initialData['name']
                            : "");
                    final price = validation
                        ? ""
                        : (initialData['prize'] != null
                            ? initialData['prize']
                            : "");
                    final description = validation
                        ? ""
                        : (initialData['description'] != null)
                            ? initialData['description']
                            : "";
                    if (snapshot.hasData) {
                      return Column(
                        children: [
                          Text(
                            'Producto',
                            style: TextStyle(
                              color: AppColors.PRIMARY_COLOR,
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              fontFamily: 'Poppins',
                            ),
                          ),
                          _productNameInput(createProductBloc, name),
                          _productPrizeInput(createProductBloc, price),
                          _productDescriptionInput(
                              createProductBloc, description),
                          _productCategory(
                              createProductBloc, AppConstants.categories),
                          _submitButton(context, createProductBloc)
                        ],
                      );
                    } else {
                      return Center(
                        child: Theme(
                          data: Theme.of(context).copyWith(
                              accentColor: AppColors.ACCENT_COLOR_BLUE),
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                  }),
            ),
          ),
        ),
      ),
    );
  }

  _productNameInput(CreateProductBloc bloc, String name) =>
      StreamBuilder<String>(
          stream: bloc.productNameStream,
          builder: (context, snapshot) {
            return CreaturTextField(
              textInputType: TextInputType.text,
              hint: 'Ingresa el nombre',
              label: 'Nombre producto',
              error: snapshot.error,
              controller: TextEditingController(text: name),
              onChanged: bloc.changeProductName,
            );
          });

  _productPrizeInput(CreateProductBloc bloc, String price) =>
      StreamBuilder<String>(
          stream: bloc.productPrizeStream,
          builder: (context, snapshot) {
            return CreaturTextField(
              textInputType: TextInputType.number,
              hint: 'Ingresa el precio',
              label: 'Precio',
              controller: TextEditingController(text: price),
              error: snapshot.error,
              onChanged: bloc.changeProductPrize,
            );
          });

  _productDescriptionInput(CreateProductBloc bloc, String description) =>
      StreamBuilder<String>(
          stream: bloc.productDescriptionStream,
          builder: (context, snapshot) {
            return CreaturTextField(
              textInputType: TextInputType.text,
              hint: 'Ingresa la descripción',
              label: 'Descripción',
              error: snapshot.error,
              controller: TextEditingController(text: description),
              onChanged: bloc.changeProductDescription,
              maxLines: 3,
              maxLength: 300,
            );
          });

  _productCategory(CreateProductBloc bloc, List items) => StreamBuilder<String>(
      stream: bloc.productCategorytream,
      builder: (context, snapshot) {
        return Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Categorias',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'Rubik',
                ),
              ),
              Center(
                child: Container(
                  padding: const EdgeInsets.only(left: 16.0, right: 10.0),
                  width: 339,
                  height: 53,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: AppColors.INPUT_BACKGROUND_COLOR,
                      border: Border.all(
                          color: AppColors.ACCENT_COLOR_GREEN.withOpacity(0.59),
                          width: 1.4)),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                        isExpanded: true,
                        value: snapshot.data,
                        items: items
                            .map((item) => DropdownMenuItem<String>(
                                child: Text(
                                  '${item.keys.first}',
                                  style: TextStyle(
                                    fontFamily: 'Rubik',
                                  ),
                                ),
                                value: item.values.first))
                            .toList(),
                        onChanged: bloc.changeProductCategory),
                  ),
                ),
              ),
              SizedBox(height: 22.0),
            ],
          ),
        );
      });

  _submitButton(BuildContext context, CreateProductBloc bloc) {
    return StreamBuilder(
        stream: bloc.formValidStream,
        builder: (context, snapshot) {
          return RaisedButton(
            onPressed: snapshot.hasData
                ? () => _submit(context, bloc.createProduct)
                : null,
            child: Container(
              width: 305,
              height: 30,
              child: Center(
                child: Text(
                  'Crear',
                  style: TextStyle(
                    fontSize: 19,
                    fontFamily: 'Rubik',
                  ),
                ),
              ),
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            color: AppColors.ACCENT_COLOR_GREEN,
          );
        });
  }
}
