import 'package:creatur/src/blocs/register/register_bloc.dart';
import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/components/button.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/components/logo.dart';
import 'package:creatur/src/components/text_field.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';

/// Class that corresponds to the user Registration View
class RegistrationView extends StatefulWidget {
  @override
  _RegistrationViewState createState() => _RegistrationViewState();
}

/// The main state for the registration view
class _RegistrationViewState extends State<RegistrationView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  /// Register function to register and log in the user into the application
  void _register(BuildContext context, Function submit) async {
    try {
      DialogBuilder(context).showLoadingIndicator();
      final response = await submit();
      DialogBuilder(context).hideOpenDialog();
      if (response != null)
        Navigator.pushReplacementNamed(context, '/profiling');
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message);
    }
  }

  void _showSnackBar(String message) {
    SnackBar snackBar = SnackBar(content: Text('$message'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final registerBloc = BlocProvider.of(context).registerBloc;

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColors.BACKGROUND_COLOR,
      body: SafeArea(
        child: Center(
          child: Container(
            margin: EdgeInsets.all(20.0),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  logoType(),
                  emailField(registerBloc),
                  passwordField(registerBloc),
                  passwordConfirmField(registerBloc),
                  SizedBox(
                    height: 20.0,
                  ),
                  submitButton(registerBloc),
                  SizedBox(
                    height: 10.0,
                  ),
                  redirectBackToLogIn(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// Defines the email field for the view
  Widget emailField(RegisterBloc bloc) => StreamBuilder<String>(
      stream: bloc.emailStream,
      builder: (context, snapshot) {
        return CreaturTextField(
          textInputType: TextInputType.emailAddress,
          hint: 'Ingresa tu correo electrónico',
          label: 'Email',
          error: snapshot.error,
          onChanged: bloc.changeEmail,
          maxLength: 80,
        );
      });

  /// Defines the password field for the view
  Widget passwordField(RegisterBloc bloc) => StreamBuilder<String>(
      stream: bloc.passwordStream,
      builder: (context, snapshot) {
        return CreaturTextField(
          hint: 'Ingresa la contraseña',
          label: 'Contraseña',
          error: snapshot.error,
          obscureText: true,
          onChanged: bloc.changePassword,
          maxLength: 20,
        );
      });

  /// Defines the password confirmation field for the view
  Widget passwordConfirmField(RegisterBloc bloc) => StreamBuilder<String>(
      stream: bloc.password2Stream,
      builder: (context, snapshot) {
        return CreaturTextField(
          hint: 'Reescribe tu contraseña',
          label: 'Validar Contraseña',
          error: snapshot.error,
          obscureText: true,
          onChanged: bloc.changePassword2,
          maxLength: 20,
        );
      });

  /// Defines the submit button for the view
  Widget submitButton(RegisterBloc bloc) {
    return StreamBuilder(
        stream: bloc.loginValidStream,
        builder: (context, snapshot) {
          return CreaturMainButton(
            onPressed:
                snapshot.hasData ? () => _register(context, bloc.submit) : null,
            text: 'Registrarme',
          );
        });
  }

  /// Redirect to logIn if the user already has an account
  Widget redirectBackToLogIn() {
    return Container(
      child: Row(
          mainAxisAlignment:
              MainAxisAlignment.center, //Center Column contents vertically,
          crossAxisAlignment:
              CrossAxisAlignment.center, //Center Column contents horizontally,
          children: [
            Text(
              'Ya tengo cuenta. ',
              style: TextStyle(fontFamily: 'Rubik'),
            ),
            GestureDetector(
              child: Text(
                'Ingresar',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppColors.PRIMARY_COLOR,
                    fontFamily: 'Rubik'),
              ),
              onTap: () => Navigator.pushReplacementNamed(context, '/login'),
            )
          ]),
    );
  }
}
