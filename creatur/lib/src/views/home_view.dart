import 'dart:async';

import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/blocs/venture_provider.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/views/main_pages/explore_page.dart';
import 'package:creatur/src/views/main_pages/home_page.dart';
import 'package:creatur/src/views/main_pages/activity_page.dart';
import 'package:creatur/src/views/main_pages/venture_page.dart';
import 'package:flutter/material.dart';

/// The class that corresponds to the home view with the navbar
class HomeView extends StatefulWidget {
  /// Default constructor
  const HomeView({Key key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

/// The class that refers to the state of the HomeView
class _HomeViewState extends State<HomeView> {
  /// The index of the selected tab
  int _currentPage = 0;

  /// The global key for the scaffold
  GlobalKey<ScaffoldState> _scaffoldKey;

  /// Saves if there's a snackbar already being shown
  bool _hasSnackBar;

  /// Stream subscription to listen to connectivity changes
  StreamSubscription<bool> _noConnListener;

  @override
  void initState() {
    super.initState();
    _hasSnackBar = false;
    _scaffoldKey = GlobalKey<ScaffoldState>();
  }

  /// The list of tabs in the home
  final List<Widget> _pages = [
    HomePageView(),
    ActivityPageView(),
    ExplorePageView(),
    VentureProvider(child: VenturePageView()),
  ];

  @override
  Widget build(BuildContext context) {
    /// Initialize the reference of the bloc
    final _connectivityBloc = BlocProvider.of(context).connectivityBloc;

    /// Uses the stream to listen when internet's gone
    _noConnListener =
        _connectivityBloc.connectivityStream.listen(_showSnackBar);

    return Scaffold(
      key: _scaffoldKey,
      body: WillPopScope(
          onWillPop: () async => false, child: _pages[_currentPage]),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onPageSelected,
        iconSize: 24.0,
        backgroundColor: AppColors.PRIMARY_COLOR,
        currentIndex: _currentPage,
        items: [
          BottomNavigationBarItem(
            backgroundColor: AppColors.PRIMARY_COLOR,
            icon: Icon(
              Icons.home_outlined,
              color: AppColors.BACKGROUND_COLOR,
            ),
            activeIcon: Icon(
              Icons.home,
              color: AppColors.BACKGROUND_COLOR,
            ),
            label: 'Inicio',
          ),
          BottomNavigationBarItem(
            backgroundColor: AppColors.PRIMARY_COLOR,
            icon: Icon(
              Icons.format_list_bulleted_outlined,
              color: AppColors.BACKGROUND_COLOR,
            ),
            activeIcon: Icon(
              Icons.format_list_bulleted,
              color: AppColors.BACKGROUND_COLOR,
            ),
            label: 'Actividad',
          ),
          BottomNavigationBarItem(
            backgroundColor: AppColors.PRIMARY_COLOR,
            icon: Icon(
              Icons.explore_outlined,
              color: AppColors.BACKGROUND_COLOR,
            ),
            activeIcon: Icon(
              Icons.explore,
              color: AppColors.BACKGROUND_COLOR,
            ),
            label: 'Explora',
          ),
          BottomNavigationBarItem(
            backgroundColor: AppColors.PRIMARY_COLOR,
            icon: Icon(
              Icons.shopping_bag_outlined,
              color: AppColors.BACKGROUND_COLOR,
            ),
            activeIcon: Icon(
              Icons.shopping_bag,
              color: AppColors.BACKGROUND_COLOR,
            ),
            label: 'Negocio',
          )
        ],
      ),
    );
  }

  /// Changes the state with the selected [index] page in the parameter
  void _onPageSelected(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  /// Shows a snackbar if the internet connection is gone
  void _showSnackBar(bool hasInternet) {
    if (!hasInternet && !_hasSnackBar) {
      SnackBar snackBar = SnackBar(content: Text(AppErrors.NO_UPDATE_ERROR));
      _scaffoldKey.currentState
          .showSnackBar(snackBar)
          .closed
          .whenComplete(() => _hasSnackBar = false);
    }
  }

  @override
  void dispose() {
    _noConnListener.cancel();
    super.dispose();
  }
}
