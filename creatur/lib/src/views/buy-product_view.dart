import 'package:cached_network_image/cached_network_image.dart';
import 'package:creatur/src/blocs/order/complete-order_bloc.dart';
import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/views/detail_views/order-detail_view.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BuyProductView extends StatefulWidget {
  /// The product to be bougth
  final ProductModel product;

  /// The venture to show information about (id not in the product)
  final VentureModel venture;

  BuyProductView({Key key, @required this.product, this.venture})
      : super(key: key);

  @override
  _BuyProductViewState createState() => _BuyProductViewState();
}

class _BuyProductViewState extends State<BuyProductView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  /// Product
  ProductModel product;

  /// Venture
  VentureModel venture;

  DateFormat dateFormat = DateFormat.yMMMMd("es_CO");

  @override
  void initState() {
    super.initState();
    product = widget.product;
    venture = widget.venture;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of(context).orderBloc;

    bool keyboardIsOpen = MediaQuery.of(context).viewInsets.bottom != 0;

    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Container(
          child: Stack(children: [
            CustomScrollView(slivers: [
              SliverAppBar(
                  backgroundColor: AppColors.PRIMARY_COLOR,
                  expandedHeight: 180.0,
                  pinned: true,
                  floating: true,
                  flexibleSpace: orderHeader()),
              orderDetail(context, product, bloc),
            ]),
            Visibility(
                visible: !keyboardIsOpen, child: finishOrderButton(bloc)),
          ]),
        ),
      ),
    );
  }

  Widget orderHeader() {
    return FlexibleSpaceBar(
      background: Container(
        color: AppColors.CONTRAST_COLOR_BLUE,
        child: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              'Resumen',
              style: TextStyle(
                  fontSize: 24,
                  fontFamily: 'Rubik',
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10.0),
            Hero(
              tag: venture?.id ?? product.ventureId,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15.0),
                child: CachedNetworkImage(
                  placeholder: (context, url) => Image.asset(
                    'assets/img/no-image.png',
                    fit: BoxFit.cover,
                  ),
                  errorWidget: (context, url, _) => Image.asset(
                    'assets/img/no-image.png',
                    fit: BoxFit.cover,
                  ),
                  imageUrl: venture?.photo ?? product.venturePhoto,
                  fit: BoxFit.fill,
                  height: 90.0,
                  width: 90.0,
                ),
              ),
            ),
            Text(
              '${venture?.name ?? product.ventureName}',
              style: TextStyle(
                  fontSize: 19,
                  fontFamily: 'Rubik',
                  fontWeight: FontWeight.bold,
                  color: AppColors.PRIMARY_COLOR),
            ),
          ]),
        ),
      ),
    );
  }

  Widget orderDetail(
      BuildContext context, ProductModel product, CompleteOrderBloc bloc) {
    final formatCurrency =
        NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');

    final size = MediaQuery.of(context).size;
    const titleStyle = TextStyle(
        fontSize: 24,
        fontFamily: 'Rubik',
        fontWeight: FontWeight.bold,
        color: AppColors.PRIMARY_COLOR);
    return SliverList(
        delegate: SliverChildListDelegate([
      Container(
        height: size.height,
        width: size.width,
        padding:
            EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0, bottom: 20.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          color: AppColors.BACKGROUND_COLOR,
        ),
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Items',
                    style: titleStyle,
                  ),
                  StreamBuilder<int>(
                    stream: bloc.unitsStream,
                    builder: (context, snapshot) {
                      return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: AppColors.PRIMARY_COLOR,
                        ),
                        width: 98,
                        height: 49,
                        child: Center(
                          child: Text(
                            '${formatCurrency.format(snapshot.hasData ? product.price * snapshot.data : 0)}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Hero(
                        tag: product.id,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: CachedNetworkImage(
                            placeholder: (context, url) => Image.asset(
                              'assets/img/no-image.png',
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (context, url, _) => Image.asset(
                              'assets/img/no-image.png',
                              fit: BoxFit.cover,
                            ),
                            imageUrl: product.photo,
                            fit: BoxFit.fill,
                            height: 66.0,
                            width: 100.0,
                          ),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Column(
                        children: [
                          Text(
                            '${product.name}',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Rubik',
                            ),
                          ),
                          Text(
                            '${formatCurrency.format(product.price)}',
                            style: TextStyle(
                              fontSize: 20,
                              fontFamily: 'Rubik',
                              fontWeight: FontWeight.w500,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  StreamBuilder<Object>(
                      stream: bloc.unitsStream,
                      builder: (context, snapshot) {
                        return Row(
                          children: [
                            ClipOval(
                              child: Material(
                                color: AppColors.ACCENT_COLOR_GREEN,
                                child: InkWell(
                                  splashColor: Colors.white,
                                  child: SizedBox(
                                      width: 34,
                                      height: 34,
                                      child: Icon(Icons.remove)),
                                  onTap: snapshot.hasData
                                      ? bloc.decrementUnits
                                      : null,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                  '${snapshot.hasData ? snapshot.data : 0}'),
                            ),
                            ClipOval(
                              child: Material(
                                color: AppColors.ACCENT_COLOR_GREEN,
                                child: InkWell(
                                    splashColor: Colors.white,
                                    child: SizedBox(
                                        width: 34,
                                        height: 34,
                                        child: Icon(Icons.add)),
                                    onTap: bloc.incrementUnits),
                              ),
                            ),
                          ],
                        );
                      }),
                ],
              ),
            ),
            SizedBox(height: 15.0),
            Text(
              'Datos de compra',
              style: titleStyle,
            ),
            orderInfo(context, bloc),
          ],
        ),
      )
    ]));
  }

  // Button for execute the order
  Widget finishOrderButton(CompleteOrderBloc bloc) {
    return Positioned(
      bottom: 0.0,
      left: 0.0,
      right: 0.0,
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(
              width: 1.0,
              color: AppColors.LIGHT_CONTRAST_COLOR,
            ),
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(30.0),
              topRight: const Radius.circular(30.0),
            ),
            color: AppColors.BACKGROUND_COLOR),
        height: 110.0,
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 35.0),
          child: StreamBuilder<bool>(
              stream: bloc.formValidStream,
              builder: (context, snapshot) {
                return RaisedButton(
                  onPressed: snapshot.hasData
                      ? () => _submit(context, bloc.createOrder)
                      : null,
                  child: Center(
                    child: Text(
                      'Finalizar compra',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                        fontFamily: 'Rubik',
                      ),
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  color: AppColors.ACCENT_COLOR_GREEN,
                  // textColor: Colors.black,
                );
              }),
        ),
      ),
    );
  }

  Widget orderInfo(BuildContext ctx, CompleteOrderBloc bloc) {
    return Container(
        child: Column(
      children: [
        SizedBox(height: 10.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Dirección entrega',
                style: TextStyle(fontSize: 16, fontFamily: 'Rubik')),
            SizedBox(
              height: 10.0,
            ),
            StreamBuilder<Object>(
                stream: bloc.addressStream,
                builder: (context, snapshot) {
                  return TextField(
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.location_on,
                            color: AppColors.PRIMARY_COLOR),
                        hintStyle: TextStyle(
                            color: Color.fromRGBO(68, 68, 68, 1),
                            fontFamily: 'Rubik'),
                        filled: true,
                        fillColor: AppColors.BACKGROUND_COLOR,
                        errorText: snapshot.error,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(
                                color: AppColors.ACCENT_COLOR_GREEN)),
                        contentPadding: EdgeInsets.all(17)),
                    onChanged: bloc.changeAddress,
                  );
                }),
          ],
        ),
        SizedBox(height: 20.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Fecha entrega',
                style: TextStyle(fontSize: 16, fontFamily: 'Rubik')),
            SizedBox(
              height: 10.0,
            ),
            StreamBuilder<DateTime>(
                stream: bloc.dateStream,
                builder: (context, snapshot) {
                  return TextField(
                      onTap: () async {
                        DateTime _date = await showDatePicker(
                          context: ctx,
                          initialDate: new DateTime.now(),
                          firstDate: new DateTime.now(),
                          lastDate: new DateTime(2021),
                          locale: Locale('es', 'CO'),
                          builder: (BuildContext context, Widget child) {
                            return Theme(
                              data: ThemeData.light().copyWith(
                                primaryColor: AppColors.PRIMARY_COLOR,
                                accentColor: AppColors.ACCENT_COLOR_GREEN,
                                colorScheme: ColorScheme.highContrastLight(
                                    primary: AppColors.PRIMARY_COLOR),
                                buttonTheme: ButtonThemeData(
                                    textTheme: ButtonTextTheme.primary),
                              ),
                              child: child,
                            );
                          },
                        );
                        if (_date != null) {
                          bloc.changeDate(_date);
                        }
                      },
                      readOnly: true,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.calendar_today,
                              color: AppColors.PRIMARY_COLOR),
                          hintStyle: TextStyle(
                              color: Color.fromRGBO(68, 68, 68, 1),
                              fontFamily: 'Rubik'),
                          filled: true,
                          fillColor: AppColors.BACKGROUND_COLOR,
                          hintText: '',
                          errorText: null,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide(
                                  color: AppColors.ACCENT_COLOR_GREEN)),
                          contentPadding: EdgeInsets.all(17)),
                      controller: TextEditingController(
                          text: snapshot.data != null
                              ? dateFormat.format(snapshot.data)
                              : ''));
                }),
          ],
        ),
      ],
    ));
  }

  _submit(BuildContext context, Function send) async {
    try {
      DialogBuilder(context).showLoadingIndicator();
      final response = await send(product.id, venture?.id ?? product.ventureId);
      DialogBuilder(context).hideOpenDialog();
      if (response != null)
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => OrderDetailView(
                    orderId: response['id'],
                    venture: venture,
                    product: product,
                    fromVenture: false,
                    buyProduct: true)));
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message);
    }
  }

  void _showSnackBar(String message) {
    SnackBar snackBar = SnackBar(content: Text('$message'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
