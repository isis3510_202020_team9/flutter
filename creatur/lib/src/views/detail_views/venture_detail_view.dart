import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:creatur/src/blocs/connectivity_bloc.dart';
import 'package:creatur/src/blocs/detail/venture-detail_bloc.dart';
import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/views/detail_views/product_detail_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

/// Class that corresponds to the venture detail view
class VentureDetailView extends StatefulWidget {
  /// The venture to obtain the data
  final VentureModel venture;

  /// The default constructor for the class
  VentureDetailView({Key key, @required this.venture}) : super(key: key);

  @override
  _VentureDetailState createState() => _VentureDetailState();
}

class _VentureDetailState extends State<VentureDetailView> {
  /// The venture of the state of the widget
  VentureModel venture;

  /// The global key for the scaffold
  GlobalKey<ScaffoldState> _scaffoldKey;

  /// Saves if there's a snackbar already being shown
  bool _hasSnackBar;

  /// Stream subscription to listen to connectivity changes
  StreamSubscription<bool> _noConnListener;

  /// The connectivity bloc to check if redirecting without connectivity to external sites
  ConnectivityBloc _connectivityBloc;

  @override
  void initState() {
    super.initState();
    venture = widget.venture;
    _hasSnackBar = false;
    _scaffoldKey = GlobalKey<ScaffoldState>();
  }

  @override
  void dispose() {
    _noConnListener.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    /// The bloc for the venture detail
    final detailBloc = BlocProvider.of(context).ventureDetailBloc;

    /// Initialize the reference of the bloc
    _connectivityBloc = BlocProvider.of(context).connectivityBloc;

    /// Uses the stream to listen when internet's gone
    _noConnListener =
        _connectivityBloc.connectivityStream.listen(_showSnackBar);

    return Scaffold(
      key: _scaffoldKey,
      body: Center(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              backgroundColor: AppColors.PRIMARY_COLOR,
              expandedHeight: 230.0,
              pinned: true,
              floating: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Hero(
                  tag: venture.id,
                  child: CachedNetworkImage(
                    placeholder: (context, url) => Image.asset(
                      'assets/img/no-image.png',
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (context, url, _) => Image.asset(
                      'assets/img/no-image.png',
                      fit: BoxFit.cover,
                    ),
                    imageUrl: venture.photo,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SliverFillRemaining(
              child: Container(
                decoration: BoxDecoration(
                  color: AppColors.BACKGROUND_COLOR,
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(30.0),
                    topRight: const Radius.circular(30.0),
                  ),
                ),
                padding: EdgeInsets.only(top: 20.0),
                child: ListView(
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    /// Venture title
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        venture.name,
                        style: TextStyle(
                          fontSize: 25,
                          color: AppColors.PRIMARY_COLOR,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Rubik',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    // Venture rating and location
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20.0),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          // The price of the product
                          Expanded(
                            child: RatingBar.builder(
                              initialRating: venture.rating,
                              // minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true, itemSize: 24,
                              ignoreGestures: true,
                              itemCount: 5,
                              itemPadding: EdgeInsets.zero,
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print('The rating changed to $rating!');
                              },
                            ),
                          ),
                          // The venture's location
                          Expanded(
                            child: Row(
                              // mainAxisAlignment:
                              //     MainAxisAlignment.spaceAround,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: 60,
                                  child: Icon(
                                    Icons.location_pin,
                                    color: AppColors.ACCENT_COLOR_GREEN,
                                    size: 28.0,
                                    semanticLabel: 'Dirección',
                                  ),
                                ),
                                Container(
                                  width: 120,
                                  child: Text(
                                    venture.address,
                                    maxLines: 3,
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'Rubik',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    // Product list
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(
                          left: 20.0, bottom: 10.0, right: 20.0),
                      child: Text(
                        'Productos',
                        style: TextStyle(
                          fontSize: 20,
                          color: AppColors.PRIMARY_COLOR,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    Container(
                      height: 198.0,
                      child: _ventureProductsList(context, detailBloc),
                    ),
                    // Contact information
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin:
                          EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
                      child: Text(
                        'Contacto',
                        style: TextStyle(
                          fontSize: 20,
                          color: AppColors.PRIMARY_COLOR,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 90.0,
                      margin: EdgeInsets.symmetric(horizontal: 20.0),
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () => _showUrl(
                                venture.phone, AppConstants.PHONE_SCHEMA),
                            child: Container(
                              decoration: BoxDecoration(
                                color: AppColors.PRIMARY_COLOR,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              child: Icon(
                                Icons.phone,
                                size: 70.0,
                                color: AppColors.ACCENT_COLOR_BLUE,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => _showUrl(
                                venture.facebookUrl, AppConstants.URL_SCHEMA),
                            child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                                child: Image.asset(
                                  'assets/img/facebook-logo.png',
                                  fit: BoxFit.contain,
                                  height: 70.0,
                                )),
                          ),
                          GestureDetector(
                            onTap: () => _showUrl(
                                venture.instagramUrl, AppConstants.URL_SCHEMA),
                            child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                                child: Image.asset(
                                  'assets/img/instagram-logo.png',
                                  fit: BoxFit.contain,
                                  height: 70.0,
                                )),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Shows a snackbar if the internet connection is gone
  void _showSnackBar(bool hasInternet) {
    if (!hasInternet && !_hasSnackBar) {
      SnackBar snackBar = SnackBar(content: Text(AppErrors.NO_UPDATE_ERROR));
      _scaffoldKey.currentState
          .showSnackBar(snackBar)
          .closed
          .whenComplete(() => _hasSnackBar = false);
    }
  }

  /// Shows a snack bar for the links of the view
  void _showSnackBarLinks(String message) {
    SnackBar snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () => _scaffoldKey.currentState
            .removeCurrentSnackBar(reason: SnackBarClosedReason.action),
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  /// Shows or opens the phone app that deals with the specified resource [url]
  void _showUrl(String url, String type) async {
    if (url != null) {
      if (type == AppConstants.URL_SCHEMA) {
        if (await canLaunch(url)) {
          final bool isConn = await _connectivityBloc.lastState;
          if (isConn ?? false)
            await launch(url);
          else
            _showSnackBarLinks(AppErrors.CANT_REDIRECT_CONNECTION);
        } else {
          _showSnackBarLinks(AppErrors.CANT_REDIRECT);
        }
      } else {
        final Uri uriInfo = Uri(scheme: type, path: url);
        if (await canLaunch(uriInfo.toString())) {
          await launch(uriInfo.toString());
        } else {
          _showSnackBarLinks(AppErrors.CANT_REDIRECT);
        }
      }
    } else {
      _showSnackBarLinks(AppErrors.CANT_REDIRECT);
    }
  }

  /// Creates the list of products for that venture
  Widget _ventureProductsList(BuildContext context, VentureDetailBloc bloc) {
    /// Calls the function to load the products
    bloc.getVentureProducts(venture.id);

    return StreamBuilder<List<ProductModel>>(
      stream: bloc.ventureProductsStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return (snapshot.data.isNotEmpty)
              ? _ventureProducts(snapshot.data)
              : Center(
                  child: Container(
                    width: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          AppErrors.NO_PRODUCTS,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Rubik',
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        OutlineButton(
                          color: AppColors.PRIMARY_COLOR,
                          onPressed: () => bloc.getVentureProducts(venture.id),
                          textColor: AppColors.PRIMARY_COLOR,
                          child: Text(
                            'Reintentar',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontWeight: FontWeight.bold,
                              color: AppColors.PRIMARY_COLOR,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
        } else if (snapshot.hasError) {
          return Center(
            child: Container(
              width: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    snapshot.error.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Rubik',
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  OutlineButton(
                    color: AppColors.PRIMARY_COLOR,
                    onPressed: () => bloc.getVentureProducts(venture.id),
                    textColor: AppColors.PRIMARY_COLOR,
                    child: Text(
                      'Reintentar',
                      style: TextStyle(
                        fontFamily: 'Rubik',
                        fontWeight: FontWeight.bold,
                        color: AppColors.PRIMARY_COLOR,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }

        return Center(
          child: Theme(
            data: Theme.of(context)
                .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  /// Renders the list widget with the [data] of the parameter
  Widget _ventureProducts(List<ProductModel> products) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemExtent: 235,
      itemCount: products.length,
      itemBuilder: (context, index) => _productCard(context, products[index]),
    );
  }

  /// Builds a product card widget
  Widget _productCard(BuildContext context, ProductModel product) {
    /// the currency formatter
    final formatCurrency =
        NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');

    return Container(
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        color: AppColors.CONTRAST_COLOR_BLUE,
        elevation: 0,
        margin: EdgeInsets.only(left: 20.0),
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    ProductDetailView(product: product, venture: venture),
              ),
            );
          },
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: Hero(
                  tag: product.id,
                  child: ClipRRect(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(8.0)),
                    child: CachedNetworkImage(
                      placeholder: (context, url) => Image.asset(
                        'assets/img/no-image.png',
                        fit: BoxFit.cover,
                      ),
                      errorWidget: (context, url, _) => Image.asset(
                        'assets/img/no-image.png',
                        fit: BoxFit.cover,
                      ),
                      imageUrl: product.photo,
                      fit: BoxFit.cover,
                      height: 100,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        product.name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: AppColors.PRIMARY_COLOR,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          fontFamily: 'Rubik',
                        ),
                      ),
                      Text(
                        '${formatCurrency.format(product.price)}',
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                          fontFamily: 'Rubik',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
