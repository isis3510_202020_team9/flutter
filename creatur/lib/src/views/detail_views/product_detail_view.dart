import 'package:cached_network_image/cached_network_image.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/views/buy-product_view.dart';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// The view for the Product Detail
class ProductDetailView extends StatelessWidget {
  /// Global key for the scaffold
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  /// The product to be displayed in the detail
  final ProductModel product;

  /// The venture to be displayed in the small info (if not in the product)
  final VentureModel venture;

  /// Tells whether the user comes from his venture.
  final bool fromVenture;

  /// Function to delete the product if is from the venture menu
  final Future Function(String) deleteFunction;

  /// the default constructor for the class
  ProductDetailView(
      {Key key,
      @required this.product,
      this.venture,
      this.fromVenture,
      this.deleteFunction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// the currency formatter
    final formatCurrency =
        NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');

    return Scaffold(
      key: _scaffoldKey,
      body: Center(
        child: Stack(
          children: <Widget>[
            CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  backgroundColor: AppColors.PRIMARY_COLOR,
                  expandedHeight: 290.0,
                  pinned: true,
                  floating: true,
                  flexibleSpace: FlexibleSpaceBar(
                    background: Hero(
                      tag: product.id,
                      child: CachedNetworkImage(
                        placeholder: (context, url) => Image.asset(
                          'assets/img/no-image.png',
                          fit: BoxFit.cover,
                        ),
                        errorWidget: (context, url, _) => Image.asset(
                          'assets/img/no-image.png',
                          fit: BoxFit.cover,
                        ),
                        imageUrl: product.photo,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SliverFillRemaining(
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.BACKGROUND_COLOR,
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(30.0),
                        topRight: const Radius.circular(30.0),
                      ),
                    ),
                    padding: EdgeInsets.all(20.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          /// Product title
                          Expanded(
                            flex: 0,
                            child: Text(
                              product.name,
                              style: TextStyle(
                                fontSize: 25,
                                color: AppColors.PRIMARY_COLOR,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Rubik',
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          // Product price and venture
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                // The price of the product
                                Text(
                                  '${formatCurrency.format(product.price)}',
                                  style: TextStyle(
                                    fontSize: 27,
                                    color: AppColors.ACCENT_COLOR_BLUE,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                                // The venture that has the product
                                Container(
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0)),
                                    color: AppColors.BACKGROUND_COLOR,
                                    elevation: 0,
                                    child: Row(
                                      // crossAxisAlignment:
                                      //     CrossAxisAlignment.baseline,
                                      // mainAxisAlignment:
                                      //     MainAxisAlignment.spaceAround,
                                      children: (fromVenture ?? false)
                                          ? <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(
                                                    right: 10.0),
                                                height: 60,
                                                child: Icon(
                                                  Icons.info_outline,
                                                  color:
                                                      AppColors.PRIMARY_COLOR,
                                                  size: 32.0,
                                                  semanticLabel: 'Categoría',
                                                ),
                                              ),
                                              Container(
                                                width: 110.0,
                                                height: 60,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      'Categoría:',
                                                      style: TextStyle(
                                                        fontSize: 18,
                                                        fontFamily: 'Rubik',
                                                      ),
                                                    ),
                                                    Text(
                                                      AppConstants.cats[product
                                                          .productCategory],
                                                      maxLines: 2,
                                                      overflow:
                                                          TextOverflow.fade,
                                                      style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontFamily: 'Rubik',
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ]
                                          : <Widget>[
                                              Hero(
                                                tag: venture?.id ??
                                                    product.ventureId,
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                  child: CachedNetworkImage(
                                                    placeholder:
                                                        (context, url) =>
                                                            Image.asset(
                                                      'assets/img/no-image.png',
                                                      fit: BoxFit.cover,
                                                      height: 60.0,
                                                      width: 60.0,
                                                    ),
                                                    errorWidget:
                                                        (context, url, _) =>
                                                            Image.asset(
                                                      'assets/img/no-image.png',
                                                      fit: BoxFit.cover,
                                                    ),
                                                    imageUrl: venture?.photo ??
                                                        product.venturePhoto,
                                                    fit: BoxFit.cover,
                                                    height: 60.0,
                                                    width: 60.0,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10.0,
                                              ),
                                              Container(
                                                width: 110.0,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      'Producto de:',
                                                      style: TextStyle(
                                                        fontSize: 12,
                                                        fontFamily: 'Rubik',
                                                      ),
                                                    ),
                                                    Text(
                                                      venture?.name ??
                                                          product.ventureName,
                                                      maxLines: 2,
                                                      overflow:
                                                          TextOverflow.fade,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontFamily: 'Rubik',
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),
                          // Product description
                          Expanded(
                            flex: 0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Descripción',
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: AppColors.PRIMARY_COLOR,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Rubik',
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  product.description,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'Rubik',
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            _bottomButton(context),
          ],
        ),
      ),
      floatingActionButton: (fromVenture ?? false)
          ? FloatingActionButton(
              heroTag: 'delete-product-venture',
              backgroundColor: AppColors.REJECT_COLOR,
              foregroundColor: AppColors.BACKGROUND_COLOR,
              onPressed: () => _deleteProduct(context),
              tooltip: 'Eliminar',
              child: Icon(
                Icons.delete_forever,
              ),
            )
          : null,
    );
  }

  /// Returns the buying button positioned in the lower part or the edit one
  Widget _bottomButton(BuildContext context) {
    if (fromVenture ?? false) {
      return Container();
    } else {
      return Positioned(
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: AppColors.LIGHT_CONTRAST_COLOR,
              ),
              borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(30.0),
                topRight: const Radius.circular(30.0),
              ),
              color: AppColors.BACKGROUND_COLOR),
          height: 110.0,
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 35.0),
            child: RaisedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        BuyProductView(product: product, venture: venture),
                  ),
                );
              },
              child: Center(
                child: Text(
                  'Comprar',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                    fontFamily: 'Rubik',
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: AppColors.ACCENT_COLOR_GREEN,
              // textColor: Colors.black,
            ),
          ),
        ),
      );
    }
  }

  /// Deletes the current product if is accessed from the venture creator of it
  Future<void> _deleteProduct(BuildContext context) async {
    bool delete = await showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Eliminar Producto',
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Rubik',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  '¿Está seguro que desea eliminar este producto?',
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Rubik',
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Si',
                style: TextStyle(
                  color: AppColors.PRIMARY_COLOR,
                  fontFamily: 'Rubik',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: AppColors.PRIMARY_COLOR,
                  fontFamily: 'Rubik',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
          ],
        );
      },
    );

    if (delete ?? false) {
      try {
        DialogBuilder(context).showLoadingIndicator();
        var res = await deleteFunction(product.id);
        DialogBuilder(context).hideOpenDialog();
        Navigator.pop(context, res);
      } catch (error) {
        DialogBuilder(context).hideOpenDialog();
        _showSnackBar(error.message ?? AppErrors.PRODUCT_DELETE_ERROR);
      }
    }
  }

  /// Shows a snackbar with the message
  void _showSnackBar(String message) {
    SnackBar snackBar = SnackBar(
      content: Text(message),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
