import 'package:cached_network_image/cached_network_image.dart';
import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/utils/validation/validation.dart';
import 'package:creatur/src/views/detail_views/product_detail_view.dart';
import 'package:creatur/src/views/detail_views/venture_detail_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// The category detail view with the products and venture lists of the category
class CategoryDetailView extends StatelessWidget {
  /// The category to search ventures and products
  final String category;

  CategoryDetailView({Key key, @required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// the categoryBloc
    final categoryBloc = BlocProvider.of(context).categoryBloc;

    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.BACKGROUND_COLOR,
        appBar: AppBar(
          leading: BackButton(
            color: Colors.black,
            onPressed: () => Navigator.pop(context),
          ),
          backgroundColor: AppColors.BACKGROUND_COLOR,
          elevation: 0,
          centerTitle: true,
          title: Text(
            capitalized(AppConstants.cats[category]),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
              fontSize: 24,
              fontFamily: 'Poppins',
            ),
            textAlign: TextAlign.center,
          ),
        ),
        body: Container(
          child: RefreshIndicator(
            semanticsLabel: 'Cargando información',
            onRefresh: () async => Future.wait(
              <Future>[
                categoryBloc.getCategoryProducts(category),
                categoryBloc.getCategoryVentures(category)
              ],
            ),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 50.0),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
                  child: Text(
                    'Productos',
                    style: TextStyle(
                      fontSize: 19,
                      color: AppColors.PRIMARY_COLOR,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Poppins',
                    ),
                  ),
                ),
                Container(
                  height: 198.0,
                  child: _CategoryProductsList(
                    category: category,
                    getProductsFun: categoryBloc.getCategoryProducts,
                    productsStream: categoryBloc.categoryProductsStream,
                  ),
                ),
                SizedBox(height: 50.0),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
                  child: Text(
                    'Emprendimientos',
                    style: TextStyle(
                      fontSize: 19,
                      color: AppColors.PRIMARY_COLOR,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Poppins',
                    ),
                  ),
                ),
                Container(
                  height: 206.0,
                  child: _CategoryVenturesList(
                    category: category,
                    getVenturesFun: categoryBloc.getCategoryVentures,
                    venturesStream: categoryBloc.categoryVenturesStream,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/// The products of the category
class _CategoryProductsList extends StatelessWidget {
  /// The category to search for
  final String category;

  /// The function to update the list of products
  final Future Function(String) getProductsFun;

  /// The stream to hear from
  final Stream<List<ProductModel>> productsStream;

  /// The default constructor
  _CategoryProductsList(
      {Key key,
      @required this.category,
      @required this.getProductsFun,
      @required this.productsStream})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// the size of the context screen
    final _screenSize = MediaQuery.of(context).size;

    /// Call the product retrieval
    getProductsFun(category);

    return StreamBuilder<List<ProductModel>>(
      stream: productsStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return (snapshot.data.isNotEmpty)
              ? _suggestedProducts(_screenSize, snapshot.data)
              : Center(
                  child: Container(
                    width: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          AppErrors.NO_DATA,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Rubik',
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        OutlineButton(
                          color: AppColors.PRIMARY_COLOR,
                          onPressed: () => getProductsFun(category),
                          textColor: AppColors.PRIMARY_COLOR,
                          child: Text(
                            'Reintentar',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontWeight: FontWeight.bold,
                              color: AppColors.PRIMARY_COLOR,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
        } else if (snapshot.hasError) {
          return Center(
            child: Container(
              width: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    snapshot.error.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Rubik',
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  OutlineButton(
                    color: AppColors.PRIMARY_COLOR,
                    onPressed: () => getProductsFun(category),
                    textColor: AppColors.PRIMARY_COLOR,
                    child: Text(
                      'Reintentar',
                      style: TextStyle(
                        fontFamily: 'Rubik',
                        fontWeight: FontWeight.bold,
                        color: AppColors.PRIMARY_COLOR,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }

        return Center(
          child: Theme(
            data: Theme.of(context)
                .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  /// Renders the list widget with the [data] of the parameter
  Widget _suggestedProducts(Size size, List<ProductModel> products) {
    return ListView.builder(
      itemExtent: 235,
      scrollDirection: Axis.horizontal,
      itemCount: products.length,
      itemBuilder: (context, index) =>
          _productCard(size, context, products[index]),
    );
  }

  /// Builds a product card widget
  Widget _productCard(Size size, BuildContext context, ProductModel product) {
    /// the currency formatter
    final formatCurrency =
        NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');

    return Container(
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        color: AppColors.CONTRAST_COLOR_BLUE,
        elevation: 0,
        margin: EdgeInsets.only(left: 20.0),
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProductDetailView(product: product),
              ),
            );
          },
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: Hero(
                  tag: product.id,
                  child: ClipRRect(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(8.0)),
                    child: CachedNetworkImage(
                      placeholder: (context, url) => Image.asset(
                        'assets/img/no-image.png',
                        fit: BoxFit.cover,
                      ),
                      errorWidget: (context, url, _) => Image.asset(
                        'assets/img/no-image.png',
                        fit: BoxFit.cover,
                      ),
                      imageUrl: product.photo,
                      fit: BoxFit.cover,
                      height: 100,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        product.name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: AppColors.PRIMARY_COLOR,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          fontFamily: 'Rubik',
                        ),
                      ),
                      Text(
                        '${formatCurrency.format(product.price)}',
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                          fontFamily: 'Rubik',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// List of the ventures from a category widget
class _CategoryVenturesList extends StatelessWidget {
  /// The type of the list (passed from parent)
  final String category;

  /// The function to update the list of ventures
  final Future Function(String) getVenturesFun;

  /// The stream to hear from
  final Stream<List<VentureModel>> venturesStream;

  /// The default constructor
  _CategoryVenturesList(
      {Key key,
      @required this.category,
      @required this.getVenturesFun,
      @required this.venturesStream})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// the size of the context screen
    final _screenSize = MediaQuery.of(context).size;

    /// Call the venture retrieval
    getVenturesFun(category);

    return StreamBuilder<List<VentureModel>>(
      stream: venturesStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return (snapshot.data.isNotEmpty)
              ? _suggestedVentures(_screenSize, snapshot.data)
              : Center(
                  child: Container(
                    width: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          AppErrors.NO_DATA,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Rubik',
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        OutlineButton(
                          color: AppColors.PRIMARY_COLOR,
                          onPressed: () => getVenturesFun(category),
                          textColor: AppColors.PRIMARY_COLOR,
                          child: Text(
                            'Reintentar',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontWeight: FontWeight.bold,
                              color: AppColors.PRIMARY_COLOR,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
        } else if (snapshot.hasError) {
          return Center(
            child: Container(
              width: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    snapshot.error.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Rubik',
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  OutlineButton(
                    color: AppColors.PRIMARY_COLOR,
                    onPressed: () => getVenturesFun(category),
                    textColor: AppColors.PRIMARY_COLOR,
                    child: Text(
                      'Reintentar',
                      style: TextStyle(
                        fontFamily: 'Rubik',
                        fontWeight: FontWeight.bold,
                        color: AppColors.PRIMARY_COLOR,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }

        return Center(
          child: Theme(
            data: Theme.of(context)
                .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  /// Renders the list widget with the [data] of the parameter
  Widget _suggestedVentures(Size size, List<VentureModel> ventures) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemExtent: 235,
      itemCount: ventures.length,
      itemBuilder: (context, index) =>
          _ventureCard(size, context, ventures[index]),
    );
  }

  /// Builds a venture card widget
  Widget _ventureCard(Size size, BuildContext context, VentureModel venture) {
    return Container(
      margin: EdgeInsets.only(left: 20.0),
      child: Card(
        elevation: 0,
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => VentureDetailView(venture: venture),
              ),
            );
          },
          child: Column(
            children: <Widget>[
              Hero(
                tag: venture.id,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: CachedNetworkImage(
                    placeholder: (context, url) => Image.asset(
                      'assets/img/no-image.png',
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (context, url, _) => Image.asset(
                      'assets/img/no-image.png',
                      fit: BoxFit.cover,
                    ),
                    imageUrl: venture.photo,
                    fit: BoxFit.cover,
                    height: 180,
                  ),
                ),
              ),
              Text(
                venture.name,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Color(0xFF000000),
                  fontSize: 15.0,
                  fontFamily: 'Rubik',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
