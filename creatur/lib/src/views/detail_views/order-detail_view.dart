import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:creatur/src/blocs/activity/order-review_bloc.dart';
import 'package:creatur/src/blocs/order/order-status_bloc.dart';
import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class OrderDetailView extends StatefulWidget {
  /// The venture to show information about (id not in the product)
  final VentureModel venture;

  /// The order id
  final String orderId;

  /// Order proceeding
  final bool fromVenture;

  /// The product if it was bought
  final ProductModel product;

  /// Buy product
  final bool buyProduct;

  const OrderDetailView(
      {Key key,
      this.venture,
      @required this.orderId,
      this.fromVenture,
      this.product,
      this.buyProduct})
      : super(key: key);

  @override
  _OrderDetailViewState createState() => _OrderDetailViewState();
}

/// Order detail
class _OrderDetailViewState extends State<OrderDetailView> {
  Timer timer;

  VentureModel venture;

  ProductModel product;

  String orderId;

  bool fromVenture;

  bool buyProduct;

  DateFormat dateFormat = DateFormat.yMMMMd("es_CO");

  final formatCurrency =
      NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    orderId = widget.orderId;
    venture = widget.venture;
    fromVenture = widget.fromVenture || false;
    product = widget.product;
    buyProduct = widget.buyProduct ?? false;
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  /// Shows the snack bar with a message
  void _showSnackBar(String message, bool actions) {
    final SnackBarAction theAction = (actions)
        ? SnackBarAction(
            label: 'OK',
            onPressed: () => _scaffoldKey.currentState
                .removeCurrentSnackBar(reason: SnackBarClosedReason.dismiss),
          )
        : null;
    SnackBar snackBar = SnackBar(
      content: Text('$message'),
      action: theAction,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  /// Advances the status of the order
  void _forwardStatus(BuildContext context, OrderStatusBloc bloc) async {
    DialogBuilder(context).showLoadingIndicator();
    try {
      bloc.changeStatus(null);
      await bloc.changeOrderStatus(orderId);
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(AppErrors.SUCCESS_FORWARD_STATE, true);
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message, false);
    }
  }

  /// Rejects the status of the order
  void _rejectStatus(BuildContext context, OrderStatusBloc bloc) async {
    DialogBuilder(context).showLoadingIndicator();
    try {
      bloc.rejectStatus();
      await bloc.changeOrderStatus(orderId);
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(AppErrors.SUCCESS_FORWARD_STATE, true);
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message, false);
    }
  }

  bool enabledReview = true;

  /// Review method with loading
  void _review(BuildContext context, Function submit) async {
    try {
      DialogBuilder(context).showLoadingIndicator();
      final response = await submit(orderId);
      DialogBuilder(context).hideOpenDialog();
      if (response != null)
        setState(() {
          enabledReview = false;
        });
      _showSnackBar(AppErrors.SUCCESS_REVIEW_CREATION, true);
    } catch (error) {
      DialogBuilder(context).hideOpenDialog();
      _showSnackBar(error.message, false);
    }
  }

  @override
  Widget build(BuildContext context) {
    TextStyle header = TextStyle(
        fontFamily: 'Rubik',
        fontWeight: FontWeight.bold,
        fontSize: 24,
        color: AppColors.PRIMARY_COLOR);
    final bloc = BlocProvider.of(context).orderDetailBloc;

    final reviewBloc = BlocProvider.of(context).reviewBloc;

    bloc.getOrder(orderId);

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColors.BACKGROUND_COLOR,
      appBar: AppBar(
        leading: BackButton(
          color: Colors.black,
          onPressed: () => (buyProduct ?? false)
              ? Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false)
              : Navigator.pop(context),
        ),
        backgroundColor: AppColors.BACKGROUND_COLOR,
        elevation: 0,
      ),
      body: SafeArea(
        child: StreamBuilder<OrderModel>(
            stream: bloc.orderStream,
            builder: (ctx, snapshot) {
              if (snapshot.hasData) {
                return SingleChildScrollView(
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 25.0),
                        child: Column(
                          children: [
                            Text(
                              'Resumen Orden',
                              style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 50.0),
                            ventureInfo(snapshot.data),
                            SizedBox(height: 50.0),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Items',
                                style: header,
                              ),
                            ),
                            SizedBox(height: 10.0),
                            productsInfo(snapshot.data),
                            SizedBox(height: 50.0),
                            orderStatus(context, snapshot.data, header),
                            reviewInput(context, snapshot.data, reviewBloc),
                            SizedBox(height: 50.0),
                            priceFooter(snapshot.data?.finalPrize)
                          ],
                        )));
              } else {
                return Center(
                  child: Theme(
                    data: Theme.of(context)
                        .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            }),
      ),
    );
  }

  Widget reviewInput(
      BuildContext context, OrderModel om, OrderReviewBloc bloc) {
    enabledReview = om.status == AppConstants.STATUS_FINISHED;

    bool enabledToReview = !fromVenture && enabledReview && !om.hasReview;
    if (enabledToReview ?? false) {
      final grades = [1, 2, 3, 4, 5];
      return StreamBuilder(
          stream: bloc.gradeStream,
          builder: (context, snapshot) {
            return Column(
              children: [
                SizedBox(height: 50.0),
                Text(
                  'Califica el servicio',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontFamily: 'Rubik',
                  ),
                ),
                SizedBox(height: 18.0),
                Center(
                  child: Container(
                    padding: const EdgeInsets.only(left: 16.0, right: 10.0),
                    width: 339,
                    height: 53,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: AppColors.INPUT_BACKGROUND_COLOR,
                        border: Border.all(
                            color:
                                AppColors.ACCENT_COLOR_GREEN.withOpacity(0.59),
                            width: 1.4)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                          isExpanded: true,
                          value: snapshot.data,
                          items: grades
                              .map((item) => DropdownMenuItem(
                                  child: Text(
                                    '$item',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                    ),
                                  ),
                                  value: item))
                              .toList(),
                          onChanged: (e) {
                            bloc.changeReview(e);
                          }),
                    ),
                  ),
                ),
                SizedBox(height: 22.0),
                RaisedButton(
                  onPressed: () => _review(context, bloc.createReview),
                  child: Container(
                    width: 305,
                    height: 50,
                    child: Center(
                      child: Text(
                        'Calificar',
                        style: TextStyle(
                          fontSize: 19,
                          fontFamily: 'Rubik',
                        ),
                      ),
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  color: AppColors.ACCENT_COLOR_GREEN,
                )
              ],
            );
          });
    } else
      return SizedBox();
  }

// Venture info
  Widget ventureInfo(OrderModel om) {
    return Row(
      children: [
        Hero(
          tag: (fromVenture) ? venture?.id ?? product?.ventureId : om.id,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: CachedNetworkImage(
              placeholder: (context, url) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              imageUrl: venture?.photo ?? product?.venturePhoto,
              errorWidget: (context, url, _) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
                height: 55.0,
                width: 55.0,
              ),
              fit: BoxFit.fill,
              height: 90.0,
              width: 90.0,
            ),
          ),
        ),
        SizedBox(width: 10.0),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${venture?.name ?? product?.ventureName}',
                style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Rubik',
                  fontWeight: FontWeight.w500,
                  color: AppColors.PRIMARY_COLOR,
                ),
              ),
              Text(
                '${dateFormat.format(om.desiredDate)}',
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: 'Rubik',
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

// Product info
  Widget productsInfo(OrderModel om) {
    final product = om.products[0];
    return Row(
      children: [
        Hero(
          tag: '${product.id}${om.id}',
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: CachedNetworkImage(
              placeholder: (context, url) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              errorWidget: (context, url, _) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              imageUrl: product.photo,
              fit: BoxFit.fill,
              height: 66.0,
              width: 100.0,
            ),
          ),
        ),
        SizedBox(width: 5.0),
        Column(
          children: [
            Text(
              '${product.name}',
              style: TextStyle(
                fontSize: 16,
                fontFamily: 'Rubik',
              ),
            ),
            Text(
              '${formatCurrency.format(product.price)}',
              style: TextStyle(
                fontSize: 20,
                fontFamily: 'Rubik',
                fontWeight: FontWeight.w500,
              ),
            )
          ],
        ),
      ],
    );
  }

  /// Order status widget
  Widget orderStatus(BuildContext context, OrderModel om, TextStyle header) {
    final bloc = BlocProvider.of(context).orderStatusBloc;

    bloc.changeStatus(om.status);

    return StreamBuilder<Object>(
        stream: bloc.statusStream,
        builder: (context, snapshot) {
          final status = snapshot.data;
          bool completed = status == AppConstants.STATUS_FINISHED;
          bool onMyWay = completed || status == AppConstants.STATUS_ON_WAY;
          bool accepted = onMyWay || status == AppConstants.STATUS_IN_PROGRESS;
          bool checked = accepted || status == AppConstants.STATUS_NOT_STARTED;
          bool rejected = status == AppConstants.STATUS_DECLINED;

          bool canReject = status == AppConstants.STATUS_NOT_STARTED;

          return Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Proceso',
                  style: header,
                ),
                manageStatusButtons(completed, canReject, bloc),
              ],
            ),
            SizedBox(height: 10.0),
            Column(children: [
              statusIcon(completed, 'Completada', rejected),
              statusIcon(onMyWay, 'En camino', rejected),
              statusIcon(accepted, 'Aceptada', rejected),
              statusIcon(checked, 'Recibida', rejected),
            ])
          ]);
        });
  }

  // Manage status buttons
  Widget manageStatusButtons(
      bool finished, bool canReject, OrderStatusBloc bloc) {
    if (fromVenture ?? false) {
      return Visibility(
        visible: !finished,
        child: Row(
          children: [
            Tooltip(
              message: 'Avanzar',
              child: ClipOval(
                child: Material(
                  color: AppColors.ACCENT_COLOR_GREEN,
                  child: InkWell(
                      // splashColor: Colors.white,
                      child: SizedBox(
                          width: 34,
                          height: 34,
                          child: Icon(
                            Icons.fast_forward,
                            color: Colors.white,
                            semanticLabel: 'Avanzar',
                          )),
                      onTap: () => _forwardStatus(context, bloc)),
                ),
              ),
            ),
            SizedBox(width: 10.0),
            Visibility(
              visible: canReject,
              child: Tooltip(
                message: 'Rechazar',
                child: ClipOval(
                  child: Material(
                    color: AppColors.REJECT_COLOR,
                    child: InkWell(
                        // splashColor: Colors.white,
                        child: SizedBox(
                            width: 34,
                            height: 34,
                            child: Icon(
                              Icons.cancel_rounded,
                              color: Colors.white,
                              semanticLabel: 'Rechazar',
                            )),
                        onTap: () => _rejectStatus(context, bloc)),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }
    return SizedBox();
  }

  /// STATUS ICON
  Widget statusIcon(bool check, String status, bool rejected) {
    IconData data = rejected ? Icons.remove : Icons.check;
    Widget child =
        check ? Icon(data, color: AppColors.BACKGROUND_COLOR) : SizedBox();
    Color backgroundColor = check
        ? AppColors.ACCENT_COLOR_GREEN
        : rejected
            ? AppColors.REJECT_COLOR
            : AppColors.STATUS_COLOR;
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleAvatar(
            backgroundColor: backgroundColor,
            child: child,
            minRadius: 14.5,
            maxRadius: 17,
          ),
          SizedBox(width: 25.0),
          Text(
            '$status',
            style: TextStyle(fontFamily: 'Poppins', fontSize: 14),
          )
        ],
      ),
    );
  }

  /// Price footer
  Widget priceFooter(int price) {
    return Container(
      height: 70.0,
      padding: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
        color: AppColors.LIGHT_CONTRAST_COLOR,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Total',
            style: TextStyle(
                fontFamily: 'Rubik', fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: AppColors.PRIMARY_COLOR,
            ),
            width: 98,
            child: Center(
              child: Text(
                '${formatCurrency.format(price)}',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}
