import 'package:cached_network_image/cached_network_image.dart';
import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/views/detail_views/order-detail_view.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// Class that corresponds to the activity page of the user
class ActivityPageView extends StatefulWidget {
  @override
  _ActivityPageState createState() => _ActivityPageState();
}

/// Main state of the activity page widget
class _ActivityPageState extends State<ActivityPageView> {
  @override
  Widget build(BuildContext context) {
    /// The activity Bloc of the user
    final activityBloc = BlocProvider.of(context).activityBloc;

    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.BACKGROUND_COLOR,
        appBar: AppBar(
          backgroundColor: AppColors.BACKGROUND_COLOR,
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Tu actividad',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
              fontSize: 24,
              fontFamily: 'Poppins',
            ),
            textAlign: TextAlign.center,
          ),
        ),
        body: _OrdersList(
          getOrdersFun: activityBloc.getUserOrders,
          ordersStream: activityBloc.userOrdersStream,
        ),
      ),
    );
  }
}

/// The orders list of the user
class _OrdersList extends StatelessWidget {
  /// The function to update the list of orders
  final Function getOrdersFun;

  /// The stream to hear from
  final Stream<List<OrderModel>> ordersStream;

  /// The default constructor
  _OrdersList({
    Key key,
    @required this.getOrdersFun,
    @required this.ordersStream,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// Calls the function to retrieve the orders
    getOrdersFun();

    return Container(
      child: RefreshIndicator(
        semanticsLabel: 'Cargando información',
        onRefresh: getOrdersFun,
        child: _ordersListStreamBuilder(),
      ),
    );
  }

  /// Loads the orders of the venture
  Widget _ordersListStreamBuilder() {
    return StreamBuilder<List<OrderModel>>(
      stream: ordersStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return (snapshot.data.isNotEmpty)
              ? _ordersListItems(snapshot.data)
              : ListView(
                  itemExtent: 300,
                  children: <Widget>[
                    Center(
                      child: Container(
                        width: 250,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppErrors.NO_ORDERS_ACTIVITY,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'Rubik',
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            OutlineButton(
                              color: AppColors.PRIMARY_COLOR,
                              onPressed: getOrdersFun,
                              textColor: AppColors.PRIMARY_COLOR,
                              child: Text(
                                'Reintentar',
                                style: TextStyle(
                                  fontFamily: 'Rubik',
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.PRIMARY_COLOR,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                );
        } else if (snapshot.hasError) {
          return ListView(
            itemExtent: 300,
            children: <Widget>[
              Center(
                child: Container(
                  width: 250,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        snapshot.error.toString(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Rubik',
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      OutlineButton(
                        color: AppColors.PRIMARY_COLOR,
                        onPressed: getOrdersFun,
                        textColor: AppColors.PRIMARY_COLOR,
                        child: Text(
                          'Reintentar',
                          style: TextStyle(
                            fontFamily: 'Rubik',
                            fontWeight: FontWeight.bold,
                            color: AppColors.PRIMARY_COLOR,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
        }

        return Center(
          child: Theme(
            data: Theme.of(context)
                .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  /// Renders the orders of the list into it
  Widget _ordersListItems(List<OrderModel> orders) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: orders.length,
        itemBuilder: (context, index) => _orderTile(context, orders[index]),
      ),
    );
  }

  /// Creates a order tile
  Widget _orderTile(BuildContext context, OrderModel order) {
    /// the currency and date formatters
    final formatCurrency =
        NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');
    final DateFormat formatDate = DateFormat.yMd("es_CO");

    return Card(
      margin: const EdgeInsets.only(top: 20.0),
      elevation: 3.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: ListTile(
        contentPadding: const EdgeInsets.all(8.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        onTap: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OrderDetailView(
                venture: order.venture,
                orderId: order.id,
                fromVenture: false,
              ),
            ),
          );
          //Refresh the orders when going back, so that they are always up to date
          getOrdersFun();
        },
        leading: Hero(
          tag: order.id,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: CachedNetworkImage(
              placeholder: (context, url) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              errorWidget: (context, url, _) => Image.asset(
                'assets/img/no-image.png',
                fit: BoxFit.cover,
              ),
              imageUrl: order.venture.photo,
              fit: BoxFit.cover,
              height: 70.0,
              width: 80.0,
            ),
          ),
        ),
        title: Text(
          order.venture.name,
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontFamily: 'Rubik',
            fontWeight: FontWeight.w500,
            color: AppColors.PRIMARY_COLOR,
            fontSize: 20,
          ),
        ),
        subtitle: _orderStatus(order.status),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(
              '${formatDate.format(order.desiredDate)}',
              style: TextStyle(
                fontFamily: 'Rubik',
                fontSize: 12,
              ),
            ),
            Text(
              '${formatCurrency.format(order.finalPrize)}',
              style: TextStyle(
                fontFamily: 'Rubik',
                fontWeight: FontWeight.w500,
                fontSize: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Retrieves the order status
  Widget _orderStatus(String status) {
    final Color color = AppColors.statusColors[status];
    final String statusStr = AppConstants.statusMap[status];

    return Container(
      // padding: const EdgeInsets.symmetric(horizontal: 10.0),
      width: 60,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Text(
        statusStr,
        textAlign: TextAlign.center,
        style:
            TextStyle(fontFamily: 'Poppins', fontSize: 14, color: Colors.black),
      ),
    );
  }
}
