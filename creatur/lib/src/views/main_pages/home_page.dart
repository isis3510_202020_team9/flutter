import 'dart:async';

import 'package:creatur/src/blocs/provider.dart';
import 'package:creatur/src/blocs/recommended/recommended_bloc.dart';
import 'package:creatur/src/components/dialog_builder.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/views/detail_views/product_detail_view.dart';
import 'package:creatur/src/views/detail_views/venture_detail_view.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';

/// The class that corresponds to the main tab of the home view
class HomePageView extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

/// The home page main state
class _HomePageState extends State<HomePageView> {
  // /// The global key for the scaffold
  // GlobalKey<ScaffoldState> _scaffoldKey;

  // /// Saves if there's a snackbar already being shown
  // bool _hasSnackBar;

  // /// Stream subscription to listen to connectivity changes
  // StreamSubscription<bool> _noConnListener;

  // @override
  // void initState() {
  //   super.initState();
  //   _hasSnackBar = false;
  //   _scaffoldKey = GlobalKey<ScaffoldState>();
  // }

  @override
  Widget build(BuildContext context) {
    // /// Initialize the reference of the bloc
    // final _connectivityBloc = BlocProvider.of(context).connectivityBloc;

    // /// Uses the stream to listen when internet's gone
    // _noConnListener =
    //     _connectivityBloc.connectivityStream.listen(_showSnackBar);

    return SafeArea(
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          // key: _scaffoldKey,
          backgroundColor: AppColors.BACKGROUND_COLOR,
          appBar: AppBar(
            backgroundColor: AppColors.BACKGROUND_COLOR,
            elevation: 0,
            bottom: TabBar(
              isScrollable: false,
              labelPadding: EdgeInsets.zero,
              indicatorWeight: 3.0,
              indicatorColor: AppColors.ACCENT_COLOR_BLUE,
              labelColor: Color(0xFF484848),
              labelStyle: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12,
                fontFamily: 'Poppins',
              ),
              tabs: <Widget>[
                Tab(
                  text: "RECOMENDADOS",
                ),
                Tab(
                  text: "CERCA DE TI",
                ),
                Tab(
                  text: "TENDENCIAS",
                ),
              ],
            ),
            title: Text(
              'Creatur',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: AppColors.PRIMARY_COLOR,
                fontSize: 22,
                fontFamily: 'Poppins',
              ),
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.logout,
                  color: AppColors.PRIMARY_COLOR,
                ),
                onPressed: () => _logout(context),
                tooltip: 'Salir',
              ),
            ],
          ),
          body: TabBarView(
            children: <Widget>[
              _ContentLists(category: AppConstants.RECOMMENDED_SECTION),
              _ContentLists(category: AppConstants.NEAR_SECTION),
              _ContentLists(category: AppConstants.TENDENCIES_SECTION),
            ],
          ),
        ),
      ),
    );
  }

  // /// Shows a snackbar if the internet connection is gone
  // void _showSnackBar(bool hasInternet) {
  //   if (!hasInternet && !_hasSnackBar) {
  //     SnackBar snackBar = SnackBar(content: Text(AppErrors.NO_UPDATE_ERROR));
  //     _scaffoldKey.currentState
  //         .showSnackBar(snackBar)
  //         .closed
  //         .whenComplete(() => _hasSnackBar = false);
  //   }
  // }

  // @override
  // void dispose() {
  //   _noConnListener.cancel();
  //   super.dispose();
  // }

  /// Function to logout
  void _logout(BuildContext context) async {
    DialogBuilder(context).showLoadingIndicator();
    await BlocProvider.of(context).recommendedBloc.logout();
    DialogBuilder(context).hideOpenDialog();
    Navigator.pushReplacementNamed(context, '/login');
  }
}

class _ContentLists extends StatelessWidget {
  /// The category of the list
  /// [0] -> recommended Lists
  /// [1] -> near lists
  /// [2] -> tendencies list
  final int category;

  /// The default constructor
  _ContentLists({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// Reference to the Bloc
    final recommendedBloc = BlocProvider.of(context).recommendedBloc;

    /// Initialize the function to refresh
    Future Function() refresh =
        _initializeRefreshFunction(category, recommendedBloc);

    return Container(
      child: RefreshIndicator(
        semanticsLabel: 'Cargando información',
        onRefresh: refresh,
        child: ListView(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
              child: Text(
                'Productos',
                style: TextStyle(
                  fontSize: 19,
                  color: AppColors.PRIMARY_COLOR,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Poppins',
                ),
              ),
            ),
            Container(
              height: 198.0,
              child: _SuggestedProductsList(
                type: category,
                getProductsFun: _getProductsFunction(category, recommendedBloc),
                productsStream: _getProductsStream(category, recommendedBloc),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
              child: Text(
                'Emprendimientos',
                style: TextStyle(
                  fontSize: 19,
                  color: AppColors.PRIMARY_COLOR,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Poppins',
                ),
              ),
            ),
            Container(
              height: 206.0,
              child: _SuggestedVenturesList(
                type: category,
                getVenturesFun: _getVenturesFunction(category, recommendedBloc),
                venturesStream: _getVenturesStream(category, recommendedBloc),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Returns the products function to use to update the stream
  Future Function() _getProductsFunction(int section, RecommendedBloc bloc) {
    Function chosen;
    switch (section) {
      case AppConstants.RECOMMENDED_SECTION:
        chosen = bloc.getSuggestedProducts;
        break;
      case AppConstants.NEAR_SECTION:
        chosen = bloc.getNear;
        break;
      case AppConstants.TENDENCIES_SECTION:
        chosen = bloc.getTrendyProducts;
        break;
      default:
        chosen = bloc.getSuggestedProducts;
        break;
    }
    return chosen;
  }

  /// Returns the ventures function to use to update the stream
  Future Function() _getVenturesFunction(int section, RecommendedBloc bloc) {
    Function chosen;
    switch (section) {
      case AppConstants.RECOMMENDED_SECTION:
        chosen = bloc.getSuggestedVentures;
        break;
      case AppConstants.NEAR_SECTION:
        chosen = bloc.getNear;
        break;
      case AppConstants.TENDENCIES_SECTION:
        chosen = bloc.getTrendyVentures;
        break;
      default:
        chosen = bloc.getSuggestedVentures;
        break;
    }
    return chosen;
  }

  /// Creates the refresh function to update on pull
  Future Function() _initializeRefreshFunction(
      int section, RecommendedBloc bloc) {
    if (section == AppConstants.NEAR_SECTION) {
      return bloc.getNear;
    }
    Future Function() prod = _getProductsFunction(section, bloc);
    Future Function() vent = _getVenturesFunction(section, bloc);
    return () async {
      return Future.wait(<Future>[prod(), vent()]);
    };
  }

  /// Gets the stream for the product list
  Stream<List<ProductModel>> _getProductsStream(
      int section, RecommendedBloc bloc) {
    Stream chosen;
    switch (section) {
      case AppConstants.RECOMMENDED_SECTION:
        chosen = bloc.suggestedProductsStream;
        break;
      case AppConstants.NEAR_SECTION:
        chosen = bloc.nearProductsStream;
        break;
      case AppConstants.TENDENCIES_SECTION:
        chosen = bloc.trendyProductsStream;
        break;
      default:
        chosen = bloc.nearProductsStream;
        break;
    }
    return chosen;
  }

  /// Gets the stream for the venture list
  Stream<List<VentureModel>> _getVenturesStream(
      int section, RecommendedBloc bloc) {
    Stream chosen;
    switch (section) {
      case AppConstants.RECOMMENDED_SECTION:
        chosen = bloc.suggestedVenturesStream;
        break;
      case AppConstants.NEAR_SECTION:
        chosen = bloc.nearVenturesStream;
        break;
      case AppConstants.TENDENCIES_SECTION:
        chosen = bloc.trendyVenturesStream;
        break;
      default:
        chosen = bloc.nearVenturesStream;
        break;
    }
    return chosen;
  }
}

/// The suggested products list widget
class _SuggestedProductsList extends StatelessWidget {
  /// The type of the list (passed from parent)
  final int type;

  /// The function to update the list of products
  final Function() getProductsFun;

  /// The stream to hear from
  final Stream<List<ProductModel>> productsStream;

  /// The page controller for the pagination
  final ScrollController _pageController;

  /// The default constructor
  _SuggestedProductsList(
      {Key key,
      @required this.type,
      @required this.getProductsFun,
      @required this.productsStream})
      : _pageController = ScrollController(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    /// the size of the context screen
    final _screenSize = MediaQuery.of(context).size;

    /// Listen when the page changes
    if (type == AppConstants.RECOMMENDED_SECTION) {
      _pageController.addListener(() {
        if ((_pageController.position.pixels + 150) >=
            _pageController.position.maxScrollExtent) {
          getProductsFun();
        }
      });
    }

    /// Call the product retrieval
    getProductsFun();

    return StreamBuilder<List<ProductModel>>(
      stream: productsStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return (snapshot.data.isNotEmpty)
              ? _suggestedProducts(_screenSize, snapshot.data)
              : Center(
                  child: Container(
                    width: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          AppErrors.NO_DATA,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Rubik',
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        OutlineButton(
                          color: AppColors.PRIMARY_COLOR,
                          onPressed: getProductsFun,
                          textColor: AppColors.PRIMARY_COLOR,
                          child: Text(
                            'Reintentar',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontWeight: FontWeight.bold,
                              color: AppColors.PRIMARY_COLOR,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
        } else if (snapshot.hasError) {
          return Center(
            child: Container(
              width: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    snapshot.error.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Rubik',
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  OutlineButton(
                    color: AppColors.PRIMARY_COLOR,
                    onPressed: getProductsFun,
                    textColor: AppColors.PRIMARY_COLOR,
                    child: Text(
                      'Reintentar',
                      style: TextStyle(
                        fontFamily: 'Rubik',
                        fontWeight: FontWeight.bold,
                        color: AppColors.PRIMARY_COLOR,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }

        return Center(
          child: Theme(
            data: Theme.of(context)
                .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  /// Renders the list widget with the [data] of the parameter
  Widget _suggestedProducts(Size size, List<ProductModel> products) {
    return ListView.builder(
      itemExtent: 235,
      scrollDirection: Axis.horizontal,
      controller: _pageController,
      itemCount: products.length,
      itemBuilder: (context, index) =>
          _productCard(size, context, products[index]),
    );
  }

  /// Builds a product card widget
  Widget _productCard(Size size, BuildContext context, ProductModel product) {
    /// the currency formatter
    final formatCurrency =
        NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'en_CO');

    return Container(
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        color: AppColors.CONTRAST_COLOR_BLUE,
        elevation: 0,
        margin: EdgeInsets.only(left: 20.0),
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProductDetailView(product: product),
              ),
            );
          },
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: Hero(
                  tag: product.id,
                  child: ClipRRect(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(8.0)),
                    child: CachedNetworkImage(
                      placeholder: (context, url) => Image.asset(
                        'assets/img/no-image.png',
                        fit: BoxFit.cover,
                      ),
                      errorWidget: (context, url, _) => Image.asset(
                        'assets/img/no-image.png',
                        fit: BoxFit.cover,
                      ),
                      imageUrl: product.photo,
                      fit: BoxFit.cover,
                      height: 100,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        product.name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: AppColors.PRIMARY_COLOR,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          fontFamily: 'Rubik',
                        ),
                      ),
                      Text(
                        '${formatCurrency.format(product.price)}',
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                          fontFamily: 'Rubik',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// List of the suggested ventures widget
class _SuggestedVenturesList extends StatelessWidget {
  /// The type of the list (passed from parent)
  final int type;

  /// The function to update the list of ventures
  final Function() getVenturesFun;

  /// The stream to hear from
  final Stream<List<VentureModel>> venturesStream;

  /// The default constructor
  _SuggestedVenturesList(
      {Key key,
      @required this.type,
      @required this.getVenturesFun,
      @required this.venturesStream})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// the size of the context screen
    final _screenSize = MediaQuery.of(context).size;

    /// Call the venture retrieval
    getVenturesFun();

    return StreamBuilder<List<VentureModel>>(
      stream: venturesStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return (snapshot.data.isNotEmpty)
              ? _suggestedVentures(_screenSize, snapshot.data)
              : Center(
                  child: Container(
                    width: 250,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          AppErrors.NO_DATA,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Rubik',
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        OutlineButton(
                          color: AppColors.PRIMARY_COLOR,
                          onPressed: getVenturesFun,
                          textColor: AppColors.PRIMARY_COLOR,
                          child: Text(
                            'Reintentar',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontWeight: FontWeight.bold,
                              color: AppColors.PRIMARY_COLOR,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
        } else if (snapshot.hasError) {
          return Center(
            child: Container(
              width: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    snapshot.error.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Rubik',
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  OutlineButton(
                    color: AppColors.PRIMARY_COLOR,
                    onPressed: getVenturesFun,
                    textColor: AppColors.PRIMARY_COLOR,
                    child: Text(
                      'Reintentar',
                      style: TextStyle(
                        fontFamily: 'Rubik',
                        fontWeight: FontWeight.bold,
                        color: AppColors.PRIMARY_COLOR,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }

        return Center(
          child: Theme(
            data: Theme.of(context)
                .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  /// Renders the list widget with the [data] of the parameter
  Widget _suggestedVentures(Size size, List<VentureModel> ventures) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemExtent: 235,
      itemCount: ventures.length,
      itemBuilder: (context, index) =>
          _ventureCard(size, context, ventures[index]),
    );
  }

  /// Builds a venture card widget
  Widget _ventureCard(Size size, BuildContext context, VentureModel venture) {
    return Container(
      margin: EdgeInsets.only(left: 20.0),
      child: Card(
        elevation: 0,
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => VentureDetailView(venture: venture),
              ),
            );
          },
          child: Column(
            children: <Widget>[
              Hero(
                tag: venture.id,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: CachedNetworkImage(
                    placeholder: (context, url) => Image.asset(
                      'assets/img/no-image.png',
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (context, url, _) => Image.asset(
                      'assets/img/no-image.png',
                      fit: BoxFit.cover,
                    ),
                    imageUrl: venture.photo,
                    fit: BoxFit.cover,
                    height: 180,
                  ),
                ),
              ),
              Text(
                venture.name,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Color(0xFF000000),
                  fontSize: 15.0,
                  fontFamily: 'Rubik',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
