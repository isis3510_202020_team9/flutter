import 'dart:async';

import 'package:creatur/src/blocs/venture/venture-home_bloc.dart';
import 'package:creatur/src/blocs/venture_provider.dart';
import 'package:creatur/src/components/button.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:creatur/src/views/venture/venture-home_view.dart';
import 'package:flutter/material.dart';

class VenturePageView extends StatefulWidget {
  @override
  _VenturePageViewState createState() => _VenturePageViewState();
}

class _VenturePageViewState extends State<VenturePageView> {
  /// Tells if the user has a venture
  bool hasVenture;

  StreamController _ventureStream;

  @override
  void initState() {
    super.initState();
    hasVenture = false;
    _ventureStream = StreamController<VentureModel>();
  }

  /// Retrieves the venture of the user
  Future<VentureModel> retrieveVenture(
      VentureHomeBloc bloc, BuildContext context) async {
    try {
      final VentureModel venture = await bloc.venture;
      _ventureStream.sink.add(venture);
      return venture;
    } catch (e) {
      _ventureStream.sink.addError(e?.message ?? AppErrors.VENTURE_HOME_ERROR);
      return null;
    }
  }

  @override
  void dispose() {
    _ventureStream.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _ventureContent(context),
    );
  }

  /// pushes a view to create a venture and if it completes then changes the state
  _createVentureAction(BuildContext context) async {
    final success = await Navigator.of(context).pushNamed('/createVenture');
    if (success ?? false) {
      setState(() {
        hasVenture = VentureProvider.of(context).ventureHomeBloc.ownsVenture;
      });
    }
  }

// Widget shows create venture button or venture info
  Widget _ventureContent(context) {
    // The venture bloc
    final ventureBloc = VentureProvider.of(context).ventureHomeBloc;
    hasVenture = ventureBloc.ownsVenture;

    if (hasVenture) {
      // Calls to get the venture
      retrieveVenture(ventureBloc, context);

      return StreamBuilder<VentureModel>(
        stream: _ventureStream.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return (snapshot.data != null)
                ? VentureHomeView(venture: snapshot.data)
                : Center(
                    child: Container(
                      width: 250,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            AppErrors.CONNECTION,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Rubik',
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          OutlineButton(
                            color: AppColors.PRIMARY_COLOR,
                            onPressed: () =>
                                retrieveVenture(ventureBloc, context),
                            textColor: AppColors.PRIMARY_COLOR,
                            child: Text(
                              'Reintentar',
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontWeight: FontWeight.bold,
                                color: AppColors.PRIMARY_COLOR,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
          } else if (snapshot.hasError) {
            return Center(
              child: Container(
                width: 250,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      snapshot.error.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Rubik',
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    OutlineButton(
                      color: AppColors.PRIMARY_COLOR,
                      onPressed: () => retrieveVenture(ventureBloc, context),
                      textColor: AppColors.PRIMARY_COLOR,
                      child: Text(
                        'Reintentar',
                        style: TextStyle(
                          fontFamily: 'Rubik',
                          fontWeight: FontWeight.bold,
                          color: AppColors.PRIMARY_COLOR,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          }

          return Center(
            child: Theme(
              data: Theme.of(context)
                  .copyWith(accentColor: AppColors.ACCENT_COLOR_BLUE),
              child: CircularProgressIndicator(),
            ),
          );
        },
      );
    } else {
      return Scaffold(
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: AssetImage('assets/img/into-venture.png'),
              ),
              CreaturMainButton(
                text: 'Crea tu propio emprendimiento',
                onPressed: () => _createVentureAction(context),
              ),
            ],
          ),
        ),
      );
    }
  }
}
