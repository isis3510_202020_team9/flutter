import 'package:creatur/src/utils/app_colors.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/validation/validation.dart';
import 'package:creatur/src/views/detail_views/category_detail_view.dart';
import 'package:flutter/material.dart';

/// Class that corresponds to the explore page
class ExplorePageView extends StatefulWidget {
  @override
  _ExplorePageState createState() => _ExplorePageState();
}

/// Main state of the explore page widget
class _ExplorePageState extends State<ExplorePageView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.BACKGROUND_COLOR,
        appBar: AppBar(
          backgroundColor: AppColors.BACKGROUND_COLOR,
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Explora',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
              fontSize: 24,
              fontFamily: 'Poppins',
            ),
            textAlign: TextAlign.center,
          ),
        ),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  // flex: 1,
                  // width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    'Categorías',
                    style: TextStyle(
                      fontSize: 24,
                      color: AppColors.PRIMARY_COLOR,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Rubik',
                    ),
                  ),
                ),
                Container(
                  // width: MediaQuery.of(context).size.width,
                  // margin: EdgeInsets.symmetric(horizontal: 20.0),
                  // height: 500,
                  child: _categoriesList(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Returns the categories rendered in screen
  Widget _categoriesList() {
    return GridView.count(
      // padding: EdgeInsets.zero,
      crossAxisCount: 2,
      shrinkWrap: true,
      // primary: false,
      crossAxisSpacing: 25.0,
      mainAxisSpacing: 25.0,
      children: AppConstants.catsList
          .map((item) => Container(
                height: 83,
                width: 153,
                child: InkWell(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (builder) => CategoryDetailView(category: item),
                    ),
                  ),
                  highlightColor: Colors.white70,
                  child: Container(
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        Positioned.fill(
                          child: Container(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: Image.asset(
                                'assets/img/${item.toLowerCase()}.png',
                                color: Color.fromRGBO(14, 197, 230, 0.85),
                                colorBlendMode: BlendMode.modulate,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          height: 83,
                          width: 153,
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Text(
                              capitalized(AppConstants.cats[item]),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 24,
                                color: AppColors.BACKGROUND_COLOR,
                                fontWeight: FontWeight.w800,
                                fontFamily: 'Poppins',
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ))
          .toList(),
    );
  }
}
