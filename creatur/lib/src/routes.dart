import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/storage.dart';
import 'package:creatur/src/views/home_view.dart';
import 'package:creatur/src/views/login_view.dart';
import 'package:creatur/src/views/registration_view.dart';
import 'package:creatur/src/views/profiling_view.dart';
import 'package:creatur/src/views/venture/create-product_view.dart';
import 'package:creatur/src/views/venture/create-venture_view.dart';
import 'package:flutter/material.dart';

final prefs = UserPreferences();

final creaturRoutes = {
  '/': (BuildContext context) => firstPage(),
  '/login': (BuildContext context) => LoginView(),
  '/home': (BuildContext context) => HomeView(),
  '/register': (BuildContext context) => RegistrationView(),
  '/profiling': (BuildContext context) => ProfilingView(),
  '/createVenture': (BuildContext context) => CreateVentureView(),
  '/createProduct': (BuildContext context) => CreateProductView()
};
// Know if the client is already login
Widget firstPage() {
  final prof = prefs.getBool(AppConstants.HAS_PROFILING);
  if (prefs.getKey(AppConstants.TOKEN) == '') {
    return LoginView();
  } else if (prof == '' || !prof) {
    return ProfilingView();
  } else {
    return HomeView();
  }
}
