import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';

/// Represents a new dialog widget with a specific content inside
class DialogBuilder {
  DialogBuilder(this.context);

  final BuildContext context;

  /// Shows a circular progress indicator
  void showLoadingIndicator([String text]) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              backgroundColor: Colors.white,
              content: LoadingIndicator(),
            ));
      },
    );
  }

  /// Hide the dialog
  void hideOpenDialog() {
    Navigator.of(context).pop();
  }
}

/// Loading indicator widget
class LoadingIndicator extends StatelessWidget {
  LoadingIndicator();
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(16),
        color: Colors.white,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              _getLoadingIndicator(context),
            ]));
  }

  /// Loading indicator style and dimensions
  Padding _getLoadingIndicator(BuildContext context) {
    return Padding(
      child: Container(
        child: Theme(
          data: Theme.of(context)
              .copyWith(accentColor: AppColors.ACCENT_COLOR_GREEN),
          child: CircularProgressIndicator(
            strokeWidth: 3,
          ),
        ),
        width: 32,
        height: 32,
      ),
      padding: EdgeInsets.only(bottom: 16),
    );
  }
}
