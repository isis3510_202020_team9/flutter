import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';

class CreaturMainButton extends StatefulWidget {
  final Function onPressed;
  final String text;

  CreaturMainButton({Key key, this.onPressed, this.text}) : super(key: key);

  @override
  _CreaturMainButtonState createState() => _CreaturMainButtonState();
}

class _CreaturMainButtonState extends State<CreaturMainButton> {
  Function onPressed;
  String text;

  @override
  void initState() {
    super.initState();
    onPressed = widget.onPressed;
    text = widget.text;
  }

  @override
  void didUpdateWidget(covariant CreaturMainButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.onPressed != oldWidget.onPressed) {
      setState(() {
        onPressed = widget.onPressed;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
      child: Container(
        width: 305,
        height: 36,
        child: Center(
          child: Text(
            text,
            style: TextStyle(fontSize: 16, fontFamily: 'Rubik'),
          ),
        ),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      color: AppColors.PRIMARY_COLOR,
      textColor: AppColors.BACKGROUND_COLOR,
    );
  }
}
