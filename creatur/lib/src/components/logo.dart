import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';

Widget logoType() {
  return Column(
    children: [
      Image(image: AssetImage('assets/img/logo.png'), width: 192, height: 192),
      Stack(
        children: [
          Text(
            'Creatur',
            style: TextStyle(
              // color: AppColors.PRIMARY_COLOR,
              fontSize: 30,
              fontFamily: 'Poppins',
              foreground: Paint()
                ..style = PaintingStyle.stroke
                ..strokeWidth = 1
                ..color = AppColors.PRIMARY_COLOR,
            ),
          ),
          Text(
            'Creatur',
            style: TextStyle(
                color: AppColors.PRIMARY_COLOR,
                fontSize: 30,
                fontFamily: 'Poppins'),
          )
        ],
      )
    ],
  );
}
