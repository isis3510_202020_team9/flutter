import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';

/// The search bar that's going to be used in the explore tab (temp in the home tab)
class SearchBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      width: 315,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: AppColors.LIGHT_CONTRAST_COLOR,
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: TextField(
          enabled: false,
          decoration: InputDecoration(
            hintText: 'Buscar',
            hintStyle: TextStyle(
                color: Color(0xff525252), fontSize: 15, fontFamily: 'Rubik'),
            prefixIcon: Icon(
              Icons.search,
              color: AppColors.PRIMARY_COLOR,
            ),
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }
}
