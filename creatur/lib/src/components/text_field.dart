import 'package:creatur/src/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// Creatur default text field
class CreaturTextField extends StatefulWidget {
  /// Hint
  final String hint;

  /// Label
  final String label;

  /// Error to be shown
  final String error;

  /// Obscure text
  final bool obscureText;

  /// Max length allowed for the text field
  final int maxLength;

  /// Function to be executed when the field value changes
  final Function onChanged;

  /// Input type
  final TextInputType textInputType;

  /// Input max lines (for creating a text area)
  final int maxLines;

  /// Controller
  final TextEditingController controller;

  const CreaturTextField(
      {Key key,
      this.hint,
      this.label,
      this.error,
      this.obscureText,
      this.maxLength,
      this.textInputType,
      this.onChanged,
      this.maxLines,
      this.controller})
      : super(key: key);

  @override
  _CreaturTextFieldState createState() => _CreaturTextFieldState();
}

class _CreaturTextFieldState extends State<CreaturTextField> {
  static const DEFAULT_LENGTH = 40;

  final border = new OutlineInputBorder(
      borderRadius: BorderRadius.circular(20),
      borderSide: BorderSide(color: AppColors.ACCENT_COLOR_GREEN));

  String hint;

  String label;

  String error;

  bool obscureText;

  int maxLength;

  Function onChanged;

  TextInputType textInputType;

  int maxLines;

  TextEditingController controller;

  @override
  void initState() {
    super.initState();
    hint = widget.hint;
    label = widget.label;
    error = widget.error;
    obscureText = widget.obscureText;
    maxLength = widget.maxLength;
    onChanged = widget.onChanged;
    maxLines = widget.maxLines;
    controller = widget.controller;
  }

  @override
  void didUpdateWidget(covariant CreaturTextField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.error != oldWidget.error) {
      setState(() {
        error = widget.error;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('$label', style: TextStyle(fontSize: 16, fontFamily: 'Rubik')),
          SizedBox(
            height: 10.0,
          ),
          TextField(
            keyboardType: textInputType,
            obscureText: obscureText != null ? obscureText : false,
            inputFormatters: [
              LengthLimitingTextInputFormatter(maxLength ?? DEFAULT_LENGTH)
            ],
            maxLines: maxLines != null ? maxLines : 1,
            controller: controller,
            decoration: InputDecoration(
                hintStyle: TextStyle(
                    color: Color.fromRGBO(68, 68, 68, 1), fontFamily: 'Rubik'),
                filled: true,
                fillColor: AppColors.LIGHT_CONTRAST_COLOR,
                hintText: hint,
                errorText: error,
                border: border,
                enabledBorder: border,
                focusColor: AppColors.ACCENT_COLOR_GREEN,
                focusedBorder: border,
                contentPadding: EdgeInsets.all(17)),
            onChanged: onChanged,
          ),
        ],
      ),
    );
  }
}
