import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  static final UserPreferences _instance = new UserPreferences._internal();

  factory UserPreferences() {
    return _instance;
  }

  UserPreferences._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // Get string
  getKey(String key) {
    return _prefs.getString(key) ?? '';
  }

  // Get bool
  getBool(String key) {
    return _prefs.getBool(key) ?? false;
  }

  // Set string
  setKey(String key, String value) async {
    await _prefs.setString(key, value);
  }

  /// Set boolean
  setBoolean(String key, bool value) async {
    await _prefs.setBool(key, value);
  }

  /// Clear all the preferences of the user
  Future<void> clearPrefs() async {
    await _prefs.clear();
  }

  /// Clear all the preferences of the user
  Future<bool> clearKey(String key) async {
    return await _prefs.remove(key);
  }
}
