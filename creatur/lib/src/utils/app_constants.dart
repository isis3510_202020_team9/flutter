/// AppColors class - class to store the application colors and to be reused in all the app
class AppConstants {
  /// URL and DB Constants
  static const String API_DEV = "https://creatur-backend-dev.herokuapp.com";
  static const String API_PRD = "https://creatur-backend.herokuapp.com";

  /// Shared preferences Keys
  static const String TOKEN = "token";
  static const String HAS_PROFILING = "has_profiling";
  static const String ROLE = "role";
  static const String CREATE_PRODUCT_FORM = "create_product_form";
  static const String CREATE_vENTURE_FORM = "create_venture_form";

  /// User roles
  static const String USER_ROLE = "CLIENT";
  static const String VENTURE_ROLE = "VENTURE";

  ///DB keys
  static const int RECOMMENDED_SECTION = 0;
  static const int NEAR_SECTION = 1;
  static const int TENDENCIES_SECTION = 2;
  static const int VENTURE_DETAIL_SECTION = 3;
  static const int USER_VENTURE_SECTION = 4;
  static const int CATEGORIES_SECTION = 5;

  /// Local DB related Stuff
  /// The name of the table of the DBs implemented
  static const String VENTURES_DB = "ventures";
  static const String PRODUCTS_DB = "products";

  /// URI related stuff
  static const String PHONE_SCHEMA = "tel";
  static const String URL_SCHEMA = "url";

  /// Categories and search stuff
  static const String CLOTHING_CATEGORY = "ROPA";
  static const String FAST_FOOD_CATEGORY = "COMIDA";
  static const String PETS_CATEGORY = "MASCOTAS";
  static const String TOYS_CATEGORY = "JUGUETES";
  static const String BAKERY_CATEGORY = "POSTRES";
  static const String SPORTS_CATEGORY = "DEPORTES";
  static const List<Map<String, String>> categories = [
    {CLOTHING_CATEGORY: 'CLOTHING'},
    {FAST_FOOD_CATEGORY: 'FAST_FOOD'},
    {PETS_CATEGORY: 'PETS'},
    {TOYS_CATEGORY: 'TOYS'},
    {BAKERY_CATEGORY: 'BAKERY'},
    {SPORTS_CATEGORY: 'SPORTS'},
  ];
  static const Map<String, String> cats = {
    'CLOTHING': CLOTHING_CATEGORY,
    'FAST_FOOD': FAST_FOOD_CATEGORY,
    'PETS': PETS_CATEGORY,
    'TOYS': TOYS_CATEGORY,
    'BAKERY': BAKERY_CATEGORY,
    'SPORTS': SPORTS_CATEGORY
  };
  static const List<String> catsList = [
    'CLOTHING',
    'FAST_FOOD',
    'PETS',
    'TOYS',
    'BAKERY',
    'SPORTS'
  ];

  /// Statuses of the orders
  static const String STATUS_DECLINED = "DECLINED";
  static const String STATUS_NOT_STARTED = "NOT_STARTED";
  static const String STATUS_IN_PROGRESS = "IN_PROGRESS";
  static const String STATUS_ON_WAY = "ON_WAY";
  static const String STATUS_FINISHED = "FINISHED";
  static const List<String> statusList = [
    STATUS_DECLINED,
    STATUS_NOT_STARTED,
    STATUS_IN_PROGRESS,
    STATUS_ON_WAY,
    STATUS_FINISHED,
  ];
  static const Map<String, String> statusMap = {
    STATUS_DECLINED: "Rechazada",
    STATUS_NOT_STARTED: "Recibida",
    STATUS_IN_PROGRESS: "Aceptada",
    STATUS_ON_WAY: "En camino",
    STATUS_FINISHED: "Completada"
  };
}
