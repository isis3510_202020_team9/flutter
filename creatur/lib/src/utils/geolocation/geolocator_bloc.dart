import 'dart:async';

import 'package:geolocator/geolocator.dart';

/// Singleton class to handle the geolocation permissions and position
class Geolocation {
  /// Singleton instance
  static final Geolocation _geolocation = Geolocation._createInstance();

  /// The current position
  Position _position;

  /// The subscription to the location stream
  // StreamSubscription<Position> _positionStream;

  /// Private constructor for the singleton
  Geolocation._createInstance();

  /// Singleton constructor to obtain the singleton
  factory Geolocation() => _geolocation;

  /// Function to request the geolocation and obtain a default one or the real one
  requestGeolocation() async {
    try {
      final isGeoEnabled = await Geolocator.checkPermission();
      final isLocationOn = await Geolocator.isLocationServiceEnabled();
      if (isGeoEnabled == LocationPermission.deniedForever ||
          isGeoEnabled == LocationPermission.denied ||
          !isLocationOn) {
        _position = new Position(latitude: 4.753506, longitude: -74.067085);
        // dispose();
        // } else if (_positionStream == null) {
      } else {
        // _initializeStream();
        _position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
      }
      return _position;
    } catch (e) {
      _position = new Position(latitude: 4.753506, longitude: -74.067085);
      // dispose();
      return _position;
    }
  }

  /// Request the permission to the user
  Future<void> requestGeoPermission() async {
    await Geolocator.requestPermission();
    // final permission = await Geolocator.requestPermission();
    // final isLocationOn = await Geolocator.isLocationServiceEnabled();
    // if (permission != LocationPermission.deniedForever &&
    //     permission != LocationPermission.denied &&
    //     isLocationOn) {
    //   _initializeStream();
    // }
  }

  // /// Initializes the stream to hear location changes
  // _initializeStream() {
  //   _positionStream =
  //       Geolocator.getPositionStream(desiredAccuracy: LocationAccuracy.high)
  //           .listen((Position position) {
  //     if (position != null) {
  //       _position = position;
  //     }
  //   });
  // }

  // /// Disposes the stream
  // dispose() {
  //   _positionStream?.cancel();
  // }
}
