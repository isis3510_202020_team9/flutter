/// Creatur error types
enum CreaturErrorType { STORAGE, CONNECTION }

/// Creatur error class
class CreaturError {
  CreaturErrorType type;

  String message;
  CreaturError({this.type, this.message});

  @override
  String toString() {
    return message;
  }
}

/// App error constants
class AppErrors {
  /// Connectivity error messages
  static const String CONNECTION =
      'La acción no se pudo completar. Revise su conexión a internet e inténtelo nuevamente.';
  static const String NO_DATA =
      'No hay información para mostrar. Inténtelo nuevamente.';
  static const String NO_UPDATE_ERROR =
      'Se ha desconectado de internet. Al actualizar, se mostrará la última información almacenada en el dispositivo.';

  /// Generic error messages
  static const String NO_PRODUCTS =
      'No se encontraron productos de este emprendimiento. Inténtelo más tarde.';
  static const String CANT_REDIRECT =
      'No se pudo encontrar una aplicación permitida para realizar la operación requerida.';
  static const String CANT_REDIRECT_CONNECTION =
      'Para poder acceder debe estar conectado a internet. Revise su conexión a internet e inténtelo nuevamente.';
  static const String NO_PRODUCTS_MY_VENTURE =
      'No se encontraron productos creados anteriormente. Desliza hacia abajo para refrescar o ¡Intenta crear uno!';
  static const String NO_ORDERS_MY_VENTURE =
      'No se encontraron órdenes que hayan realizado los clientes a su emprendimiento. Desliza hacia abajo para refrescar.';
  static const String NO_ORDERS_ACTIVITY =
      'No se encontraron órdenes que hayan realizado anteriormente. Desliza hacia abajo para refrescar.';

  /// Specific sections error messages
  static const String NEAR_PRODUCTS_ERROR =
      'Ocurrió un error al intentar obtener los productos cercanos. Revise su conexión e inténtelo nuevamente.';
  static const String NEAR_VENTURES_ERROR =
      'Ocurrió un error al intentar obtener los emprendimientos cercanos. Revise su conexión e inténtelo nuevamente.';
  static const String RECOMMENDED_PRODUCTS_ERROR =
      'Ocurrió un error al intentar obtener los productos recomendados. Revise su conexión e inténtelo nuevamente.';
  static const String RECOMMENDED_VENTURES_ERROR =
      'Ocurrió un error al intentar obtener los emprendimientos recomendados. Revise su conexión e inténtelo nuevamente.';
  static const String TRENDY_VENTURES_ERROR =
      'Ocurrió un error al intentar obtener las tendencias en emprendimientos. Revise su conexión e inténtelo nuevamente.';
  static const String TRENDY_PRODUCTS_ERROR =
      'Ocurrió un error al intentar obtener las tendencias en productos. Revise su conexión e inténtelo nuevamente.';
  static const String CATEGORY_PRODUCTS_ERROR =
      'Ocurrió un error al intentar obtener los productos de esta categoría. Revise su conexión e inténtelo nuevamente.';
  static const String CATEGORY_VENTURES_ERROR =
      'Ocurrió un error al intentar obtener los emprendimientos de esta categoría. Revise su conexión e inténtelo nuevamente.';
  static const String VENTURE_PRODUCTS_ERROR =
      'Ocurrió un error al intentar obtener los productos. Revise su conexión e inténtelo nuevamente.';
  static const String VENTURE_ORDERS_ERROR =
      'Ocurrió un error al intentar obtener las órdenes de su emprendimiento. Revise su conexión e inténtelo nuevamente.';
  static const String VENTURE_HOME_ERROR =
      'Ocurrió un error al intentar cargar la información de su emprendimiento. Revise su conexión e inténtelo nuevamente.';
  static const String USER_ORDERS_ERROR =
      'Ocurrió un error al intentar obtener las órdenes realizadas. Revise su conexión e inténtelo nuevamente.';
  static const String PRODUCT_DELETE_ERROR =
      'Ocurrió un error al intentar eliminar el produto. Revise su conexión e inténtelo nuevamente.';

  /// Success messages in general
  static const String SUCCESS_FORWARD_STATE =
      'La orden cambió de estado exitosamente';
  static const String SUCCESS_DELETE_PRODUCT =
      'El producto se eliminó exitosamente';
  static const String SUCCESS_REVIEW_CREATION =
      'Tu calificación fue creada exitosamente.';
}
