/// Check if the field is filled
isFilled(String field) {
  return field != null && field.isNotEmpty;
}

/// transforms a string to be capitalized
String capitalized(String text) {
  return "${text[0].toUpperCase()}${text.substring(1).toLowerCase()}";
}
