import 'package:flutter/material.dart';

/// AppColors class - class to store the application colors and to be reused in all the app
class AppColors {
  static const Color PRIMARY_COLOR = Color(0xFF005E66);
  static const Color ACCENT_COLOR_GREEN = Color(0xFF0DE777);
  static const Color ACCENT_COLOR_BLUE = Color(0xFF0EC5E6);
  static const Color CONTRAST_COLOR_BLUE = Color(0xFFB8E2E6);
  static const Color BACKGROUND_COLOR = Color(0xFFFFFFFF);
  static const INPUT_BACKGROUND_COLOR = Color.fromRGBO(242, 242, 242, 1);
  static const Color LIGHT_CONTRAST_COLOR = Color(0xFFF2F2F2);
  static const Color STATUS_COLOR = Color.fromRGBO(202, 250, 225, 1);
  static const Color REJECT_COLOR = Color.fromRGBO(238, 45, 45, 1);

  static const Map<String, Color> statusColors = {
    'DECLINED': Color(0xFFEE2D2D),
    'NOT_STARTED': CONTRAST_COLOR_BLUE,
    'IN_PROGRESS': ACCENT_COLOR_BLUE,
    'ON_WAY': Color(0xFFF0DE39),
    'FINISHED': ACCENT_COLOR_GREEN
  };
}
