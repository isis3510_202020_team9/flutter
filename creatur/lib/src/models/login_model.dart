import 'dart:convert';

import 'package:creatur/src/models/Interfaces/base_model.dart';

/// Class that models the login of the user
class LoginModel implements BaseModel {
  /// The email to log in
  String _email;

  /// The password to log in
  String _password;

  /// The default named constructor of the login model
  LoginModel(this._email, this._password);

  /// Named constructor which builds an instance of this class
  @override
  LoginModel.fromJson(String encoded) {
    Map<String, dynamic> parsedJson = json.decode(encoded);
    _email = parsedJson["email"];
    _password = parsedJson["password"];
  }

  /// Encondes the object into a JSON string.
  @override
  Map<String, dynamic> encodeToJson() {
    return {"email": _email, "password": _password};
  }

  /// Retrieves the email
  String get email => _email;

  /// Retrieves the password
  String get password => _password;
}
