import 'package:creatur/src/models/Interfaces/base_model.dart';

class ProductUnits {
  String id;
  int units;

  ProductUnits({this.id, this.units});

  Map<String, dynamic> toJson() {
    var product = {"id": this.id, "units": this.units};
    return product;
  }
}

class CreateOrderModel extends BaseModel {
  /// Selected options at profiling
  List<ProductUnits> _productUnits;

  String _address;

  String _desiredDate;

  String _ventureId;

  CreateOrderModel(
      this._address, this._desiredDate, this._ventureId, this._productUnits);

  CreateOrderModel.fromJson(Map<String, dynamic> parsedJson) {
    _productUnits = parsedJson["productInfo"];
  }

  @override
  Map<String, dynamic> encodeToJson() {
    return {
      "productsInfo": _productUnits,
      "ventureId": _ventureId,
      "address": _address,
      "desiredDate": _desiredDate
    };
  }
}
