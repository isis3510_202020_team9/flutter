import 'package:creatur/src/models/Interfaces/base_model.dart';
import 'package:creatur/src/models/venture_model.dart';

/// Class that models a Product
class ProductModel implements BaseModel {
  /// The product id
  String _id;

  /// The product name
  String _name;

  /// the product price
  int _price;

  /// the product description
  String _description;

  /// the product units
  int _units;

  /// the product photo
  String _photo;

  /// the product category
  String _productCategory;

  /// the product tags
  String _tags;

  /// The name of the venture which owns the product
  String _ventureName;

  /// The photo of the venture which owns the product
  String _venturePhoto;

  /// The id of the venture
  String _ventureId;

  /// Default product constructor
  ProductModel(this._id, this._name, this._price, this._description,
      this._units, this._photo, this._productCategory, this._tags,
      [this._ventureName, this._venturePhoto, this._ventureId]);

  /// Creates a productModel from a json
  ProductModel.fromJson(Map<String, dynamic> parsedJson) {
    _id = parsedJson['id'];
    _name = parsedJson['name'];
    _price = parsedJson['prize'];
    _description = parsedJson['description'];
    _units = parsedJson['units'];
    _photo = parsedJson['photo'];
    _productCategory = parsedJson['productCategory'];
    _tags = parsedJson['tags'];
    if (parsedJson['venture'] != null) {
      final venture = VentureModel.fromJson(parsedJson['venture']);
      _ventureName = venture.name;
      _ventureId = venture.id;
      _venturePhoto = venture.photo;
    }
  }

  /// Converts the product into a Map
  @override
  Map<String, dynamic> encodeToJson() {
    return {
      'id': _id,
      'name': _name,
      'prize': _price,
      'description': _description,
      'units': _units,
      'photo': _photo,
      'productCategory': _productCategory,
      'tags': _tags,
      'ventureName': _ventureName,
      'venturePhoto': _venturePhoto,
      'ventureId': _ventureId
    };
  }

  /// product id
  String get id => _id;

  /// product name
  String get name => _name;

  /// product price
  int get price => _price;

  /// product description
  String get description => _description;

  /// product units available
  int get units => units;

  /// product photo url
  String get photo => _photo;

  /// product category
  String get productCategory => _productCategory;

  /// product tags
  String get tags => _tags;

  /// Venture name
  String get ventureName => _ventureName;

  /// Venture Photo
  String get venturePhoto => _venturePhoto;

  String get ventureId => _ventureId;
}
