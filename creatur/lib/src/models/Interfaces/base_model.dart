/// The BaseModel Class to definde the common model interface
abstract class BaseModel {
  /// Encodes the object's properties into a Map
  Map<String, dynamic> encodeToJson();
}
