import 'package:creatur/src/models/Interfaces/base_model.dart';

class UserModel implements BaseModel {
  /// The user email
  String _email;

  /// The default constructor for the user model
  UserModel(this._email);

  /// Named constructor which builds an instance of this class from a JSON
  UserModel.fromJson(Map<String, dynamic> parsedJson) {
    // TODO: COMPLETE USER MODEL
    _email = parsedJson['email'];
  }

  /// Encondes the object into a JSON string.
  @override
  Map<String, dynamic> encodeToJson() {
    return {"email": _email};
  }

  /// Retrieves the email
  String get email => _email;
}
