import 'package:creatur/src/models/Interfaces/base_model.dart';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';

class ReviewModel {
  int id;
  int grade;
  String comment;

  ReviewModel({this.id, this.grade, this.comment});

  Map<String, dynamic> toJson() {
    return {"id": this.id, "grade": this.grade, "comment": this.comment};
  }

  ReviewModel.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    grade = parsedJson["grade"];
    comment = parsedJson["comment"];
  }
}

class OrderModel extends BaseModel {
  String _address;

  DateTime _desiredDate;

  String _specifications;

  int _finalPrize;

  String _status;

  String _id;

  List<ProductModel> _products;

  VentureModel _venture;

  ReviewModel _review;

  OrderModel(this._address, this._desiredDate, this._specifications,
      this._finalPrize, this._status, this._id,
      [this._products, this._venture, this._review]);

  OrderModel.fromJson(Map<String, dynamic> parsedJson) {
    _id = parsedJson["id"];
    _specifications = parsedJson["specifications"];
    _finalPrize = parsedJson["finalPrize"];
    _desiredDate = DateTime.parse(parsedJson["desiredDate"]);
    _status = parsedJson["status"];
    _address = parsedJson["address"];
    if (parsedJson["review"] != null) {
      _review = ReviewModel.fromJson(parsedJson["review"]);
    }
    if (parsedJson["products"] != null) {
      _products = List<ProductModel>.from((parsedJson["products"] as List)
          .map((pr) => ProductModel.fromJson(pr['product'])));
    }
    if (parsedJson["venture"] != null) {
      _venture = VentureModel.fromJson(parsedJson["venture"]);
    }
  }

  @override
  Map<String, dynamic> encodeToJson() {
    return {
      "address": _address,
      "desiredDate": _desiredDate.toIso8601String(),
      "specifications": _specifications,
      "finalPrize": _finalPrize,
      "status": _status,
      "id": _id,
    };
  }

  /// Order id
  String get id => _id;

  /// Order address
  String get address => _address;

  /// Order price
  int get finalPrize => _finalPrize;

  /// Order specifications
  String get specifications => _specifications;

  /// Order desire date
  DateTime get desiredDate => _desiredDate;

  /// Order status
  String get status => _status;

  /// Products of the order
  List<ProductModel> get products => _products;

  /// The order's venture
  VentureModel get venture => _venture;

  bool get hasReview => _review != null;
}
