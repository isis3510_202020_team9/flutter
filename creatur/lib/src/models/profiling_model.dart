import 'package:creatur/src/models/Interfaces/base_model.dart';
import 'dart:convert';

/// Profiling response interface
class Response {
  int question;
  int answer;

  Response({this.question, this.answer});

  Map<String, dynamic> toJson() {
    return {"question": this.question, "answer": this.answer};
  }
}

/// Profiling model
class ProfilingModel implements BaseModel {
  ProfilingModel(this._responses);

  /// Selected options at profiling
  List<Response> _responses;

  ProfilingModel.fromJson(String encoded) {
    Map<String, dynamic> parsedJson = json.decode(encoded);
    _responses = parsedJson["responses"];
  }

  /// Encondes the object into a JSON string.
  @override
  Map<String, dynamic> encodeToJson() {
    return {"responses": _responses};
  }

  /// Retrieves the email
  List<Response> get responses => _responses;
}
