import 'package:creatur/src/models/Interfaces/base_model.dart';

/// Class that models the venture
class VentureModel implements BaseModel {
  /// Id of the venture
  String _id;

  /// The name of the venture
  String _name;

  /// The phone of the venture
  String _phone;

  /// The photo of the venture (logo or place)
  String _photo;

  /// The address of the venture
  String _address;

  /// The facebook url of the venture
  String _facebookUrl;

  /// The instagram url of the venture
  String _instagramUrl;

  /// The latitude of the venture
  double _latitude;

  /// The longitude of the venture
  double _longitude;

  /// The rating of the venture
  double _rating;

  /// The default constructor of a venture
  VentureModel(
      this._id,
      this._name,
      this._phone,
      this._photo,
      this._address,
      this._facebookUrl,
      this._instagramUrl,
      this._latitude,
      this._longitude,
      this._rating);

  /// Builds a VentureModel from a Json
  VentureModel.fromJson(Map<String, dynamic> parsedJson) {
    _id = parsedJson['id'];
    _name = parsedJson['name'];
    _phone = parsedJson['phone'];
    _photo = parsedJson['photo'];
    _address = parsedJson['address'];
    _facebookUrl = parsedJson['facebookUrl'];
    _instagramUrl = parsedJson['instagramUrl'];
    _latitude = parsedJson['latitude'];
    _longitude = parsedJson['longitude'];
    _rating =
        parsedJson['rating'] != null ? parsedJson['rating'].toDouble() : 4.0;
  }

  /// Encodes the venture into a Map
  @override
  Map<String, dynamic> encodeToJson() => {
        'id': _id,
        'name': _name,
        'phone': _phone,
        'photo': _photo,
        'address': _address,
        'facebookUrl': _facebookUrl,
        'instagramUrl': _instagramUrl,
        'latitude': _latitude,
        'longitude': _longitude,
        'rating': _rating
      };

  Map<String, dynamic> encodeToJsonProduct() => {
        'ventureId': _id,
        'ventureName': _name,
        'venturePhoto': _photo,
      };

  /// Venture ID
  String get id => _id;

  /// Venture name
  String get name => _name;

  /// Venture phone
  String get phone => _phone;

  /// Venture photo
  String get photo => _photo;

  /// Venture address
  String get address => _address;

  /// Venture Facebook URL
  String get facebookUrl => _facebookUrl;

  /// Venture Instagram URL
  String get instagramUrl => _instagramUrl;

  /// Venture latitude
  double get latitude => _latitude;

  /// Venture longitude
  double get longitude => _longitude;

  /// Venture rating
  double get rating => _rating;
}
