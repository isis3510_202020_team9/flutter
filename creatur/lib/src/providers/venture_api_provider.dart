import 'dart:convert';

import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:http/http.dart' show Client;

/// The provider class for the venture that accesses the database
class VentureApiProvider {
  /// The http client to use for the requests
  final Client _client;

  /// The default constructor
  VentureApiProvider() : this._client = Client();

  /// Method that retrieves the suggested ventures and returns those
  Future<List<VentureModel>> getSuggestedVentures(String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/ventures/suggested',
      headers: <String, String>{'Authorization': token},
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<VentureModel>.from(
        (json.decode(response.body) as List)
            .map((e) => VentureModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to call the suggested ventures');
    }
  }

  /// Method that retrieves the near ventures and returns those
  Future<Map<String, List>> getNear(
      double lat, double lon, String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/ventures/near?latitude=$lat&longitude=$lon',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      var parsedJson = json.decode(response.body);
      List<ProductModel> retrievedProducts = List<ProductModel>.from(
        (parsedJson['nearProducts'] as List)
            .map((e) => ProductModel.fromJson(e)),
      );

      List<VentureModel> retrievedVentures = List<VentureModel>.from(
        (parsedJson['nearVentures'] as List)
            .map((e) => VentureModel.fromJson(e)),
      );

      return <String, List>{
        'products': retrievedProducts,
        'ventures': retrievedVentures
      };
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to call the suggested ventures && products');
    }
  }

  /// Method that returns the trendy ventures from the server
  Future<List<VentureModel>> getTrendyVentures(String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/ventures/trendy',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<VentureModel>.from(
        (json.decode(response.body) as List)
            .map((e) => VentureModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to call the trendy ventures');
    }
  }

  /// Method that returns the products of a venture from the server
  Future<List<ProductModel>> getProducts(String ventureId, String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/ventures/$ventureId/products',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<ProductModel>.from(
        (json.decode(response.body) as List)
            .map((e) => ProductModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }

  /// Method that retrieves the category ventures and returns those
  Future<List<VentureModel>> getCategoryVentures(
      String token, String category) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/ventures/category/$category',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<VentureModel>.from(
        (json.decode(response.body) as List)
            .map((e) => VentureModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to call the category ventures');
    }
  }

  /// Method that deletes the product of a venture from the server with the id
  Future<bool> deleteVentureProduct(String productId, String token) async {
    final response = await _client.delete(
      AppConstants.API_DEV + '/products/$productId',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful return answer
      return true;
    } else {
      // If that call was not successful, throw an error.
      throw Exception(AppErrors.CONNECTION);
    }
  }

  // Method that creates a venture for the user
  Future<VentureModel> createVenture(
      Map<String, dynamic> payload, String token) async {
    final response = await _client.post(
      AppConstants.API_DEV + "/ventures/start",
      body: json.encode(payload),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return VentureModel.fromJson(jsonDecode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }
}
