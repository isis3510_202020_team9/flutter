import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

/// the class that contains the SQLite implementation of the venture
class VentureDB {
  /// Singleton instance of the DB
  static final VentureDB _instance = VentureDB._createInstance();

  /// The database reference
  Future<Database> _venturedb;

  /// the default constructor to insert into the database
  factory VentureDB() => _instance;

  /// The singleton constructor of the DB
  VentureDB._createInstance() {
    _init();
  }

  /// Initializes the database and the reference to it
  void _init() async {
    this._venturedb = openDatabase(
      join(await getDatabasesPath(), 'venture_database.db'),
      onCreate: (db, version) {
        return db.execute(
          """CREATE TABLE ventures(id TEXT NOT NULL, name TEXT, phone TEXT, 
          photo TEXT, address TEXT, facebookUrl TEXT, instagramUrl TEXT, 
          latitude REAL, longitude REAL, rating REAL, section INTEGER NOT NULL, 
          PRIMARY KEY(id, section))""",
          // The section can be 0 (recommended), 1 (near), 2 (tendencies)
        );
      },
      version: 1,
    );
  }

  /// Inserts a venture taking into account its category (section)
  Future<void> insertVenture(VentureModel venture, int section) async {
    final Database db = await _venturedb;

    Map<String, dynamic> ventureMap = venture.encodeToJson();
    ventureMap['section'] = section;

    await db.insert(
      AppConstants.VENTURES_DB,
      ventureMap,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  /// Inserts all the ventures of the given list
  Future<void> insertAllVentures(
      List<VentureModel> ventures, int section) async {
    final Database db = await _venturedb;

    await _deleteVentures(section);

    for (var v in ventures) {
      Map<String, dynamic> ventureMap = v.encodeToJson();
      ventureMap['section'] = section;

      await db.insert(
        AppConstants.VENTURES_DB,
        ventureMap,
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
  }

  /// Queries for the ventures of the database of the category selected
  /// if there's no category passed, all the ventures are returned
  Future<List<VentureModel>> getAllVentures(int section,
      {int limit, int offset}) async {
    final Database db = await _venturedb;

    final List<Map<String, dynamic>> queryRes = await db.query(
      AppConstants.VENTURES_DB,
      where: 'section = ?',
      whereArgs: [section],
      limit: limit ?? 20,
      offset: offset ?? 0,
    );

    if (queryRes.isNotEmpty) {
      return List<VentureModel>.from(
          queryRes.map((e) => VentureModel.fromJson(e)));
    }
    return [];
  }

  /// Get the venture with the specified [id]
  Future<VentureModel> getVenture(String id) async {
    final Database db = await _venturedb;

    final List<Map<String, dynamic>> queryRes = await db.query(
      AppConstants.VENTURES_DB,
      where: 'id = ?',
      whereArgs: [id],
      limit: 1,
    );

    if (queryRes.isNotEmpty && queryRes[0] != null) {
      return VentureModel.fromJson(queryRes[0]);
    }
    return null;
  }

  /// Get the venture with the specified [id]
  Future<VentureModel> getUserVenture() async {
    final Database db = await _venturedb;

    final List<Map<String, dynamic>> queryRes = await db.query(
      AppConstants.VENTURES_DB,
      where: 'section = ?',
      whereArgs: [AppConstants.USER_VENTURE_SECTION],
      limit: 1,
    );

    if (queryRes.isNotEmpty && queryRes[0] != null) {
      return VentureModel.fromJson(queryRes[0]);
    }
    return null;
  }

  /// Deletes the ventures of the specified section
  Future<void> _deleteVentures(int section) async {
    final Database db = await _venturedb;

    await db.delete(
      AppConstants.VENTURES_DB,
      where: 'section = ?',
      whereArgs: [section],
    );
  }

  /// Deletes all of the ventures in the local database
  Future<void> deleteAllVentures() async {
    final Database db = await _venturedb;

    await db.delete(
      AppConstants.VENTURES_DB,
      where: '1',
    );
  }
}
