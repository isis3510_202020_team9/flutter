import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

/// The class that abstracts the local DB for the products
class ProductDB {
  /// Singleton instance of the DB
  static final ProductDB _instance = ProductDB._createInstance();

  /// The database reference
  Future<Database> _productdb;

  /// the default constructor to insert into the database
  factory ProductDB() => _instance;

  /// The singleton constructor of the DB
  ProductDB._createInstance() {
    _init();
  }

  /// Initializes the database and the reference to it
  void _init() async {
    this._productdb = openDatabase(
      join(await getDatabasesPath(), 'product_database.db'),
      onCreate: (db, version) {
        return db.execute(
          """CREATE TABLE products(id TEXT NOT NULL, name TEXT, prize INTEGER, 
          description TEXT, units INTEGER, photo TEXT, productCategory TEXT, 
          tags TEXT, section INTEGER NOT NULL, ventureName TEXT, 
          venturePhoto TEXT, ventureId TEXT, PRIMARY KEY(id, section))""",
          // The section can be 0 (recommended), 1 (near), 2 (tendencies)
        );
      },
      version: 1,
    );
  }

  /// Inserts a product taking into account its category (section)
  Future<void> insertProduct(ProductModel product, int section) async {
    final Database db = await _productdb;

    Map<String, dynamic> productMap = product.encodeToJson();
    productMap['section'] = section;

    await db.insert(
      AppConstants.PRODUCTS_DB,
      productMap,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  /// Inserts all the products of the given list
  Future<void> insertAllProducts(List<ProductModel> products, int section,
      [String ventureId]) async {
    final Database db = await _productdb;

    await _deleteProducts(section, ventureId);

    for (var p in products) {
      Map<String, dynamic> productMap = p.encodeToJson();
      productMap['section'] = section;
      if (ventureId != null) productMap['ventureId'] = ventureId;

      await db.insert(
        AppConstants.PRODUCTS_DB,
        productMap,
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
  }

  /// Queries for the products of the database of the category selected
  /// if there's no category passed, all the products are returned (with limits)
  Future<List<ProductModel>> getAllProducts(int section,
      {int limit, int offset}) async {
    final Database db = await _productdb;

    final List<Map<String, dynamic>> queryRes = await db.query(
      AppConstants.PRODUCTS_DB,
      where: 'section = ?',
      whereArgs: [section],
      limit: limit ?? 20,
      offset: offset ?? 0,
    );

    if (queryRes.isNotEmpty) {
      return List<ProductModel>.from(
          queryRes.map((e) => ProductModel.fromJson(e)));
    }
    return [];
  }

  /// Queries for the products of the database of the venture selected
  Future<List<ProductModel>> getProductsOfVenture(String ventureId) async {
    final Database db = await _productdb;

    final List<Map<String, dynamic>> queryRes = await db.query(
        AppConstants.PRODUCTS_DB,
        where: 'ventureId = ? AND section = ?',
        whereArgs: [ventureId, AppConstants.VENTURE_DETAIL_SECTION],
        groupBy: 'id');

    if (queryRes.isNotEmpty) {
      return List<ProductModel>.from(
          queryRes.map((e) => ProductModel.fromJson(e)));
    }
    return [];
  }

  /// Get the product with the specified [id]
  Future<ProductModel> getProduct(String id) async {
    final Database db = await _productdb;

    final List<Map<String, dynamic>> queryRes = await db.query(
      AppConstants.PRODUCTS_DB,
      where: 'id = ?',
      whereArgs: [id],
      limit: 1,
    );

    if (queryRes.isNotEmpty && queryRes[0] != null) {
      return ProductModel.fromJson(queryRes[0]);
    }
    return null;
  }

  /// Deletes the products of the specified section
  Future<void> _deleteProducts(int section, [String ventureId]) async {
    final Database db = await _productdb;

    await db.delete(
      AppConstants.PRODUCTS_DB,
      where:
          (ventureId != null) ? 'section = ? AND ventureId = ?' : 'section = ?',
      whereArgs: ventureId != null ? [section, ventureId] : [section],
    );
  }

  /// Deletes the product of the section with the specified id
  Future<void> deleteProduct(int section, String productId) async {
    final Database db = await _productdb;

    await db.delete(
      AppConstants.PRODUCTS_DB,
      where: 'section = ? AND id = ?',
      whereArgs: [section, productId],
    );
  }

  /// Deletes all of the products in the local database
  Future<void> deleteAllProducts() async {
    final Database db = await _productdb;

    await db.delete(
      AppConstants.PRODUCTS_DB,
      where: '1',
    );
  }
}
