import 'dart:convert';

import 'package:creatur/src/models/order_model.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
import 'package:http/http.dart' show Client;

class OrderApiProvider {
  /// The http client to use for the requests
  final Client _client;

  OrderApiProvider() : this._client = Client();

  Future<Map<String, dynamic>> createOrder(
      Map<String, dynamic> payload, String token) async {
    final response = await _client.post(
      AppConstants.API_DEV + "/products/buy",
      body: json.encode(payload),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return jsonDecode(response.body);
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }

  /// Method that returns the orders of a venture from the server
  Future<List<OrderModel>> getOrders(String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/ventures/orders',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<OrderModel>.from(
        (json.decode(response.body) as List).map((e) => OrderModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.VENTURE_ORDERS_ERROR);
    }
  }

  /// Get an order by its id
  Future<OrderModel> getOrder(String orderId, String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/users/orders/$orderId',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return OrderModel.fromJson(jsonDecode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }

  /// Change order status
  Future<OrderModel> changeStatus(
      String orderId, String status, String token) async {
    final response = await _client.put(
      AppConstants.API_DEV + '/ventures/orders/$orderId/status',
      body: json.encode({'status': status}),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return OrderModel.fromJson(jsonDecode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }

  /// Method that returns the orders of a user from the server
  Future<List<OrderModel>> getUserOrders(String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/users/orders',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<OrderModel>.from(
        (json.decode(response.body) as List).map((e) => OrderModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.USER_ORDERS_ERROR);
    }
  }

  /// Create a review
  Future createReview(String orderId, String token, int grade) async {
    final response = await _client.post(
      AppConstants.API_DEV + '/reviews',
      body: json.encode({'grade': grade, 'orderId': orderId}),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return true;
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }
}
