import 'dart:convert';
import 'package:creatur/src/models/product_model.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
// import 'package:creatur/src/utils/storage.dart';
import 'package:http/http.dart' show Client;

/// The provider class for the product that accesses the database
class ProductApiProvider {
  /// The http client to use for the requests
  final Client _client;

  /// The default constructor
  ProductApiProvider() : this._client = Client();

  /// Method that retrieves the suggested products and returns those
  Future<List<ProductModel>> getSuggestedProducts(
      int page, String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/products/suggested?page=$page',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<ProductModel>.from(
        (json.decode(response.body) as List)
            .map((e) => ProductModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to call the suggested products');
    }
  }

  /// Method that retrieves the trendy products and returns those
  Future<List<ProductModel>> getTrendyProducts(String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/products/trendy',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<ProductModel>.from(
        (json.decode(response.body) as List)
            .map((e) => ProductModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to call the trendy products');
    }
  }

  /// Method that retrieves the suggested products and returns those
  Future<List<ProductModel>> getCategoryProducts(
      String token, String category) async {
    final response = await _client.get(
      AppConstants.API_DEV + '/products/category/$category',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return List<ProductModel>.from(
        (json.decode(response.body) as List)
            .map((e) => ProductModel.fromJson(e)),
      );
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to call the category products');
    }
  }

  // Method that creates a product for the venture
  Future<ProductModel> createProduct(
      Map<String, dynamic> payload, String token) async {
    final response = await _client.post(
      AppConstants.API_DEV + "/ventures/products",
      body: json.encode(payload),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return ProductModel.fromJson(jsonDecode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }
}
