import 'dart:async';
import 'dart:convert';
import 'package:creatur/src/models/venture_model.dart';
import 'package:creatur/src/utils/app_constants.dart';
import 'package:creatur/src/utils/errors.dart';
// import 'package:creatur/src/utils/storage.dart';
import 'package:http/http.dart' show Client;

/// The provider class for the user that accesses the database
class UserApiProvider {
  /// The http Client for making the requests
  final Client _client;

  /// Default constructor
  UserApiProvider() : this._client = Client();

  /// Method that logs the user Into the application and returs the user with the token
  Future<dynamic> logIn(Map<String, dynamic> credentials) async {
    final response = await _client.post(
      AppConstants.API_DEV + "/users/login",
      body: json.encode(credentials),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return jsonDecode(response.body);
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }

  /// Method that registers the user Into the application and returs the user with the token
  Future<String> register(Map<String, dynamic> credentials) async {
    final response = await _client.post(
      AppConstants.API_DEV + "/users/signup",
      body: json.encode(credentials),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body)['token'];
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }

  /// Method that profile the user based on three questions
  Future profiling(Map<String, dynamic> answers, String token) async {
    final response = await _client.post(
      AppConstants.API_DEV + "/users/profiling",
      body: json.encode(answers),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return response.body;
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.CONNECTION);
    }
  }

  /// Retrieves the venture of the user from the database
  Future<VentureModel> getMyVenture(String token) async {
    final response = await _client.get(
      AppConstants.API_DEV + "/users/my-venture",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': token,
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return VentureModel.fromJson(jsonDecode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body != null
          ? jsonDecode(response.body)['error']
          : AppErrors.VENTURE_HOME_ERROR);
    }
  }
}
