import 'package:creatur/src/utils/geolocation/geolocator_bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:creatur/src/utils/storage.dart';
import 'package:flutter/material.dart';
import 'src/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Pass all uncaught errors from the framework to Crashlytics.
  await Firebase.initializeApp();
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  final prefs = UserPreferences();
  final locationService = Geolocation();
  await prefs.initPrefs();
  locationService.requestGeoPermission();
  runApp(App());
}
