# Flutter

## Folder structure
```sh
├── android
├── ios
└── lib
    ├── src/
        ├── blocs/
        ├── components/
        ├── models/
        ├── resources/
        ├── utils/
        ├── views/
        app.dart
    main.dart
```